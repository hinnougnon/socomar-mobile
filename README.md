# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repo is the application developed for fleet of cars management.
The purpose of this assignment is to develop a small app which tackles the everyday problems face on fleet management.

Find how it looks in folder "screenshots".

### How do I get set up? ###
* Dependencies
   - You should have Android Studio
   - You should download/update your android sdk if it was not done

* Web services configuration
    - You will find the web services called in file 'ApiEndpointInterface.java'. The path is 'app/src/main/java/rightcom/com/filo/api/interfaces/ApiEndpointInterface.java' .

* Deployment instructions
    - You can build the project directly on your phone or build and generate apk
    - The last apk generated is in the folder 'app/build/outputs/apk/debug'
        
* Tests
    Tested with Tecno and Samsung devices

* Logs 
    To view logs look for Logcat
    

### Possible performance optimizations for the Code. ###
    - Removes some files which are not using.
    - Bring some improvements on views.
    - Application logs are currently not saved, but it is  possible to implement Crash Analytics system if the device will be connected to internet.


### Who do I talk to? ###

* Repo owner or admin
Full name: Emmanuel HODONOU 
Mail: melyade.services@gmail.com
bibucket: https://bitbucket.org/hinnougnon/