package rightcom.com.filo.fragments;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.Objects;

import rightcom.com.filo.R;
import rightcom.com.filo.app.Filo;
import rightcom.com.filo.window.LabelVehicleActivity;


public class LabelVehicleWizardFirstPageFragment extends Fragment {

    public static final String TAG = LabelVehicleWizardFirstPageFragment.class.getSimpleName();
    //UserModel currentUser;
    private static final String ARG_POSITION = "position";


    private View rootView;

    private TextView tvTitle;
    private TextView tvDescription;
    private TextView tvBack;
    private TextView tvNext;

    private FloatingActionButton fabEditCode;
    private LinearLayout llBack;
    private LinearLayout llNext;

    private TextInputLayout tilVIN ;

    private int position;
    private String qrcodecontent;
    private Filo app;

    private Dialog dialog;

    private OnLabelVehicleWizardFirstPageFragListener mListener;

    public static LabelVehicleWizardFirstPageFragment newInstance(int position) {
        LabelVehicleWizardFirstPageFragment f = new LabelVehicleWizardFirstPageFragment();
        Bundle b = new Bundle();
        b.putInt(ARG_POSITION, position);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

       // displayToast();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_labelvehicle_first_page, container, false);

        app = (Filo) getActivity().getApplication();

        ViewCompat.setElevation(rootView, 50);
        initView();
        initTypeFace();
        initListener();
        return rootView;
    }

    private void initView() {
        tvTitle = rootView.findViewById(R.id.tvLabelTitle);
        tvDescription = rootView.findViewById(R.id.tvLabelDescription);
        tvBack = rootView.findViewById(R.id.tvBackLabel);
        tvNext = rootView.findViewById(R.id.tvNextLabel);
        llBack = rootView.findViewById(R.id.layoutBottomBackLabel);
        llNext = rootView.findViewById(R.id.layoutBottomNextLabel);
        tilVIN= rootView.findViewById(R.id.tilVIN);
    }

    private void initTypeFace() {
        tvTitle.setTypeface(app.getTypeFace());
        tvDescription.setTypeface(app.getTypeFace());
        tvBack.setTypeface(app.getTypeFace());
        tvNext.setTypeface(app.getTypeFace());
    }

    private void initListener() {
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onBackClick();
            }
        });
        llNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                go_to_secondpage();
            }
        });
    }
    private void go_to_secondpage(){
        String VIN_ENTERED = tilVIN.getEditText().getText().toString();
        if(VIN_ENTERED!=null && !VIN_ENTERED.equals("")){
            LabelVehicleWizardSecondPageFragment.VIN_PROVIDED = VIN_ENTERED;
                    LabelVehicleActivity.swipeto(1);
            closeKeyboard();
        }
        else {
            Toast.makeText(this.getContext(),"Veuillez saisir le numero de châssis.", Toast.LENGTH_LONG).show();
        }


    }

    private void closeKeyboard(){
        InputMethodManager inputManager =
                (InputMethodManager) this.getContext().
                        getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(
                this.getActivity().getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt(ARG_POSITION);

    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnLabelVehicleWizardFirstPageFragListener) {
            mListener = (OnLabelVehicleWizardFirstPageFragListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnLabelVehicleWizardFirstPageFragListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.mListener = null;
    }

    public interface OnLabelVehicleWizardFirstPageFragListener {
        void onBackClick();
    }
}