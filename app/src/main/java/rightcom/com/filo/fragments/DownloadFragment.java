package rightcom.com.filo.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Objects;

import rightcom.com.filo.R;
import rightcom.com.filo.app.Filo;

public class DownloadFragment extends Fragment implements View.OnClickListener {

    public static final String TAG = "DownloadFragment";
    private OnDownloadFragListener mListener;
    private View view;

    private TextInputLayout tilDownload;
    private EditText etNavire;
    private RelativeLayout rlSync;

    private TextView tvSync;

    private Filo app;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_download, container, false);

        initView();
        initTypeFace();
        initListener();

        return view;
    }

    private void initView() {
        tilDownload = view.findViewById(R.id.tilDownload);
        etNavire = view.findViewById(R.id.etNavire);
        rlSync = view.findViewById(R.id.rlSync);

        tvSync = view.findViewById(R.id.tvSync);


    }

    private void initListener() {
        tilDownload.setOnClickListener(tilDownloadListener);
        etNavire.setOnClickListener(etNavireListener);
        rlSync.setOnClickListener(rlSyncListener);
    }

    private void initTypeFace() {
        app = (Filo) Objects.requireNonNull(getActivity()).getApplication();
        tvSync.setTypeface(app.getTypeFace());
    }

    private View.OnClickListener tilDownloadListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            etNavire.setHint("Navire.");
        }
    };

    private View.OnClickListener etNavireListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Log.e(TAG, "On editText click");
            etNavire.setHint("Navire.");
        }
    };
    private View.OnClickListener rlSyncListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mListener.onTvDownloadFragTextClick("Download");
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnDownloadFragListener) {
            mListener = (OnDownloadFragListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnOldDashboardFragListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {

    }

    public interface OnDownloadFragListener {
        void onTvDownloadFragTextClick(String s);
    }
}
