package rightcom.com.filo.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Objects;

import rightcom.com.filo.R;
import rightcom.com.filo.app.Filo;
import rightcom.com.filo.tools.Utils;
import rightcom.com.filo.tools.Value;
import rightcom.com.filo.window.ExitVehicleActivity;

public class ExitVehicleWizardScanCodeResultFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = ExitVehicleWizardScanCodeResultFragment.class.getSimpleName();
    private static final String ARG_POSITION = "position";
    public static String currentCodeGenerated = "";

    private TextView tvTitle;
    private TextView tvScannedLabelText;
    private TextView tvBtnPreview;
    private TextView tvBtnNext;

    private RelativeLayout rlPreview;
    private RelativeLayout rlNext;

    private Intent intent = null;
    private String labelText = null;
    private String actionSelected = null;

    private View view;

    private Filo app;

    public static ExitVehicleWizardScanCodeResultFragment newInstance(int position) {

        ExitVehicleWizardScanCodeResultFragment fragment = new ExitVehicleWizardScanCodeResultFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_POSITION, position);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_exit_vehicle_wizard_scan_code_results, container, false);

        app = (Filo) Objects.requireNonNull(getActivity()).getApplication();

        tvTitle = view.findViewById(R.id.tvTitle);
        tvScannedLabelText  = view.findViewById(R.id.tvScannedLabelText);
        tvBtnPreview  = view.findViewById(R.id.tvBtnPreview);
        tvBtnNext  = view.findViewById(R.id.tvBtnNext);

        rlPreview  = view.findViewById(R.id.rlPreview);
        rlNext  = view.findViewById(R.id.rlNext);

        tvScannedLabelText.setText(currentCodeGenerated);

//        intent = getIntent();
//        actionSelected = intent.getStringExtra(Value.SELECTED_ACTION_KEY);
//        labelText = intent.getStringExtra(Value.LABEL_TEXT_KEY);
//        tvScannedLabelText.setText(labelText);

        initListener();
        initTypeFace();

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rlPreview:
//                Utils.loadFragment(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), R.id.frame_container, new ExitVehicleWizardStartFragment(), false, ExitVehicleWizardStartFragment.TAG);
                break;

            case R.id.rlNext:
                OCRScannerFragment.qrCodeGenerated = currentCodeGenerated;
                OCRScannerFragment.SELECTED_ACTION = Value.EXIT_ACTION_KEY;
//                ExitVehicleWizardStartFragment.swipeto(2, null, null, "D4ZXQ8");
                ExitVehicleActivity.swipeto(2);
//                intent1.putExtra(Value.LABEL_TEXT_KEY, labelText);
//                intent1.putExtra(Value.SELECTED_ACTION_KEY, actionSelected);
//                startActivity(intent1);
//                finish();
                break;
        }
    }

    private void initListener() {
        rlPreview.setOnClickListener(this);
        rlNext.setOnClickListener(this);
    }

    private void initTypeFace() {
        tvTitle.setTypeface(app.getTypeFace());
        tvScannedLabelText.setTypeface(app.getTypeFace());
        tvBtnPreview.setTypeface(app.getTypeFace());
        tvBtnNext.setTypeface(app.getTypeFace());
    }
}
