package rightcom.com.filo.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.Objects;

import rightcom.com.filo.R;
import rightcom.com.filo.app.Filo;
import rightcom.com.filo.models.UserRightModel;

public class ChooseOperationFragment extends Fragment {

    public static final String TAG = ChooseOperationFragment.class.getSimpleName();

    private OnChooseoperationFragListener mListener;
    private View view;

    //private TextView tvTotalIssue;
    //private TextView tvPendingIssue;
    private TextView tvLabel;
    private TextView tvReceive;
    private TextView tvChangeLocation;
    private TextView tvExit;

    //private CardView cardSearch;
    //private CardView cardDownload;
    private CardView cardLabel;
    private CardView cardReceive;
    private CardView cardChangeLocation;
    private CardView cardExit;

    //private MaterialRippleLayout mrlSearch = null;
    //private MaterialRippleLayout mrlDownload = null;
    private MaterialRippleLayout mrlLabel = null;
    private MaterialRippleLayout mrlReceive = null;
    private MaterialRippleLayout mrlChangeLocation = null;
    private MaterialRippleLayout mrlExit = null;

    private Filo filo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_submit_issue, container, false);

        initView();
        initCardView();
        initListener();
        initTypeFace();

        return view;
    }

    private void initView() {

        //tvTotalIssue = view.findViewById(R.id.tvTotalIssue);
        //tvPendingIssue = view.findViewById(R.id.tvPendingIssue);
        tvLabel = view.findViewById(R.id.tvLabel);
        tvReceive = view.findViewById(R.id.tvReceive);
        tvChangeLocation = view.findViewById(R.id.tvChangeLocation);
        tvExit = view.findViewById(R.id.tvExit);

        //cardSearch = view.findViewById(R.id.cardSearch);
        //cardDownload = view.findViewById(R.id.cardDownload);
        cardLabel = view.findViewById(R.id.cardLabel);
        cardReceive = view.findViewById(R.id.cardReceive);
        cardChangeLocation = view.findViewById(R.id.cardChangeLocation);
        cardExit = view.findViewById(R.id.cardExit);

        //mrlSearch = view.findViewById(R.id.cardMRLsearch);
        //mrlDownload = view.findViewById(R.id.cardMRLDownload);
        mrlLabel = view.findViewById(R.id.cardMRLLabel);
        mrlReceive = view.findViewById(R.id.cardMRLReceive);
        mrlChangeLocation = view.findViewById(R.id.cardMRLChangeLocation);
        mrlExit = view.findViewById(R.id.cardMRLExit);

    }

    private void initCardView() {
        //cardSearch.setCardBackgroundColor(getResources().getColor(R.color.colorReportIssue));
        //cardDownload.setCardBackgroundColor(getResources().getColor(R.color.colorNewIssue));
        cardLabel.setCardBackgroundColor(getResources().getColor(R.color.colorTotalIssue));
        cardReceive.setCardBackgroundColor(getResources().getColor(R.color.colorPendingIssue));
        cardChangeLocation.setCardBackgroundColor(getResources().getColor(R.color.colorSolvedIssue));
        cardExit.setCardBackgroundColor(getResources().getColor(R.color.colorSolvedIssue));
    }

    private void initListener() {
        //mrlSearch.setOnClickListener(mrlSearchListener);
        //mrlDownload.setOnClickListener(mrlDownloadListener);
        mrlLabel.setOnClickListener(mrlLabelListener);
        mrlChangeLocation.setOnClickListener(mrlChangelocationListener);
        mrlReceive.setOnClickListener(mrlReceiveIssueListener);
        mrlExit.setOnClickListener(mrlExitListener);
    }

    private void initTypeFace() {
        filo = (Filo) Objects.requireNonNull(getActivity()).getApplication();
        //tvTotalIssue.setTypeface(filo.getTypeFace());
        //tvPendingIssue.setTypeface(filo.getTypeFace());
        tvLabel.setTypeface(filo.getTypeFace());
        tvReceive.setTypeface(filo.getTypeFace());
        tvChangeLocation.setTypeface(filo.getTypeFace());
        tvExit.setTypeface(filo.getTypeFace());
    }


    private View.OnClickListener mrlLabelListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(!Filo.CheckUserRight(UserRightModel.LABEL)){
                Filo.NotifyUserOnWrongAccess();
                return;
            }
            mListener.onLabelActionSelected();

        }
    };
    private View.OnClickListener mrlChangelocationListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mListener.onChangelocationActionSelected();
        }
    };
    private View.OnClickListener mrlReceiveIssueListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(!Filo.CheckUserRight(UserRightModel.RECEPTION)){
                Filo.NotifyUserOnWrongAccess();
                return;
            }
            mListener.onReceiveActionSelected();
        }
    };
    private View.OnClickListener mrlExitListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(!Filo.CheckUserRight(UserRightModel.EXIT)){
                Filo.NotifyUserOnWrongAccess();
                return;
            }
            mListener.onExitActionSelected();
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnChooseoperationFragListener) {
            mListener = (OnChooseoperationFragListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnChooseoperationFragListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnChooseoperationFragListener {
        void onLabelActionSelected();
        void onChangelocationActionSelected();
        void onReceiveActionSelected();
        void onExitActionSelected();
    }
}
