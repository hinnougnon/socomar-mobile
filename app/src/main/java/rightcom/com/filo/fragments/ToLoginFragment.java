package rightcom.com.filo.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.Objects;

import rightcom.com.filo.R;
import rightcom.com.filo.app.Filo;

public class ToLoginFragment extends Fragment {

    public static final String TAG = ToLoginFragment.class.getSimpleName();
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnToLoginFragListener mListener;

    private MaterialRippleLayout mrlSimpleConnexion;
    private MaterialRippleLayout mrlArrowBack;

    private TextView tvConnexion;

    private View view;

    private Filo app;

    public ToLoginFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ToLoginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ToLoginFragment newInstance(String param1, String param2) {
        ToLoginFragment fragment = new ToLoginFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_to_login, container, false);

        init();
        setTypefaceToView();
        listenClickOnView();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnToLoginFragListener) {
            mListener = (OnToLoginFragListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnToLoginFragListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void init (){
        mrlSimpleConnexion = view.findViewById(R.id.mrlSimpleConnexion);
        mrlArrowBack = view.findViewById(R.id.mrlArrowBack);
        tvConnexion = view.findViewById(R.id.tvConnexion);
    }

    private void setTypefaceToView() {
        app = (Filo) Objects.requireNonNull(getActivity()).getApplication();
//        view.tilMail.setTypeface(TypefaceManager.getRegularTypeFace(context))
//        view.tilPassword.setTypeface(TypefaceManager.getRegularTypeFace(context))
//        view.edtMail.typeface = TypefaceManager.getRegularTypeFace(context)
//        view.edtPassword.typeface = TypefaceManager.getRegularTypeFace(context)
        //view.tvNew.typeface = TypefaceManager.getRegularTypeFace(context)
        //view.tvMakeInscriptionHere.typeface = TypefaceManager.getRegularTypeFace(context)
        tvConnexion.setTypeface(app.getTypeFace());
//        view.tvConnexionWithOther.typeface = TypefaceManager.getRegularTypeFace(context)
    }

    private void listenClickOnView(){
        //view.mrlGoToMakeInscription.setOnClickListener { mListener!!.onGoToMakeInscription() }
        mrlSimpleConnexion.setOnClickListener(mrlSimpleConnexionListener);
        mrlArrowBack.setOnClickListener(mrlArrowBackListener);
    }

    private View.OnClickListener mrlSimpleConnexionListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            controlDataAndRequestLogin();
        }
    };

    private View.OnClickListener mrlArrowBackListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Objects.requireNonNull(getActivity()).onBackPressed();
        }
    };

    private void controlDataAndRequestLogin() {
        /*when {
            view.edtMail.text.toString().isEmpty() -> view.edtMail.error = getString(R.string.text_saisir_mail)
            view.edtPassword.text.toString().isEmpty() -> view.tilPassword.error = getString(R.string.text_saisir_password)
            else -> {
                val userLoginModel = UserLoginModel()
                userLoginModel.email = view.edtMail.text.toString()
                userLoginModel.password = view.edtPassword.text.toString()
                userLoginModel.login_type = Value.TYPE_NORMAL
                mListener!!.onMakeConnexion(userLoginModel)
            }
        }*/
        mListener.onLoginProcessStart("User click on login button.");
    }

    public interface OnToLoginFragListener {
        void onLoginProcessStart(String userLoginMoel);
        void onRegisterProcessStart();
    }
}
