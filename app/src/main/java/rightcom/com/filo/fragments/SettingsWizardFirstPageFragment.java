package rightcom.com.filo.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import rightcom.com.filo.R;
import rightcom.com.filo.adapters.SettingsFragAdapter;
import rightcom.com.filo.app.Filo;
import rightcom.com.filo.dialogs.DialogSearchBluetoophPrinter;
import rightcom.com.filo.dialogs.DialogSettingsEditOCRTextLength;
import rightcom.com.filo.models.PrinterModel;
import rightcom.com.filo.models.SettingModel;
import rightcom.com.filo.models.UserModel;

public class SettingsWizardFirstPageFragment extends Fragment implements SettingsFragAdapter.OnItemClickListener {

    public static final String TAG = SettingsWizardFirstPageFragment.class.getSimpleName();
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_POSITION = "position";

    private int position;
    private UserModel currentUser;
    public PrinterModel currentPrinter;
    public SettingModel currentSetting;
    DialogSearchBluetoophPrinter dialogSearchBluetoophPrinter;
    DialogSettingsEditOCRTextLength dialogSettingsEditOCRTextLength;

    private OnSettingsWizardFirstPageFragListener mListener;

    private LinkedHashMap<String, String> settings = new LinkedHashMap<>();
    private LinkedList<String> settingsKeys = new LinkedList<>();
    private SettingsFragAdapter adapter;
    private RecyclerView rvSettings;

    private Dialog dialog;

    private Filo app;

    private View view;

    BluetoothAdapter mBluetoothAdapter;
    private UUID applicationUUID = UUID
            .fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static final int REQUEST_ENABLE_BT = 2;

    public SettingsWizardFirstPageFragment() {
        // Required empty public constructor
    }

    public static SettingsWizardFirstPageFragment newInstance(int position) {
        SettingsWizardFirstPageFragment fragment = new SettingsWizardFirstPageFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            position = getArguments().getInt(ARG_POSITION);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_settings_wizard, container, false);

        app = (Filo) getActivity().getApplication();

        currentUser = Filo.getUSer();
        currentSetting = Filo.getSetting();
        currentPrinter = Filo.getPrinter();

        initView();
        initSettingsList();
        initRecyclerView();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSettingsWizardFirstPageFragListener) {
            mListener = (OnSettingsWizardFirstPageFragListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnSettingsWizardFirstPageFragListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(String s, int i) {
        if (i == 1) {
            createAndShowPopUp(s);
        }
        if (i == 2) {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mBluetoothAdapter == null) {
                Toast.makeText(getContext(), getResources().getText(R.string.message_no_bluetooph_found), Toast.LENGTH_LONG).show();
            } else {
                if (!mBluetoothAdapter.isEnabled()) {
                    Intent enableBtIntent = new Intent(
                            BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableBtIntent,
                            REQUEST_ENABLE_BT);
                } else {
                    showBluetoothDefaultSelectorDialog();
                    /*ListPairedDevices();
                    Intent connectIntent = new Intent(MainPrinterDemoActivity.this,
                            DeviceListActivity.class);
                    startActivityForResult(connectIntent,
                            REQUEST_CONNECT_DEVICE);*/
                }
            }
        }
        if (i == 3) {
            showPopUpSetupOCRMinCharactersLength();
        }
    }

    public void onActivityResult(int mRequestCode, int mResultCode,
                                 Intent mDataIntent) {
        super.onActivityResult(mRequestCode, mResultCode, mDataIntent);

        switch (mRequestCode) {
            case REQUEST_ENABLE_BT:
                if (mResultCode == Activity.RESULT_OK) {
                    showBluetoothDefaultSelectorDialog();
                    /*ListPairedDevices();
                    Intent connectIntent = new Intent(MainPrinterDemoActivity.this,
                            DeviceListActivity.class);
                    startActivityForResult(connectIntent, REQUEST_CONNECT_DEVICE);*/
                } else {
                    Toast.makeText(getContext(), getResources().getText(R.string.message_need_blueetooph_activated), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }
    public Set<BluetoothDevice> ListPairedDevices() {
        Set<BluetoothDevice> mPairedDevices = mBluetoothAdapter
                .getBondedDevices();
        return mPairedDevices;
    }
    private void setDefaultIPSetting() {
        currentSetting=Filo.getSetting();
        if(currentSetting.getdddressip()!=null && !currentSetting.getdddressip().isEmpty()){
            settings.put(getResources().getText(R.string.label_server_ip).toString(), currentSetting.getdddressip().toString());
        }
        else{
            settings.put(getResources().getText(R.string.label_server_ip).toString(), getResources().getText(R.string.label_noset).toString());
        }
    }
    public void setDefaultBluetoothSetting() {
        currentPrinter=Filo.getPrinter();
        if(currentPrinter.getdddressmac()!=null && !currentPrinter.getdddressmac().isEmpty()){
            settings.put(getResources().getText(R.string.label_default_printer).toString(), currentPrinter.getFriendlyName().toString());
        }
        else{
            settings.put(getResources().getText(R.string.label_default_printer).toString(), getResources().getText(R.string.label_noset).toString());
        }
        if(adapter!=null){
            adapter.notifyDataSetChanged();
        }
    }
    public void setOCRMinLengthCharactersSetting() {
        currentSetting=Filo.getSetting();
        /*if(currentSetting.getocrmintextlength()!=0){
            settings.put(getResources().getText(R.string.label_ocr_min_length_text).toString(), currentSetting.getocrmintextlength()+" "+getResources().getText(R.string.label_characters).toString());
        }
        else{
            settings.put(getResources().getText(R.string.label_ocr_min_length_text).toString(), Filo.DEFAULT_VIN_MIN_LENGTH+" "+getResources().getText(R.string.label_characters).toString());
        }*/
        settings.put(getResources().getText(R.string.label_ocr_min_length_text).toString(), currentSetting.getocrmintextlength()+" "+getResources().getText(R.string.label_characters).toString());
        if(adapter!=null){
            adapter.notifyDataSetChanged();
        }
    }
    private void showBluetoothDefaultSelectorDialog(){
        Set<BluetoothDevice> mPairedDevices = ListPairedDevices();
        if (mPairedDevices.size() > 0) {
            dialogSearchBluetoophPrinter = new DialogSearchBluetoophPrinter(this);
            dialogSearchBluetoophPrinter.showDialog();
        }
        else {
            Toast.makeText(getContext(), getResources().getText(R.string.message_no_paired_devices_found), Toast.LENGTH_LONG).show();
        }

    }

    private void initView() {
        rvSettings = view.findViewById(R.id.rvSettings);
    }

    private void initSettingsList() {
        settings.put("Connecté en tant que", currentUser.getusername());
        setDefaultIPSetting();
        setDefaultBluetoothSetting();
        setOCRMinLengthCharactersSetting();
        settingsKeys.clear();
        settingsKeys.addAll(settings.keySet());
        Log.i(TAG, "");
    }

    private void initRecyclerView() {
        rvSettings.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rvSettings.setLayoutManager(layoutManager);
        adapter = new SettingsFragAdapter(SettingsWizardFirstPageFragment.this, settings, settingsKeys);
        rvSettings.setAdapter(adapter);
    }

    private void createAndShowPopUp(final String ip) {
        dialog = new Dialog(getActivity());
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.fragment_settings_wizard_first_page_popup);
        TextView tvPopUpTitle = dialog.findViewById(R.id.tvPopupTitle);
        TextView tvPopUpInfo = dialog.findViewById(R.id.tvInfo);
        final TextInputLayout tilUserPwd = dialog.findViewById(R.id.tilUserPassword);
        final TextInputLayout tilIpAddress = dialog.findViewById(R.id.tilIpAddress);
        RelativeLayout rlCancel = dialog.findViewById(R.id.rlCancel);
        TextView tvCancel = dialog.findViewById(R.id.tvCancel);
        RelativeLayout rlSave = dialog.findViewById(R.id.rlSave);
        TextView tvSave = dialog.findViewById(R.id.tvSave);
        /*Set dialog TextView TypeFace*/
        tvPopUpTitle.setTypeface(app.getTypeFace());
        tvPopUpInfo.setTypeface(app.getTypeFace());
        tilUserPwd.setTypeface(app.getTypeFace());
        tvCancel.setTypeface(app.getTypeFace());
        tvSave.setTypeface(app.getTypeFace());

        if (currentUser.isAuthenticated()) {
            tvPopUpTitle.setText("Adresse IP");
            tvPopUpInfo.setText("Modification de l'adresse ip actuelle.");
            tilIpAddress.setHint("Adresse IP");
            tilIpAddress.getEditText().setText(currentSetting.getdddressip());

            tilIpAddress.setVisibility(View.VISIBLE);
            tilUserPwd.setVisibility(View.GONE);
        }

        rlCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        rlSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentUser.isAuthenticated()) {
                    dialog.dismiss();
                    if (tilIpAddress.getEditText().getText().toString().isEmpty()) {
                        tilIpAddress.setError("Veuillez saisir une adresse ip.");
                    } else {
                        currentSetting.setaddressip(tilIpAddress.getEditText().getText().toString());
                        Filo.setSetting(currentSetting);
                        settings.put(ip, tilIpAddress.getEditText().getText().toString());
                        adapter.notifyDataSetChanged();
                    }
                } else {
                    dialog.dismiss();
                    //createAndShowProgressPopUp();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (tilUserPwd.getEditText().getText().toString().isEmpty()) {
                                tilUserPwd.setError("Veuillez saisir le mot de passe.");
                            } else {
                                if (tilUserPwd.getEditText().getText().toString().equals("test")) {
                                    currentUser.setAuthenticated(true);
                                    Toast.makeText(getActivity(), "Authentication success.", Toast.LENGTH_LONG).show();
                                    createAndShowPopUp(ip);
                                } else {
                                    Toast.makeText(getActivity(), "Authentication failed.", Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    }, 3000);
                }
            }
        });
        dialog.show();
    }

    private void showPopUpSetupOCRMinCharactersLength(){
        dialogSettingsEditOCRTextLength = new DialogSettingsEditOCRTextLength(this);
        dialogSettingsEditOCRTextLength.showDialog();
    }


    public interface OnSettingsWizardFirstPageFragListener {

    }
}
