package rightcom.com.filo.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.Objects;

import rightcom.com.filo.R;
import rightcom.com.filo.app.Filo;

public class DashboardFragment extends Fragment implements View.OnClickListener {

    public static final String TAG = DashboardFragment.class.getSimpleName();

    private OnDashboardFragListener mListener;
    private View view;

    private TextView tvSubTitle;
    private TextView tvReport;
    private TextView tvIssue;
    private TextView tvActivities;
    private TextView tvValueTotalIssue;
    private TextView tvTotalIssue;
    private TextView tvValuePendingIssue;
    private TextView tvPendingIssue;
    private TextView tvValueSolvedIssue;
    private TextView tvSolvedIssue;
    private TextView tvValueNewIssue;
    private TextView tvNewIssue;
    private TextView tvPoweredBy;
    private TextView tvRight;
    private TextView tvCom;

    private CardView cardReportIssue;
    private CardView cardNewIssue;
    private CardView cardTotalIssue;
    private CardView cardPendingIssue;
    private CardView cardSolvedIssue;

    private MaterialRippleLayout mrlTotalIssue = null;
    private MaterialRippleLayout mrlPendingIssue = null;
    private MaterialRippleLayout mrlSolvedIssue = null;
    private MaterialRippleLayout mrlReportIssue = null;
    private MaterialRippleLayout mrlNewIssue = null;

    private LinearLayout linearLayout = null;

    private Filo filo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        initView();
        initCardView();
        initListener();
        initTypeFace();

        return view;
    }

    private void initView() {

        linearLayout = view.findViewById(R.id.layout_parent_1);

        tvSubTitle = view.findViewById(R.id.txtviewSubtitle);
        tvReport = view.findViewById(R.id.tvReport);
        tvIssue = view.findViewById(R.id.tvIssue);
        tvActivities = view.findViewById(R.id.tvActivities);
        tvValueTotalIssue = view.findViewById(R.id.tvValueTotalIssue);
        tvTotalIssue = view.findViewById(R.id.tvTotalIssue);
        tvValuePendingIssue = view.findViewById(R.id.tvValuePendingIssue);
        tvPendingIssue = view.findViewById(R.id.tvPendingIssue);
        tvValueSolvedIssue = view.findViewById(R.id.tvValueSolvedIssue);
        tvSolvedIssue = view.findViewById(R.id.tvSolvedIssue);
        tvValueNewIssue = view.findViewById(R.id.tvValueNewIssue);
        tvNewIssue = view.findViewById(R.id.tvNewIssue);
        tvPoweredBy = view.findViewById(R.id.tvPoweredBy);
        tvRight = view.findViewById(R.id.tvRight);
        tvCom = view.findViewById(R.id.tvCom);

        cardReportIssue = view.findViewById(R.id.cardReportIssue);
        cardNewIssue = view.findViewById(R.id.cardNewIssue);
        cardTotalIssue = view.findViewById(R.id.cardTotalIssue);
        cardPendingIssue = view.findViewById(R.id.cardPendingIssue);
        cardSolvedIssue = view.findViewById(R.id.cardSolvedIssue);


        mrlTotalIssue = view.findViewById(R.id.cardMRLTotalIssue);
        mrlPendingIssue = view.findViewById(R.id.cardMRLPendingIssue);
        mrlSolvedIssue = view.findViewById(R.id.cardMRLSolvedIssue);
        mrlReportIssue = view.findViewById(R.id.cardMRLReportIssue);
        mrlNewIssue = view.findViewById(R.id.cardMRLNewIssue);
    }

    private void initCardView() {
        cardReportIssue.setCardBackgroundColor(getResources().getColor(R.color.colorReportIssue));
        cardNewIssue.setCardBackgroundColor(getResources().getColor(R.color.colorNewIssue));
        cardTotalIssue.setCardBackgroundColor(getResources().getColor(R.color.color6));
        cardPendingIssue.setCardBackgroundColor(getResources().getColor(R.color.colorPendingIssue));
        cardSolvedIssue.setCardBackgroundColor(getResources().getColor(R.color.colorSolvedIssue));
    }

    private void initListener() {
        mrlTotalIssue.setOnClickListener(mrlTotalIssueListener);
        mrlPendingIssue.setOnClickListener(mrlPendingIssueListener);
        mrlSolvedIssue.setOnClickListener(mrlSolvedIssueListener);
        mrlReportIssue.setOnClickListener(mrlReportIssueListener);
        mrlNewIssue.setOnClickListener(mrlNewIssueListener);
    }

    private void initTypeFace() {
        filo = (Filo) Objects.requireNonNull(getActivity()).getApplication();
        tvSubTitle.setTypeface(filo.getTypeFace());
        tvReport.setTypeface(filo.getTypeFace());
        tvIssue.setTypeface(filo.getTypeFace());
        tvActivities.setTypeface(filo.getTypeFace());
        tvValueTotalIssue.setTypeface(filo.getTypeFace());
        tvTotalIssue.setTypeface(filo.getTypeFace());
        tvValuePendingIssue.setTypeface(filo.getTypeFace());
        tvPendingIssue.setTypeface(filo.getTypeFace());
        tvValueSolvedIssue.setTypeface(filo.getTypeFace());
        tvSolvedIssue.setTypeface(filo.getTypeFace());
        tvValueNewIssue.setTypeface(filo.getTypeFace());
        tvNewIssue.setTypeface(filo.getTypeFace());
        tvPoweredBy.setTypeface(filo.getTypeFace());
        tvRight.setTypeface(filo.getTypeFace());
        tvCom.setTypeface(filo.getTypeFace());
    }

    private View.OnClickListener mrlTotalIssueListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mListener.onTotalIssueClickListener();
        }
    };

    private View.OnClickListener mrlPendingIssueListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mListener.onPendingIssueClickListener();
        }
    };

    private View.OnClickListener mrlSolvedIssueListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mListener.onSolvedIssueClickListener();
        }
    };

    private View.OnClickListener mrlReportIssueListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mListener.onReportIssueClickListener();
        }
    };

    private View.OnClickListener mrlNewIssueListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mListener.onNewIssueClickListener();
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnDashboardFragListener) {
            mListener = (OnDashboardFragListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnOldDashboardFragListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cardTotalIssue: mListener.onTotalIssueClickListener(); break;
            case R.id.cardPendingIssue: mListener.onPendingIssueClickListener(); break;
            case R.id.cardSolvedIssue: mListener.onSolvedIssueClickListener(); break;
            case R.id.cardReportIssue: mListener.onReportIssueClickListener(); break;
            case R.id.cardNewIssue: mListener.onNewIssueClickListener(); break;
        }
    }

    public interface OnDashboardFragListener {
        void onTotalIssueClickListener();
        void onPendingIssueClickListener();
        void onSolvedIssueClickListener();
        void onReportIssueClickListener();
        void onNewIssueClickListener();
    }
}
