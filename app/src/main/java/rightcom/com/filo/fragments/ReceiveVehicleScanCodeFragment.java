package rightcom.com.filo.fragments;
        import android.Manifest;
        import android.content.Context;
        import android.content.Intent;
        import android.content.pm.PackageManager;
        import android.os.Bundle;
        import android.support.annotation.NonNull;
        import android.support.v4.app.ActivityCompat;
        import android.support.v4.app.Fragment;
        import android.support.v4.content.ContextCompat;
        import android.support.v4.view.ViewCompat;
        import android.util.Log;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.LinearLayout;
        import android.widget.TextView;
        import android.widget.Toast;

        import rightcom.com.filo.app.Filo;
        import rightcom.com.filo.fragments.ReceiveVehicleWizardShowResultsFragment;
        import rightcom.com.filo.R;
        import rightcom.com.filo.fragments.ReceiveVehicleWizardStartFragment;
        import rightcom.com.filo.tools.Value;

        import com.google.zxing.integration.android.IntentIntegrator;
        import com.google.zxing.integration.android.IntentResult;

        import java.util.Objects;

public class ReceiveVehicleScanCodeFragment extends Fragment {

    public static final String TAG = ReceiveVehicleScanCodeFragment.class.getSimpleName();
    //UserModel currentUser;
    private static final String ARG_POSITION = "position";
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 20;

    private int position;
    private String qrcodecontent;

    private TextView tvReceiveTitle;
    private TextView tvReceiveSubTitle;
    private TextView tvReceiveScanCode;
    private TextView tvReceiveBack;

    private LinearLayout llReceiveBack;
    private LinearLayout llReceiveScanCode;

    private OnReceiveVehicleScanCodeFragListener mListener;
    private View rootView;
    private Filo app;

    public static ReceiveVehicleScanCodeFragment newInstance(int position) {
        ReceiveVehicleScanCodeFragment f = new ReceiveVehicleScanCodeFragment();
        Bundle b = new Bundle();
        b.putInt(ARG_POSITION, position);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        displayToast();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_receivevehicle_scancode, container, false);
        // currentUser=KoodPay.getUSer();
        ViewCompat.setElevation(rootView, 50);

        app = (Filo) Objects.requireNonNull(getActivity()).getApplication();

        initView();
        initTypeFace();
        initListener();

        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt(ARG_POSITION);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() != null) {
                qrcodecontent = result.getContents();
                call_go_to_show_results(qrcodecontent);
//                Toast.makeText(getActivity(), "QR Code retrouvé:"+qrcodecontent, Toast.LENGTH_SHORT).show();
                Log.e(TAG, "QR Code retrouvé:" + qrcodecontent);
            }
            else{
                Toast.makeText(getActivity(), "QR Code non retrouvé", Toast.LENGTH_SHORT).show();
            }
            qrcodecontent = null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // camera-related task you need to do.
                    scanFromFragment();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    //checkForPermissions();
                }
                return;
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnReceiveVehicleScanCodeFragListener) {
            mListener = (OnReceiveVehicleScanCodeFragListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnReceiveVehicleScanCodeFragListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.mListener = null;
    }

    private void initView() {
        tvReceiveTitle = rootView.findViewById(R.id.tvReceiveTitle);
        tvReceiveSubTitle = rootView.findViewById(R.id.tvReceiveSubTitle);
        tvReceiveScanCode = rootView.findViewById(R.id.tvReceiveScanCode);
        tvReceiveBack = rootView.findViewById(R.id.tvReceiveBack);

        llReceiveBack = rootView.findViewById(R.id.llReceiveBack);
        llReceiveScanCode = rootView.findViewById(R.id.llReceiveScanCode);
    }

    private void initTypeFace() {
        tvReceiveTitle.setTypeface(app.getTypeFace());
        tvReceiveSubTitle.setTypeface(app.getTypeFace());
        tvReceiveScanCode.setTypeface(app.getTypeFace());
        tvReceiveBack.setTypeface(app.getTypeFace());
    }

    private void initListener() {
        llReceiveBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onReceiveScanCodeBackClick();
            }
        });
        llReceiveScanCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkForPermissions();
            }
        });
    }

    public void scanFromFragment() {
        IntentIntegrator.forSupportFragment(this)
                .setOrientationLocked(false).setPrompt(getResources().getString(R.string.lbl_scantheqrcode)).initiateScan();
    }

    private void displayToast() {
        if(getActivity() != null && qrcodecontent != null) {

            qrcodecontent = null;
        }
    }

    private void call_go_to_show_results(String qrcode){
        //ReceiveVehicleWizardShowResultsFragment.CurrentQRcodeGenerated = qrcode;
        ReceiveVehicleWizardStartFragment.swipeto(1, TAG, Value.RECEIVE_ACTION_KEY, qrcode);
    }

    private void checkForPermissions() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            requestForPermissions();
        } else {
            // Permission has already been granted
            scanFromFragment();
        }
    }

    private void requestForPermissions() {
        // No explanation needed; request the permission
        requestPermissions(new String[]{Manifest.permission.CAMERA},
                MY_PERMISSIONS_REQUEST_CAMERA);
        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
        // app-defined int constant. The callback method gets the
        // result of the request.
    }

    public interface OnReceiveVehicleScanCodeFragListener {
        void onReceiveScanCodeBackClick();
    }
}