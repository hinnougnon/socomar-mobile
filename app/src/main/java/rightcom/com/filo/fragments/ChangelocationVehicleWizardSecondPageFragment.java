package rightcom.com.filo.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rightcom.com.filo.R;
import rightcom.com.filo.api.ApiServiceGenerator;
import rightcom.com.filo.api.interfaces.ApiEndpointInterface;
import rightcom.com.filo.api.responses.CheckVINResponse;
import rightcom.com.filo.app.Filo;
import rightcom.com.filo.models.UserModel;
import rightcom.com.filo.tools.Value;
import rightcom.com.filo.window.ChangeLocationVehicleActivity;
import rightcom.com.filo.window.ReceiveVehicleActivity;

public class ChangelocationVehicleWizardSecondPageFragment extends Fragment {

    UserModel currentUser;
    public static String QRCodeSCANNED = "";

    public static final String TAG = ChangelocationVehicleWizardSecondPageFragment.class.getSimpleName();
    private static final String ARG_POSITION = "position";

    public static String SELECTED_ACTION = null;


    private OnChangelocationVehicleWizardSecondPageFragment mListener;

    private TextView tvTitle;
    private TextView tvSubTitle;
    private TextView tvBack;

    private ImageView ivSearch;
//    private ImageView ivFound;
    private ImageView ivNotFound;

    private LinearLayout llBack;

    private View view;

    private Filo app;

    public ChangelocationVehicleWizardSecondPageFragment() {
        // Required empty public constructor
    }

    public static ChangelocationVehicleWizardSecondPageFragment newInstance(int position) {
        ChangelocationVehicleWizardSecondPageFragment fragment = new ChangelocationVehicleWizardSecondPageFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    public static ChangelocationVehicleWizardSecondPageFragment newInstance(String param1, String param2) {
        ChangelocationVehicleWizardSecondPageFragment fragment = new ChangelocationVehicleWizardSecondPageFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_changelocationvehicle_second_page, container, false);

        initView();
        initTypeFace();
        initListener();

        return view;
    }

    private void initView() {
        tvTitle = view.findViewById(R.id.tvTitlechangelocationsecondpage);
        tvSubTitle = view.findViewById(R.id.tvDescriptionchangelocationsecondpage);
        tvBack = view.findViewById(R.id.tvBackchangelocationsecondpage);
        ivSearch = view.findViewById(R.id.ivSearchchangelocationsecondpage);
        ivNotFound = view.findViewById(R.id.ivFailedchangelocationsecondpage);
        llBack = view.findViewById(R.id.layoutBackchangelocationsecondpage);
    }

    private void showUINoresults(String error_message){
        ivSearch.setVisibility(View.GONE);
        ivNotFound.setVisibility(View.VISIBLE);
        tvTitle.setText("Résultat(s)");
        tvSubTitle.setText("Aucun élément retrouvé. \n"+error_message);
    }

    private void call_Search(){
        if(QRCodeSCANNED!=null && QRCodeSCANNED!=""){
            String[] ArrayCurrentQRcodeReceived = QRCodeSCANNED.split("_");
            String VIN= "";
            if(ArrayCurrentQRcodeReceived.length>0){
                VIN = ArrayCurrentQRcodeReceived[0];
            }
            if(VIN!=null && VIN!=""){
                ApiEndpointInterface apiService =
                        ApiServiceGenerator.getClient().create(ApiEndpointInterface.class);

                //VIN_SCANNED="s0l1ne42";//
                String step= "in";
                Call<CheckVINResponse> call = apiService.lookforVIN(VIN,step,currentUser.getkey());
                call.enqueue(new Callback<CheckVINResponse>() {
                    @Override
                    public void onResponse(Call<CheckVINResponse>call, Response<CheckVINResponse> response) {
                        //KoodPay.appDialog.dismiss();
                        if(response.isSuccessful())
                        {
                            CheckVINResponse data=response.body();
                            if(data.getVinkey()!=null && data.getVinkey()!="" && data.getStatut().equals( Value.API_STATUS_OK)){
                                // convert result
                                try {
                                    ChangelocationVehicleWizardThirdPageFragment.checkingVINResponseObject=data;
                                    ChangeLocationVehicleActivity.swipeto(2);
                                } catch (final Exception e) {
                                    e.printStackTrace();
                                }


                            }
                            else
                            {
                                showUINoresults(data.getMsg());
                                return;
                            }
                        }
                        else
                        {

                            showUINoresults("Le serveur a rencontré un problème lors de l'exécution de la requête.");
                            return;
                        }
                    }
                    @Override
                    public void onFailure(Call<CheckVINResponse>call, Throwable t) {
                        showUINoresults("Echec lors de la connexion au serveur.");
                    }
                });
            }
            else{
                showUINoresults("QR Code invalide après décodage du code.");
            }

        }
        else{
            showUINoresults("QR Code invalide car le code récupéré est vide.");
        }
    }

    @Override
    public void setUserVisibleHint(boolean visible){
        super.setUserVisibleHint(visible);
        if (visible){   // only at fragment screen is resumed
            currentUser=Filo.getUSer();
            call_Search();
        }
    }

    private void initTypeFace() {
        app = (Filo) Objects.requireNonNull(getActivity()).getApplication();
        tvTitle.setTypeface(app.getTypeFace());
        tvSubTitle.setTypeface(app.getTypeFace());
        tvBack.setTypeface(app.getTypeFace());
    }

    private void initListener() {
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangeLocationVehicleActivity.swipeto(0);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnChangelocationVehicleWizardSecondPageFragment) {
            mListener = (OnChangelocationVehicleWizardSecondPageFragment) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnChangelocationVehicleWizardSecondPageFragment");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnChangelocationVehicleWizardSecondPageFragment {
        void onBackClick();
    }

}
