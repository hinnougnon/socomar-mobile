package rightcom.com.filo.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rightcom.com.filo.LoginPageActivity;
import rightcom.com.filo.R;
import rightcom.com.filo.adapters.LocationAdapter;
import rightcom.com.filo.api.ApiServiceGenerator;
import rightcom.com.filo.api.interfaces.ApiEndpointInterface;
import rightcom.com.filo.api.responses.CargettingResponse;
import rightcom.com.filo.api.responses.CheckVINResponse;
import rightcom.com.filo.api.responses.LocationResponse;
import rightcom.com.filo.app.Filo;
import rightcom.com.filo.dialogs.CustomDialog;
import rightcom.com.filo.models.UserModel;
import rightcom.com.filo.printers.zebra.ZebraLabelPrinter;
import rightcom.com.filo.tools.Value;

import static rightcom.com.filo.tools.Value.WIDTH;

public class ReceiveVehicleWizardFourthPageFragment extends Fragment {
    UserModel currentUser;
	private static final String ARG_POSITION = "position";
    public static CheckVINResponse checkingVINResponseObject ;
    public static LocationResponse cargetting_location ;
	private int position;
	private boolean isSaveReceive = false;

	private TextView tvTitle;
	private TextView tvSubTitle;

    private ImageView ivQRCode;
    private ImageView ivFailed;
    private MKLoader mkLoader;

	private LinearLayout llBack;
	private LinearLayout llPrint;
    private TextView tvBack;
    private TextView tvPrint;

	private View rootView;

	private Filo app;


    private OnReceiveVehicleWizardFourthPageFragListener mListener;

	public static ReceiveVehicleWizardFourthPageFragment newInstance(int position) {
		ReceiveVehicleWizardFourthPageFragment f = new ReceiveVehicleWizardFourthPageFragment();
		Bundle b = new Bundle();
		b.putInt(ARG_POSITION, position);
		f.setArguments(b);
		return f;
	}

    public static void newInstance(String qr) {
	    Bundle bundle = new Bundle();
    }

    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		position = Objects.requireNonNull(getArguments()).getInt(ARG_POSITION);
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

	    rootView = inflater.inflate(R.layout.fragment_receivevehicle_fourth_page, container, false);

         app = (Filo) Objects.requireNonNull(getActivity()).getApplication();

		 currentUser=Filo.getUSer();
		ViewCompat.setElevation(rootView, 50);

        initView();
        initTypeFace();
        initListener();

		return rootView;
	}

    private void call_SaveReceive(){
        if(checkingVINResponseObject!=null && checkingVINResponseObject.getVinkey() != "" && checkingVINResponseObject.getVinkey() != null && cargetting_location!=null){
            ApiEndpointInterface apiService =
                    ApiServiceGenerator.getClient().create(ApiEndpointInterface.class);

            String mvt="in";
            Call<CargettingResponse> call = apiService.cargetting(checkingVINResponseObject.getVin(),mvt,cargetting_location.getlockey(),"",currentUser.getkey());
            call.enqueue(new Callback<CargettingResponse>() {
                @Override
                public void onResponse(Call<CargettingResponse>call, Response<CargettingResponse> response) {
                    //KoodPay.appDialog.dismiss();
                    if(response.isSuccessful())
                    {
                        CargettingResponse data=response.body();
                        if(data.getStatut()!=null && data.getStatut()!="" && data.getStatut().equals( Value.API_STATUS_OK)){
                            // convert result
                            try {
//                                Toast.makeText(getContext(), "save", Toast.LENGTH_SHORT).show();
                                isSaveReceive = true;
                                generateQRCode();
                            } catch (final Exception e) {
                                e.printStackTrace();
                            }
                        }
                        else
                        {
                            showFailedTosave(data.getMsg());
                            return;
                        }
                    }
                    else
                    {
                        showFailedTosave("Le serveur a rencontré un problème lors de l'exécution de la requête.");
                        return;
                    }
                }
                @Override
                public void onFailure(Call<CargettingResponse>call, Throwable t) {
                    // Log error here since request failed
                    //KoodPay.appDialog.dismiss();
                    showFailedTosave("Ceci est dû à un échec lors de la connexion au serveur.");
                }
            });
        }
    }

    private void generateQRCode() {
        // create thread to avoid ANR Exception
        Thread t = new Thread(new Runnable() {
            public void run() {
                try {
                    synchronized (this) {
                        wait(3000);
// runOnUiThread method used to do UI task in main thread.
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Bitmap bitmap = null;
                                    bitmap = encodeAsBitmap(checkingVINResponseObject.getVin() +"_"+checkingVINResponseObject.getBL());
                                    ivQRCode.setImageBitmap(bitmap);
                                    tvTitle.setText("Sauvegarde terminée");
                                    tvSubTitle.setText("");
                                    tvPrint.setText("imprimer");
                                    ivQRCode.setVisibility(View.VISIBLE);
                                    mkLoader.setVisibility(View.GONE);
                                } catch (WriterException e) {
                                    e.printStackTrace();
                                } // end of catch block
                            } // end of run method
                        });
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
    }

    public Bitmap encodeAsBitmap(String str) throws WriterException {
        BitMatrix result;
        try {
            result = new MultiFormatWriter().encode(str,
                    BarcodeFormat.QR_CODE, WIDTH, WIDTH, null);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int w = result.getWidth();
        int h = result.getHeight();
        int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++) {
            int offset = y * w;
            for (int x = 0; x < w; x++) {
                pixels[offset + x] = result.get(x, y) ? getResources().getColor(R.color.colorBlackFilterOpac166):getResources().getColor(R.color.colorWhite);
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, 500, 0, 0, w, h);
        return bitmap;
    } /// end of this method

    private void showFailedTosave(String msg_server){
	    tvTitle.setText("Echec de sauvegarde");
	    tvSubTitle.setText("L'opération de réception a échouée."+msg_server);
        ivFailed.setVisibility(View.VISIBLE);
        ivQRCode.setVisibility(View.GONE);
        mkLoader.setVisibility(View.GONE);
    }

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed()){   // only at fragment screen is resumed

            call_SaveReceive();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnReceiveVehicleWizardFourthPageFragListener) {
            mListener = (OnReceiveVehicleWizardFourthPageFragListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnReceiveVehicleWizardFourthPageFragListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.mListener = null;
    }

    private void initView() {

	    tvTitle = rootView.findViewById(R.id.tvTitle);
	    tvSubTitle = rootView.findViewById(R.id.tvSubTitle);

        ivQRCode = rootView.findViewById(R.id.ivRVWSRQRCode);
        ivFailed = rootView.findViewById(R.id.ivFailed);
        mkLoader = rootView.findViewById(R.id.mRVWSRkLoader);

        tvBack = rootView.findViewById(R.id.tvBottomBackText);
        tvPrint = rootView.findViewById(R.id.tvPrint);

        llBack = rootView.findViewById(R.id.layoutBack);
        llPrint = rootView.findViewById(R.id.layoutPrint);
    }

    private void initTypeFace() {
	    tvTitle.setTypeface(app.getTypeFace());
	    tvSubTitle.setTypeface(app.getTypeFace());
        tvBack.setTypeface(app.getTypeFace());
        tvPrint.setTypeface(app.getTypeFace());
    }

    private void initListener() {
//        llBack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                ReceiveVehicleWizardStartFragment.swipeto(0, null, null, null);
//                mListener.go_to_receivevehicle_firstpage_fragment();
//            }
//        });
        llPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isSaveReceive) {
                    //Toast.makeText(getActivity(), tvPrint.getText().toString().toUpperCase(), Toast.LENGTH_LONG).show();
                    callPrint();
                    mListener.go_to_receivevehicle_firstpage_fragment();
                } else {
                    tvTitle.setText("Sauvegarde en cours...");
                    tvSubTitle.setText("Nous sommes entrain de sauvegarder l'opération. Veuillez patienter...");
                    ivFailed.setVisibility(View.GONE);
                    call_SaveReceive();
                }
            }
        });
    }
    private boolean sendData = true;
    public void callPrint() {
        Filo.appDialog= CustomDialog.callLoadingDialog(getContext());
        Filo.appDialog.show();
        new Thread(new Runnable() {
            public void run() {
                Looper.prepare();
                //connectAndSendJob("UYDTUYTYUDT","kjsdhjkh");
                ZebraLabelPrinter objzebra=new ZebraLabelPrinter();
                objzebra.connectAndSendLabelJob(checkingVINResponseObject.getVin(),checkingVINResponseObject.getBL());
                Looper.loop();
                Looper.myLooper().quit();
                sendData = true;
            }
        }).start();

    }
	public interface OnReceiveVehicleWizardFourthPageFragListener {
        void go_to_receivevehicle_firstpage_fragment();
    }

}