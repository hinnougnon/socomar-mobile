package rightcom.com.filo.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;

import rightcom.com.filo.R;
import rightcom.com.filo.adapters.ExitVehiclePageAdapter;

public class ExitVehicleWizardStartFragment extends Fragment {

    public static final String TAG = ExitVehicleWizardStartFragment.class.getSimpleName();
	//UserModel currentUser;
	private ExitVehiclePageAdapter adapter;
	private static ViewPager pager;
	private TextView previousButton;
	private TextView nextButton;
	private TextView navigator;
	private int currentItem;

	private EditText txt_addmoneyrechargecode_code;
	private TextView btnaddmoneyrechargecode_scan;
	private TextView btnaddmoneyrechargecode_type;

//	private OnLabelVehicleWizardStartFragListener mListener;

	public ExitVehicleWizardStartFragment(){}

	public static ExitVehicleWizardStartFragment newInstance() {
		return new ExitVehicleWizardStartFragment();
	}

	private ExitVehicleWizardStartFragment instance_created;

    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View rootView = inflater.inflate(R.layout.fragment_exitvehicle_start, container, false);

		//currentUser=KoodPay.getUSer();
		instance_created=this;
		currentItem = 0;

		pager = (ViewPager) rootView.findViewById(R.id.activity_wizard_universal_pager);
		previousButton = (TextView) rootView.findViewById(R.id.activity_wizard_universal_previous);
		nextButton = (TextView) rootView.findViewById(R.id.activity_wizard_universal_next);
		navigator = (TextView) rootView.findViewById(R.id.activity_wizard_universal_possition);

		adapter = new ExitVehiclePageAdapter( getChildFragmentManager());
		pager.setAdapter(adapter);
		pager.setCurrentItem(currentItem);

		setNavigator();

		pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int position) {
				// TODO Auto-generated method stub
				setNavigator();
			}
		});

		previousButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*if (pager.getCurrentItem() != 0) {
					pager.setCurrentItem(pager.getCurrentItem() - 1);
				}
				setNavigator();*/
				Toast.makeText(getActivity(), "Skip",
						Toast.LENGTH_SHORT).show();
			}
		});

		nextButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (pager.getCurrentItem() != (pager.getAdapter().getCount() - 1)) {
					pager.setCurrentItem(pager.getCurrentItem() + 1);
				} else {
					Toast.makeText(getActivity(), "Finish",
							Toast.LENGTH_SHORT).show();
				}
				setNavigator();
			}
		});
		return rootView;
	}

	public void setNavigator() {
		String navigation = "";
		for (int i = 0; i < adapter.getCount(); i++) {
			if (i == pager.getCurrentItem()) {
				navigation += getString(R.string.material_icon_point_full)
						+ "  ";
			} else {
				navigation += getString(R.string.material_icon_point_empty)
						+ "  ";
			}
		}
		navigator.setText(navigation);
	}

	public void setCurrentSlidePosition(int position) {
		this.currentItem = position;
	}

	public int getCurrentSlidePosition() {
		return this.currentItem;
	}

	public static void swipeto(int position, String parentFragTag, String selectedAction, String qr) {
        switch (position) {
            case 1:
                ExitVehicleWizardScanCodeResultFragment.newInstance(position);
                pager.setCurrentItem(position);
                Objects.requireNonNull(pager.getAdapter()).notifyDataSetChanged();
                break;
            case 2:
                OCRScannerFragment.newInstance(parentFragTag, null, selectedAction, position);
                pager.setCurrentItem(position);
				Objects.requireNonNull(pager.getAdapter()).notifyDataSetChanged();
                break;
            case 3:
                ExitVehicleWizardThirdPageFragment.newInstance(position);
                pager.setCurrentItem(position);
				Objects.requireNonNull(pager.getAdapter()).notifyDataSetChanged();
                break;

            default:
            	pager.setCurrentItem(0);
				Objects.requireNonNull(pager.getAdapter()).notifyDataSetChanged();
				break;
        }
//        if (qr == null) {
//            OCRScannerFragment.newInstance(parentFragTag, null, SELECTED_ACTION, position);
//            pager.setCurrentItem(position);
//        } else {
//            LabelVehicleWizardShowResultsFragment.newInstance(qr);
//            pager.setCurrentItem(position);
//        }
	}

	public static void refreshOCRFrag() {
        Objects.requireNonNull(pager.getAdapter()).notifyDataSetChanged();
    }

	final View.OnClickListener buttonProcessTypeListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {


		}
	};

	final View.OnClickListener buttonProcessScanListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
		}
	};

//	@Override
//	public void onAttach(Context context) {
//		super.onAttach(context);
//		if (context instanceof OnLabelVehicleWizardStartFragListener) {
//			mListener = (OnLabelVehicleWizardStartFragListener) context;
//		} else {
//			throw new RuntimeException(context.toString()
//					+ " must implement OnOldDashboardFragListener");
//		}
//	}
//
//	@Override
//	public void onDetach() {
//		super.onDetach();
//		mListener = null;
//	}

	@Override
	public void onActivityCreated(final Bundle savedState) {
		super.onActivityCreated(savedState);
	}

//    @Override
//    public void onBackClick() {
//        Toast.makeText(getActivity(), "LabelVehicleWizardStartFragment -> Back", Toast.LENGTH_LONG).show();
//    }
//
//    @Override
//    public void onNextClick() {
//        Toast.makeText(getActivity(), "LabelVehicleWizardStartFragment -> Next", Toast.LENGTH_LONG).show();
//    }

    public interface OnLabelVehicleWizardStartFragListener {
	}

}