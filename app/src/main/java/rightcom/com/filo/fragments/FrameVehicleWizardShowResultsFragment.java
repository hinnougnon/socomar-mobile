package rightcom.com.filo.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Objects;

import rightcom.com.filo.R;
import rightcom.com.filo.app.Filo;

public class FrameVehicleWizardShowResultsFragment extends Fragment implements View.OnClickListener {

    public static final String TAG = FrameVehicleWizardShowResultsFragment.class.getSimpleName();

    private TextView tvTitle;
    private TextView tvScannedFrameText;
    private TextView tvBtnPreview;
    private TextView tvBtnNext;

    private RelativeLayout rlPreview;
    private RelativeLayout rlNext;

    private Intent intent = null;
    private String labelText = null;
    private String frameText = null;

    private Filo app;

    private View view;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_frame_vehicle_wizard_show_results, container, false);

        app = (Filo) Objects.requireNonNull(getActivity()).getApplication();

        tvTitle = view.findViewById(R.id.tvTitle);
        tvScannedFrameText  = view.findViewById(R.id.tvScannedFrameText);
        tvBtnPreview  = view.findViewById(R.id.tvBtnPreview);
        tvBtnNext  = view.findViewById(R.id.tvBtnNext);

        rlPreview  = view.findViewById(R.id.rlPreview);
        rlNext  = view.findViewById(R.id.rlNext);

//        intent = getIntent();
//        labelText = intent.getStringExtra(Value.LABEL_TEXT_KEY);
//        frameText = intent.getStringExtra(Value.FRAME_TEXT_KEY);
//
//        tvScannedFrameText.setText(frameText);

        initListener();
        initTypeFace();

        return view;
    }

    @Override
    public void onClick(View view) {
//        Intent intent1 = null;
//        switch (view.getId()) {
//            case R.id.rlPreview:
//                intent1 = new Intent(FrameVehicleWizardShowResultsFragment.this, StartFrameScanActivity.class);
//                intent1.putExtra(Value.LABEL_TEXT_KEY, labelText);
//                startActivity(intent1);
//                finish();
//                break;
//
//            case R.id.rlNext:
//                intent1 = new Intent(FrameVehicleWizardShowResultsFragment.this, FinishScanProcessActivity.class);
//                intent1.putExtra(Value.LABEL_TEXT_KEY, labelText);
//                intent1.putExtra(Value.FRAME_TEXT_KEY, frameText);
//                startActivity(intent1);
//                finish();
//                break;
//        }
    }

    private void initListener() {
        rlPreview.setOnClickListener(this);
        rlNext.setOnClickListener(this);
    }

    private void initTypeFace() {
        tvTitle.setTypeface(app.getTypeFace());
        tvScannedFrameText.setTypeface(app.getTypeFace());
        tvBtnPreview.setTypeface(app.getTypeFace());
        tvBtnNext.setTypeface(app.getTypeFace());
    }
}
