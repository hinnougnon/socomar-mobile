package rightcom.com.filo.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import rightcom.com.filo.R;
import rightcom.com.filo.adapters.OCRFragScanTextsAdapter;
import rightcom.com.filo.app.Filo;
import rightcom.com.filo.models.SettingModel;
import rightcom.com.filo.tools.Value;
import rightcom.com.filo.window.ExitVehicleActivity;
import rightcom.com.filo.window.ReceiveVehicleActivity;

public class OCRScannerFragment extends Fragment implements OCRFragScanTextsAdapter.OnItemClickListener  {

    public static final String TAG = OCRScannerFragment.class.getSimpleName();
    private static final int requestPermissionID = 101;
    private static final int DEFAULT_VIN_LENGTH = 15;
    private static final String ARG_POSITION = "position";
    public static String PARENT_FRAG_TAG = "";
    public static String SELECTED_ACTION = "";
    public static String qrCodeGenerated = "";

    private boolean autoFocus = true;
    private boolean useFlash = true;

    private Intent intent = null;
    private String fragParentTag = null;
    private String labelText = null;
    private String selectedAction = null;
    private List<String> strings = new ArrayList<>();
    private OCRFragScanTextsAdapter adapter;

    private SurfaceView svCamera;
    private RelativeLayout rlBottom;
    private CameraSource mCameraSource;
    private TextView tvScannedText;
    private ImageView ivPlay;
    private RecyclerView rvScannedText;
    private SettingModel currentsetting;
    private View view;

    public static OCRScannerFragment newInstance(int position) {

        OCRScannerFragment f = new OCRScannerFragment();

        return f;
    }

    public static OCRScannerFragment newInstance(String fragParentTag, String labelText, String selectedAction, int position) {

        OCRScannerFragment fragment = new OCRScannerFragment();
        /*Bundle bundle = new Bundle();
        bundle.putInt(ARG_POSITION, position);
        bundle.putString(Value.FRAG_PARENT_KEY, fragParentTag);
        bundle.putString(Value.LABEL_TEXT_KEY, labelText);
        bundle.putString(Value.SELECTED_ACTION_KEY, selectedAction);
        fragment.setArguments(bundle);*/

        return fragment;
    }

//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        Bundle bundle = getArguments();
//        if (bundle != null) {
//            fragParentTag = bundle.getString(Value.FRAG_PARENT_KEY);
//            labelText = bundle.getString(Value.LABEL_TEXT_KEY);
//            selectedAction = bundle.getString(Value.SELECTED_ACTION_KEY);
//        } else {
//            Log.e(TAG, "Bundle null.");
//        }
//    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.ocr_scanner_fragment, container, false);

        svCamera = view.findViewById(R.id.svCamera);
        rlBottom = view.findViewById(R.id.rlBottom);
        tvScannedText = view.findViewById(R.id.tvScannedText);
        ivPlay = view.findViewById(R.id.ivPlay);
        rvScannedText = view.findViewById(R.id.rvScannedText);
        currentsetting=Filo.getSetting();
        initListener();
        initRecyclerView();
        startCameraSource();

//        intent = getIntent();
//        activityParentTag = intent.getStringExtra(Value.ACTIVITY_PARENT_KEY);
//        if (intent.hasExtra(Value.LABEL_TEXT_KEY)) {
//            labelText = intent.getStringExtra(Value.LABEL_TEXT_KEY);
//        }
//        if (intent.hasExtra(Value.SELECTED_ACTION_KEY)) {
//            SELECTED_ACTION = intent.getStringExtra(Value.SELECTED_ACTION_KEY);
//        }

        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode != requestPermissionID) {
            Log.d(TAG, "Got unexpected permission result: " + requestCode);
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            try {
                if (ActivityCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                mCameraSource.start(svCamera.getHolder());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void initListener() {
        tvScannedText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                checkSelectedActionAndStartProcess(tvScannedText.getText().toString());
//                LabelVehicleWizardShowResultsFragment.CurrentQRcodeGenerated = tvScannedText.getText().toString();
//                LabelVehicleWizardStartFragment.swipeto(2, null, null, "D42QIL");
                String s = tvScannedText.getText().toString();
                if (OCRScannerFragment.PARENT_FRAG_TAG != null && OCRScannerFragment.SELECTED_ACTION != null) {
                    switch (SELECTED_ACTION) {
                        /*case Value.LABEL_ACTION_KEY:
                            LabelVehicleWizardShowResultsFragment.CurrentQRcodeGenerated = s;
                            ScanCodeCheckingFragment.SELECTED_ACTION = Value.LABEL_ACTION_KEY;
                            ScanCodeCheckingFragment.FRAG_PARENT_TAG = fragParentTag;
                            LabelVehicleWizardStartFragment.swipeto(2, null, null, "D42QIL");
                            break;*/

                        case Value.RECEIVE_ACTION_KEY:
                            /*ReceiveVehicleWizardShowResultsFragment.CurrentQRcodeGenerated = s;
                            ScanCodeCheckingFragment.SELECTED_ACTION = Value.RECEIVE_ACTION_KEY;
                            ScanCodeCheckingFragment.FRAG_PARENT_TAG = fragParentTag;
                            ReceiveVehicleWizardStartFragment.swipeto(2, null, null, "D42QIL");*/
                            ReceiveVehicleWizardSecondPageFragment.VIN_SCANNED = s;
//                            ReceiveVehicleWizardPagerLayoutFragment.swipeto(2);
                            ReceiveVehicleActivity.swipeto(2);
                            break;

                        case Value.EXIT_ACTION_KEY:
//                            ExitVehicleWizardThirdPageFragment.QR_CODE_GENERATED = qrCodeGenerated;
//                            ExitVehicleWizardThirdPageFragment.OCR_CODE_GENERATED = s;
                            ExitVehicleWizardSecondPageFragment.currentOCRCode = s;
//                            ExitVehicleWizardStartFragment.swipeto(3, null, null, s);
                            ExitVehicleActivity.swipeto(1);
//                            ExitVehicleActivity.swipeto(3, null, null, s);
                            break;
                    }
                }
            }
        });
        ivPlay.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onClick(View view) {
                hidePlayButton();
                strings.clear();
//                startCameraSource();
//                LabelVehicleWizardStartFragment.refreshOCRFrag();
                if (OCRScannerFragment.PARENT_FRAG_TAG != null && OCRScannerFragment.SELECTED_ACTION != null) {
                    switch (SELECTED_ACTION) {
                        /*case Value.LABEL_ACTION_KEY:
                            LabelVehicleWizardStartFragment.refreshOCRFrag();
                            break;*/
                        case Value.RECEIVE_ACTION_KEY:
                            ReceiveVehicleActivity.refreshOCRFrag();
                            break;

                        case Value.EXIT_ACTION_KEY:
                            ExitVehicleActivity.refreshOCRFrag();
                            break;
                    }
                }
            }
        });
    }

    private void initRecyclerView() {
        rvScannedText.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvScannedText.setLayoutManager(layoutManager);
        adapter = new OCRFragScanTextsAdapter(OCRScannerFragment.this, strings);
        rvScannedText.setAdapter(adapter);
    }

    private void startCameraSource() {

        //Create the TextRecognizer
        final TextRecognizer textRecognizer = new TextRecognizer.Builder(getActivity()).build();

        if (!textRecognizer.isOperational()) {
            Log.w(TAG, "Detector dependencies not loaded yet");
        } else {
            currentsetting=Filo.getSetting();
            //Initialize camerasource to use high resolution and set Autofocus on.
            mCameraSource = new CameraSource.Builder(Objects.requireNonNull(getActivity()), textRecognizer)
                    .setFacing(CameraSource.CAMERA_FACING_BACK)
                    .setRequestedPreviewSize(1280, 1024)
                    .setAutoFocusEnabled(true)
                    .setRequestedFps(2.0f)
                    //.setFlashMode(useFlash ? Camera.Parameters.FLASH_MODE_TORCH : null)
                    //.setFocusMode(autoFocus ? Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO : null)
                    .build();

            /**
             * Add call back to SurfaceView and check if camera permission is granted.
             * If permission is granted we can start our cameraSource and pass it to surfaceView
             */
            svCamera.getHolder().addCallback(new SurfaceHolder.Callback() {
                @Override
                public void surfaceCreated(SurfaceHolder holder) {
                    try {

                        if (ActivityCompat.checkSelfPermission(getActivity(),
                                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                            requestPermissions(new String[]{Manifest.permission.CAMERA}, requestPermissionID);
                            return;
                        }
                        mCameraSource.start(svCamera.getHolder());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                }

                @Override
                public void surfaceDestroyed(SurfaceHolder holder) {
                    mCameraSource.stop();
                }
            });

            //Set the TextRecognizer's Processor.
            textRecognizer.setProcessor(new Detector.Processor<TextBlock>() {
                @Override
                public void release() {
                }

                /**
                 * Detect all the text from camera using TextBlock and the values into a stringBuilder
                 * which will then be set to the textView.
                 * */
                @Override
                public void receiveDetections(Detector.Detections<TextBlock> detections) {
                    final SparseArray<TextBlock> items = detections.getDetectedItems();
                    if (items.size() != 0 ){

                        tvScannedText.post(new Runnable() {
                            @Override
                            public void run() {
                                    showRVScanText();
                                    for(int i=0;i<items.size();i++){
                                        TextBlock item = items.valueAt(i);
                                        if (Filo.isValidText(item.getValue(),currentsetting.getocrmintextlength())) {
                                            String current_string_found = item.getValue().toUpperCase().replace("O","0");
                                            String string_with_1 = current_string_found.replace("I","1");
                                            String string_with_L = current_string_found.replace("I","L");
                                            strings.add(String.valueOf(string_with_1));
                                            strings.add(String.valueOf(string_with_L));
                                        }
                                    }
                                    if (strings.size()>0) {
                                        showPlayButton();
                                        synchronized (mCameraSource) {
                                            try {
                                                mCameraSource.release();
                                            } catch (Exception e) {
                                                Log.e(TAG, e.getMessage());
                                            }
                                        }
                                        adapter.notifyDataSetChanged();
                                    }

                            }
                        });
                    }
                }


            });
        }
    }

   /* private Boolean isValidText(String text) {
        return Pattern.matches("^[a-zA-Z0-9]{16}$", text);
    }*/

    private void hidePlayButton () {
        if (ivPlay.getVisibility() == View.VISIBLE) {
            ivPlay.setVisibility(View.GONE);
            rlBottom.setVisibility(View.GONE);
        }
    }

    private void showPlayButton() {
        if (ivPlay.getVisibility() == View.GONE) {
            ivPlay.setVisibility(View.VISIBLE);
            rlBottom.setVisibility(View.VISIBLE);
        }
    }

    private void hideRVScanText() {
        if (rvScannedText.getVisibility() == View.VISIBLE) {
            rvScannedText.setVisibility(View.GONE);
            if (tvScannedText.getVisibility() == View.GONE) {
                tvScannedText.setVisibility(View.VISIBLE);
            }
        }
    }

    private void showRVScanText() {
        if (rvScannedText.getVisibility() == View.GONE) {
            rvScannedText.setVisibility(View.VISIBLE);
            if (tvScannedText.getVisibility() == View.VISIBLE) {
                tvScannedText.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onItemClick(String s) {
//        checkSelectedActionAndStartProcess(s);
        if (OCRScannerFragment.PARENT_FRAG_TAG != null && OCRScannerFragment.SELECTED_ACTION != null) {
            switch (SELECTED_ACTION) {
/*                case Value.LABEL_ACTION_KEY:
                    LabelVehicleWizardShowResultsFragment.CurrentQRcodeGenerated = s;
                    ScanCodeCheckingFragment.SELECTED_ACTION = Value.LABEL_ACTION_KEY;
                    ScanCodeCheckingFragment.FRAG_PARENT_TAG = fragParentTag;
                    LabelVehicleWizardStartFragment.swipeto(2, null, null, "D42QIL");
                    break;*/
                case Value.RECEIVE_ACTION_KEY:
                    /*ReceiveVehicleWizardShowResultsFragment.CurrentQRcodeGenerated = s;
                    ScanCodeCheckingFragment.SELECTED_ACTION = Value.RECEIVE_ACTION_KEY;
                    ScanCodeCheckingFragment.FRAG_PARENT_TAG = fragParentTag;
                    ReceiveVehicleWizardStartFragment.swipeto(2, null, null, "D42QIL");*/
                    ReceiveVehicleWizardSecondPageFragment.VIN_SCANNED = s;
//                    ReceiveVehicleWizardPagerLayoutFragment.swipeto(2);
                    ReceiveVehicleActivity.swipeto(2);
                    break;

                case Value.EXIT_ACTION_KEY:
//                    ExitVehicleWizardThirdPageFragment.QR_CODE_GENERATED = qrCodeGenerated;
//                    ExitVehicleWizardThirdPageFragment.OCR_CODE_GENERATED = s;
                    ExitVehicleWizardSecondPageFragment.currentOCRCode = s;
//                    ExitVehicleWizardStartFragment.swipeto(3, null, null, s);
//                    ExitVehicleActivity.swipeto(3, null, null, s);
                    ExitVehicleActivity.swipeto(1);
                    break;
            }
        }
    }

    private void checkSelectedActionAndStartProcess(String capturedText) {
        Intent intent1;
        switch (selectedAction) {
//            case Value.EXIT_ACTION_KEY: // Action sortie
//                if (Objects.equals(fragParentTag, StartSelectedActionActivity.TAG)) {
//                    intent1 = new Intent(OCRScannerFragment.this, LabelScannedActivity.class);
//                    intent1.putExtra(Value.SELECTED_ACTION_KEY, Value.EXIT_ACTION_KEY);
//                    intent1.putExtra(Value.LABEL_TEXT_KEY, capturedText);
//                    startActivity(intent1);
//                    finish();
//                } else if (Objects.equals(fragParentTag, StartFrameScanActivity.TAG)) {
//                    intent1 = new Intent(OCRScannerFragment.this, FrameScannedActivity.class);
//                    if (labelText != null) {
//                        intent1.putExtra(Value.LABEL_TEXT_KEY, labelText);
//                    }
//                    intent1.putExtra(Value.FRAME_TEXT_KEY, capturedText);
//                    startActivity(intent1);
//                    finish();
//                }
//                break;

            /*case Value.LABEL_ACTION_KEY: // Action Etiquette
//                intent1 = new Intent(OCRScannerFragment.this, LabelActivity.class);
//                intent1.putExtra(Value.FRAME_TEXT_KEY, capturedText);
//                startActivity(intent1);
//                finish();
                LabelVehicleWizardStartFragment.swipeto(2, null, null, capturedText);
                break;*/
        }
    }

    public void update() {
        startCameraSource();
    }
}
