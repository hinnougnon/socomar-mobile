package rightcom.com.filo.fragments;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rightcom.com.filo.LoginPageActivity;
import rightcom.com.filo.R;
import rightcom.com.filo.api.ApiServiceGenerator;
import rightcom.com.filo.api.interfaces.ApiEndpointInterface;
import rightcom.com.filo.api.responses.CheckVINResponse;
import rightcom.com.filo.api.responses.LocationResponse;
import rightcom.com.filo.app.Filo;
import rightcom.com.filo.models.CaptureSingleton;
import rightcom.com.filo.models.ExitModel;
import rightcom.com.filo.models.UserModel;
import rightcom.com.filo.tools.Value;
import rightcom.com.filo.window.ExitVehicleActivity;
import rightcom.com.filo.window.ExitVehicleWizardCameraActivity;
import rightcom.com.filo.window.OCRScannerActivity;

public class ExitVehicleWizardSecondPageFragment extends Fragment {

    public static final String TAG = ExitVehicleWizardSecondPageFragment.class.getSimpleName();
    private static final String ARG_POSITION = "position";
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 20;
    public static final int OCR_ACTIVITY_RESULT_CODE = 3;

    public static String currentOCRCode = "";

    private UserModel currentUser;
    private ExitModel currentExit;


    private View rootView;

    private TextView tvLabelText;
    private TextView tvTreatLabel;
    private TextView tvVINText;
    private TextView tvTreatVin;
    private TextView tvExitValid;

    private ImageView ivLabelDone;
    private ImageView ivVINDone;

    private RelativeLayout rlTreatLabel;
    private RelativeLayout rlTreatVin;

    private LinearLayout llValid;

    private String qrcodecontent;
    private ProgressDialog progressDialog;

    private Filo app;

    private OnExitVehicleWizardSecondPageFragListener mListener;

    public static ExitVehicleWizardSecondPageFragment newInstance(int position) {
        ExitVehicleWizardSecondPageFragment f = new ExitVehicleWizardSecondPageFragment();
        Bundle b = new Bundle();
        b.putInt(ARG_POSITION, position);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        displayToast();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_exit_vehicle_second_page, container, false);
        ViewCompat.setElevation(rootView, 50);
        currentUser = Filo.getUSer();
        currentExit = new ExitModel();

        initView();
        initTypeFace();
        initListener();

        return rootView;
    }

    private void initView() {
        tvLabelText = rootView.findViewById(R.id.tvLabelText);
        tvTreatLabel = rootView.findViewById(R.id.tvTreatLabel);
        tvVINText = rootView.findViewById(R.id.tvVINText);
        tvTreatVin = rootView.findViewById(R.id.tvTreatVin);
        tvExitValid = rootView.findViewById(R.id.tvExitValid);

        ivLabelDone = rootView.findViewById(R.id.ivLabelDone);
        ivVINDone = rootView.findViewById(R.id.ivVINDone);

        rlTreatLabel = rootView.findViewById(R.id.rlTreatLabel);
        rlTreatVin = rootView.findViewById(R.id.rlTreatVin);

        llValid = rootView.findViewById(R.id.layoutValid);
    }

    private void initTypeFace() {
        //tvLabelText.setTypeface(app.getTypeFace());
        //tvTreatLabel.setTypeface(app.getTypeFace());
        //tvVINText.setTypeface(app.getTypeFace());
        //tvTreatVin.setTypeface(app.getTypeFace());
        //tvExitValid.setTypeface(app.getTypeFace());
    }

    private void initListener() {
        rlTreatLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Start qr code scan process
                callGoToScannerQRCodeFragment();
            }
        });

        rlTreatVin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callOCRActivity();
            }
        });

        llValid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               callGoToSaveFragment();
            }
        });
    }
    private void displayToast() {
        if(getActivity() != null && qrcodecontent != null) {

            qrcodecontent = null;
        }
    }

    // initiate QR code scan
    private void callGoToScannerQRCodeFragment() {
        currentExit = new ExitModel(); // Reset currentExit object content
        updateProcessStatus();
        checkForPermissions(); // Check permission and start qr code scan intent
    }
    // initiate VIN scanner
    private void callOCRActivity() {
        if (currentExit.getQrcode() != null && !currentExit.getQrcode().isEmpty() && currentExit.getIsvalidqrcode()) {
//            OCRScannerFragment.SELECTED_ACTION = Value.EXIT_ACTION_KEY;
//            OCRScannerFragment.PARENT_FRAG_TAG = ExitVehicleActivity.TAG;
//            ExitVehicleActivity.swipeto(2);
            //startActivityForResult(new Intent(getActivity(), OCRScannerActivity.class), OCR_ACTIVITY_RESULT_CODE);
            startImageCapture();
        } else {
            dialogBuilder("Validation", "Veuillez traiter la première étape avant celle-ci.");
        }
    }


    private static final int TAKE_PICTURE_REQUEST_B = 100;
    //private Bitmap mCameraBitmap;
    private void startImageCapture() {
        startActivityForResult(new Intent(getContext(), ExitVehicleWizardCameraActivity.class), TAKE_PICTURE_REQUEST_B);
    }







    // go to save fragment to save data
    private void callGoToSaveFragment() {
        if (currentExit.getIsvalidqrcode() && currentExit.compareQRCodeAndOCRCode()) {
            ExitVehicleWizardThirdPageFragment.currentExit = currentExit;
            ExitVehicleWizardThirdPageFragment.cargetting_location = new LocationResponse();
            ExitVehicleActivity.swipeto(3);
        } else {
            Toast.makeText(getActivity(), "Veuillez traiter les deux étapes ci-dessus.", Toast.LENGTH_LONG).show();
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        /*if (requestCode == OCR_ACTIVITY_RESULT_CODE) {
            if(data!=null){
                currentExit.setOcrcode(data.getStringExtra("OCRCODE"));
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        checkOCRCode();
                    }
                }, 1000);
            }
            else {
                Toast.makeText(getActivity(), "Aucun châssis scanné.", Toast.LENGTH_SHORT).show();
                Log.e(TAG, "Aucun châssis scanné");
            }

        }*/
        if (requestCode == TAKE_PICTURE_REQUEST_B) {
            if (resultCode == getActivity().RESULT_OK) {
                // Recycle the previous bitmap.
                /*if (mCameraBitmap != null) {
                    mCameraBitmap.recycle();
                    mCameraBitmap = null;
                }*/
                //Bundle extras = data.getExtras();
                //mCameraBitmap = (Bitmap) extras.get("data");

                //byte[] cameraData = extras.getByteArray(ExitVehicleWizardCameraActivity.EXTRA_CAMERA_DATA);
                byte[] cameraData = CaptureSingleton.getInstance().getPicture();
                CaptureSingleton.getInstance().clearData();
                if (cameraData != null) {
                    String imgString = Base64.encodeToString(cameraData,
                            Base64.NO_WRAP);
                    currentExit.setOcrcode(imgString);
                    updateProcessStatus();

                    //mCameraBitmap = BitmapFactory.decodeByteArray(cameraData, 0, cameraData.length);

                    //mCameraImageView.setImageBitmap(mCameraBitmap);
                    //mSaveImageButton.setEnabled(true);
                }
            } else {
                //mCameraBitmap = null;
                //mSaveImageButton.setEnabled(false);
            }
        }
        else {
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if(result != null) {
                if(result.getContents() != null && !result.getContents().isEmpty()) {
                    qrcodecontent = result.getContents();
                    currentExit.setQrcode(qrcodecontent);
//                checkQRCode(qrcodecontent);
                    scanCodeChecking();
                    Log.e(TAG, "QR Code retrouvé : " + qrcodecontent);
                } else {
                    Toast.makeText(getActivity(), "QR Code non retrouvé", Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "QR Code non retrouvé");
                }
                qrcodecontent = null;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // camera-related task you need to do.
                    scanFromFragment();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    //checkForPermissions();
                }
                return;
            }
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (currentOCRCode != null && !currentOCRCode.isEmpty() && isVisibleToUser) {
            currentExit.setOcrcode(currentOCRCode);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    updateProcessStatus();
                    checkOCRCode();
                }
            }, 1000);
        }
    }


    private void checkForPermissions() {
        if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            requestForPermissions();
        } else {
            // Permission has already been granted
            scanFromFragment();
        }
    }

    public void scanFromFragment() {
        IntentIntegrator.forSupportFragment(this)
                .setOrientationLocked(false).setPrompt(getResources().getString(R.string.lbl_scantheqrcode)).initiateScan();
    }

    private void requestForPermissions() {
        // No explanation needed; request the permission
        requestPermissions(new String[]{Manifest.permission.CAMERA},
                MY_PERMISSIONS_REQUEST_CAMERA);
        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
        // app-defined int constant. The callback method gets the
        // result of the request.
    }

    private void dialogBuilder(String title, String messsage) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
        alertDialog.setTitle(title);
        alertDialog.setMessage(messsage);
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog.show();
    }

//    private void checkQRCode(String qrcode){
//        // Make api call and check qr code validation
//        createAndShowProgressPopUp("Vérification de l'étiquette en cours...");
//        scanCodeChecking(qrcode);
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//
//                if (labelProcessSuccess) {
//                    labelProcessSuccess = false;
//                } else {
//                    labelProcessSuccess = true;
//
//                }
//            }
//        }, 5000);
//    }

    private void checkOCRCode() {
        // Make api call and check ocr code
        createAndShowProgressPopUp("Validation du châssis en cours...");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                closeProgressPopUp();
                updateProcessStatus();
                if (!currentExit.compareQRCodeAndOCRCode()) {
                    dialogBuilder("Echec de validation du châssis", "Désolé ! Le code récupéré n'est pas valide.");
                }
            }
        }, 3000);
    }

    private void scanCodeChecking() {
        createAndShowProgressPopUp("Validation de l'étiquette en cours...");
        ApiEndpointInterface apiService = ApiServiceGenerator.getClient().create(ApiEndpointInterface.class);
        String step = "out";
        Call<CheckVINResponse> call = apiService.lookforVIN(currentExit.getQrcode(), step, currentUser.getkey());
        call.enqueue(new Callback<CheckVINResponse>() {
            @Override
            public void onResponse(Call<CheckVINResponse>call, Response<CheckVINResponse> response) {
                if(response.isSuccessful()) {
                    CheckVINResponse data=response.body();
                    if( data.getVinkey() != null && data.getVinkey() != "" && data.getStatut().equals( Value.API_STATUS_OK) ) {
                        currentExit.setIsvalidqrcode(true);
                        closeProgressPopUp();
                    } else {
                        closeProgressPopUp();
                        currentExit=new ExitModel();
                        dialogBuilder("Echec de validation de l'étiquette", "Désolé ! Le code récupéré n'est pas valide."+data.getMsg());
                        return;
                    }
                } else {
                    closeProgressPopUp();
                    currentExit=new ExitModel();
                    dialogBuilder("Echec de validation de l'étiquette", "Le serveur a rencontré un problème lors de l'exécution de la requête.");
                    return;
                }
                updateProcessStatus();
            }
            @Override
            public void onFailure(Call<CheckVINResponse>call, Throwable t) {
                closeProgressPopUp();
                updateProcessStatus();
                currentExit=new ExitModel();
                dialogBuilder("Echec de validation de l'étiquette", "Ceci est dû à un échec lors de la connexion au serveur.");
                Log.e("Error", t.getMessage());
            }
        });
    }

    private void createAndShowProgressPopUp(String message) {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    private void closeProgressPopUp() {
        progressDialog.dismiss();
    }

    private void updateProcessStatus() {
        if (currentExit.getIsvalidqrcode()) {
            ivLabelDone.setImageDrawable(Objects.requireNonNull(getActivity()).getResources().getDrawable(R.drawable.ic_scan_code_found));
        } else {
            ivLabelDone.setImageDrawable(Objects.requireNonNull(getActivity()).getResources().getDrawable(R.drawable.ic_search_not_found));
        }
        if (ivLabelDone.getVisibility() == View.GONE) {
            ivLabelDone.setVisibility(View.VISIBLE);
        }

        if (currentExit.getOcrcode() != null && !currentExit.getOcrcode().isEmpty()) {
            ivVINDone.setImageDrawable(Objects.requireNonNull(getActivity()).getResources().getDrawable(R.drawable.ic_scan_code_found));
        }
        else {
            ivVINDone.setImageDrawable(Objects.requireNonNull(getActivity()).getResources().getDrawable(R.drawable.ic_search_not_found));
        }

        if (ivVINDone.getVisibility() == View.GONE) {
            ivVINDone.setVisibility(View.VISIBLE);
        }
    }
    

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnExitVehicleWizardSecondPageFragListener) {
            mListener = (OnExitVehicleWizardSecondPageFragListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnExitVehicleWizardSecondPageFragListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnExitVehicleWizardSecondPageFragListener {
        void go_to_exitvehicle_firstpage_fragment();
    }
}
