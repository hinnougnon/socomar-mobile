package rightcom.com.filo.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.Objects;

import rightcom.com.filo.R;
import rightcom.com.filo.app.Filo;
import rightcom.com.filo.tools.Value;
import rightcom.com.filo.window.ExitVehicleActivity;

public class ExitVehicleWizardFirstPageFragment extends Fragment {

    public static final String TAG = ExitVehicleWizardFirstPageFragment.class.getSimpleName();
    //UserModel currentUser;
    private static final String ARG_POSITION = "position";

    private View rootView;

    private TextView tvLabelTitle;
    private TextView tvLabelDescription;
    private TextView tvBottomText;
    private TextView tvNext;

    private LinearLayout llBack;
    private LinearLayout llNext;

    private int position;
    private String qrcodecontent;
    private Filo app;

    private OnExitVehicleWizardFirstPageFragment mListener;

    public static ExitVehicleWizardFirstPageFragment newInstance(int position) {
        ExitVehicleWizardFirstPageFragment f = new ExitVehicleWizardFirstPageFragment();
       Bundle b = new Bundle();
        b.putInt(ARG_POSITION, position);
        f.setArguments(b);
        return f;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        displayToast();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_exitvehicle_first_page, container, false);
        // currentUser=KoodPay.getUSer();
        ViewCompat.setElevation(rootView, 50);
        initView();
        initTypeFace();
        initListener();
        return rootView;
    }
    private void initView() {
        tvLabelTitle = rootView.findViewById(R.id.tvLabelTitle);
        tvLabelDescription = rootView.findViewById(R.id.tvLabelDescription);
        tvBottomText = rootView.findViewById(R.id.tvBottomText);
        tvNext = rootView.findViewById(R.id.tvNext);
        llBack = rootView.findViewById(R.id.layoutBack);
        llNext = rootView.findViewById(R.id.layoutBottomNext);
    }

    private void initTypeFace() {
        app = (Filo) Objects.requireNonNull(getActivity()).getApplication();
        tvLabelTitle.setTypeface(app.getTypeFace());
        tvLabelDescription.setTypeface(app.getTypeFace());
        tvBottomText.setTypeface(app.getTypeFace());
        tvNext.setTypeface(app.getTypeFace());
    }
    private void initListener() {
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getActivity(), "Retour", Toast.LENGTH_LONG).show();
                mListener.onBackClick();
            }
        });
        llNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExitVehicleActivity.swipeto(1);
            }
        });
    }

    private void displayToast() {
        if(getActivity() != null && qrcodecontent != null) {

            qrcodecontent = null;
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt(ARG_POSITION);

    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnExitVehicleWizardFirstPageFragment) {
            mListener = (OnExitVehicleWizardFirstPageFragment) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnExitVehicleWizardFirstPageFragment");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.mListener = null;
    }

    public interface OnExitVehicleWizardFirstPageFragment {
        void onBackClick();
    }
}