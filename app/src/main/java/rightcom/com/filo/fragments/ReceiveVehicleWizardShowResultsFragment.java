package rightcom.com.filo.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.WriterException;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.Objects;

import rightcom.com.filo.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rightcom.com.filo.api.responses.CheckVINResponse;
import rightcom.com.filo.app.Filo;
import rightcom.com.filo.tools.Value;

public class ReceiveVehicleWizardShowResultsFragment extends Fragment {

	private static final String ARG_POSITION = "position";
    public static CheckVINResponse checkingVINResponseObject = null;

	private int position;
//	private LinearLayout layout;
//	private TextView icon;
//	private TextView btn_receivevehicle_save;
//	private EditText txt_receivevehicle_qrcode;

	private TextView tvCode;
	private TextView tvFrame;
	private TextView tvBL;
	private TextView tvPosition;

	private TextView tvScannedCodeText;
	private TextView tvScannedFrameText;
	private TextView tvScannedBLText;

	private TextView tvBack;
	private TextView tvValid;

    private ImageView ivQRCode;
    private MKLoader mkLoader;

	private LinearLayout llBack;
	private LinearLayout llValid;

	private Spinner spinner;

	private View rootView;

	private Filo app;

	public String qrCode = "";

    private OnReceiveVehicleWizardShowResultsFragListener mListener;

	public static ReceiveVehicleWizardShowResultsFragment newInstance(int position) {
		ReceiveVehicleWizardShowResultsFragment f = new ReceiveVehicleWizardShowResultsFragment();
		Bundle b = new Bundle();
		b.putInt(ARG_POSITION, position);
		f.setArguments(b);
		return f;
	}

    public static void newInstance(String qr) {
	    Bundle bundle = new Bundle();
	    bundle.putString(Value.ARG_QR_CODE_KEY, qr);
    }

    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		position = Objects.requireNonNull(getArguments()).getInt(ARG_POSITION);
		if (getArguments() != null) {
            qrCode = getArguments().getString(Value.ARG_QR_CODE_KEY);
            if (qrCode != null)
                Log.e(Value.ARG_QR_CODE_KEY, qrCode);
        }
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_receivevehicle_showresults,
				container, false);

        app = (Filo) Objects.requireNonNull(getActivity()).getApplication();

		spinner = rootView.findViewById(R.id.spinner);

		// currentUser=KoodPay.getUSer();
		ViewCompat.setElevation(rootView, 50);
//		txt_receivevehicle_qrcode= (EditText) rootView.findViewById(R.id.txtqrcode);
//		txt_receivevehicle_qrcode.setText("Z9hs5O");
//		btn_receivevehicle_save= (TextView)rootView.findViewById(R.id.btn_receivevehicle_save);
//		btn_receivevehicle_save.setOnClickListener(buttonProcessGenerateListener );
//		CurrentQRcodeGenerated="";

        initView();
        initTypeFace();
        initListener();
		initSpinner();

		return rootView;
	}

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed()){   // only at fragment screen is resumed
            if(checkingVINResponseObject!=null && checkingVINResponseObject.getVinkey() != "" && checkingVINResponseObject.getVinkey() != null)
            {
                // create thread to avoid ANR Exception
                Thread t = new Thread(new Runnable() {
                    public void run() {
                        try {
                            synchronized (this) {
                                wait(3000);
// runOnUiThread method used to do UI task in main thread.
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            Bitmap bitmap = null;
                                            bitmap = app.encodeAsBitmap(checkingVINResponseObject.getVin());
                                            ivQRCode.setImageBitmap(bitmap);

                                            ivQRCode.setVisibility(View.VISIBLE);
                                            mkLoader.setVisibility(View.GONE);
                                        } catch (WriterException e) {
                                            e.printStackTrace();
                                        } // end of catch block
                                    } // end of run method
                                });
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
                t.start();
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnReceiveVehicleWizardShowResultsFragListener) {
            mListener = (OnReceiveVehicleWizardShowResultsFragListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnReceiveVehicleWizardShowResultsFragListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.mListener = null;
    }

    private void initView() {
        tvCode = rootView.findViewById(R.id.tvCode);
        tvFrame = rootView.findViewById(R.id.tvFrame);
        tvBL = rootView.findViewById(R.id.tvBL);
        tvPosition = rootView.findViewById(R.id.tvPosition);

        tvScannedCodeText = rootView.findViewById(R.id.tvScannedCodeText);
        tvScannedFrameText = rootView.findViewById(R.id.tvScanFrameText);
        tvScannedBLText = rootView.findViewById(R.id.tvScannedBLText);

        spinner = rootView.findViewById(R.id.spinner);

        ivQRCode = rootView.findViewById(R.id.ivRVWSRQRCode);
        mkLoader = rootView.findViewById(R.id.mRVWSRkLoader);

        tvBack = rootView.findViewById(R.id.tvBottomBackText);
        tvValid = rootView.findViewById(R.id.tvValid);

        llBack = rootView.findViewById(R.id.layoutBack);
        llValid = rootView.findViewById(R.id.layoutValid);

        tvScannedCodeText.setText(checkingVINResponseObject.getVin());
        tvScannedFrameText.setText("A55F4F5S5Q8D5F4E5");
        tvScannedBLText.setText("RF5D8E5G8H5D56S5F");
    }

    private void initTypeFace() {
        tvCode.setTypeface(app.getTypeFace());
        tvFrame.setTypeface(app.getTypeFace());
        tvBL.setTypeface(app.getTypeFace());
        tvPosition.setTypeface(app.getTypeFace());

        tvScannedCodeText.setTypeface(app.getTypeFace());
        tvScannedFrameText.setTypeface(app.getTypeFace());
        tvScannedBLText.setTypeface(app.getTypeFace());

        tvBack.setTypeface(app.getTypeFace());
        tvValid.setTypeface(app.getTypeFace());
    }

    private void initListener() {
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ReceiveVehicleWizardStartFragment.swipeto(0, null, null, null);
                mListener.go_to_receivevehicle_firstpage_fragment();
            }
        });
        llValid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), tvValid.getText().toString().toUpperCase(), Toast.LENGTH_LONG).show();
            }
        });
    }

	private void initSpinner() {
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.spinner_array, R.layout.qrcode_scan_result_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
    }

	final View.OnClickListener buttonProcessGenerateListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
//			CurrentQRcodeGenerated="";
//			if(txt_receivepaymentgeneratecode_amount.getText().toString().equals("") || txt_receivepaymentgeneratecode_amount.getText().toString().equals(" ")  ) {
//				Toast.makeText(getActivity(), R.string.fields_required,
//						Toast.LENGTH_SHORT).show();
//				return;
//			}
//			if(txt_receivepaymentgeneratecode_password.getText().toString().equals("") || txt_receivepaymentgeneratecode_password.getText().toString().equals(" ")  ) {
//				Toast.makeText(getActivity(), R.string.fields_required,
//						Toast.LENGTH_SHORT).show();
//				return;
//			}
//
//			call_DoTransaction(txt_receivepaymentgeneratecode_amount.getText().toString(),txt_receivepaymentgeneratecode_password.getText().toString());
		}
	};

	final View.OnClickListener buttonProcessScanListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
		}
	};

//	private void call_DoTransaction(String amount, String password){
//		//KoodPay.appDialog= CustomDialog.createCustomDialog(LoginPageActivity.this);
//		//KoodPay.appDialog.show();
//		ApiEndpointInterface apiService =
//				ApiServiceGenerator.getClient().create(ApiEndpointInterface.class);
//
//		Call<GeneralResponse<ReceivePaymentGenerateCodeResponse>> call = apiService.receivepayment_generatecode(currentUser.getid(),amount,password);
//		call.enqueue(new Callback<GeneralResponse<ReceivePaymentGenerateCodeResponse>>() {
//			@Override
//			public void onResponse(Call<GeneralResponse<ReceivePaymentGenerateCodeResponse>>call, Response<GeneralResponse<ReceivePaymentGenerateCodeResponse>> response) {
//				KoodPay.appDialog.dismiss();
//				if(response.isSuccessful())
//				{
//					GeneralResponse mydata=response.body();
//					if(mydata.getData().getErr()==null && mydata.getData().getResult()!=null){
//						// convert result
//						try {
//							ReceivePaymentGenerateCodeResponse resultdata=(ReceivePaymentGenerateCodeResponse)mydata.getData().getResult();
//							resetform();
//							Toast.makeText(getActivity(), R.string.transaction_succeded,
//									Toast.LENGTH_SHORT).show();
//							CurrentQRcodeGenerated=resultdata.getid();
//							ReceivePaymentFragment.swipeto(2);
//						} catch (final Exception e) {
//							e.printStackTrace();
//						}
//					}
//					else
//					{
//						Toast.makeText(getActivity(), R.string.transaction_failed,
//								Toast.LENGTH_SHORT).show();
//						return;
//					}
//				}
//				else
//				{
//					Toast.makeText(getActivity(), R.string.transaction_failed,
//							Toast.LENGTH_SHORT).show();
//					return;
//				}
//			}
//
//			@Override
//			public void onFailure(Call<GeneralResponse<ReceivePaymentGenerateCodeResponse>>call, Throwable t) {
//				// Log error here since request failed
//				KoodPay.appDialog.dismiss();
//				Toast.makeText(getActivity(), R.string.failed_connect,
//						Toast.LENGTH_SHORT).show();
//			}
//		});
//
//
//	}

	private void resetform(){
		/*txt_receivepaymentgeneratecode_amount.setText("");
		txt_receivepaymentgeneratecode_password.setText("");*/
	}

	public static void updateQRCode(String qrCode) {
		Log.e("QRCode", qrCode);
	}

	public interface OnReceiveVehicleWizardShowResultsFragListener {
        void go_to_receivevehicle_firstpage_fragment();
    }

}