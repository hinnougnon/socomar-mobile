package rightcom.com.filo.fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.balysv.materialripple.MaterialRippleLayout;

import java.io.File;
import java.util.Locale;

import rightcom.com.filo.R;
import rightcom.com.filo.tools.Utils;
import rightcom.com.filo.tools.Value;

public class StoryDetailFragment extends Fragment {

    private OnIssueStoryListener mListener;
    private MaterialRippleLayout mrlClose;
    private MaterialRippleLayout mrlAttachement;
    private TextView tvIssueName;
    private TextView tvDuration;
    private TextView tvComment;
    private TextView tvState;
    private TextView tvResponseContent;
    private RelativeLayout rlvState;
    private RelativeLayout rlvDivider;
    private RatingBar ratingBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_story_detail, container, false);
        // Inflate the layout for this fragment
        View  view = inflater.inflate(R.layout.fragment_story_detail, container, false);

        mrlClose = view.findViewById(R.id.mrlClose);
        mrlAttachement = view.findViewById(R.id.mrlAttachement);
        tvIssueName = view.findViewById(R.id.tvIssueName);
        tvDuration = view.findViewById(R.id.tvDuration);
        tvComment = view.findViewById(R.id.tvComment);
        tvState = view.findViewById(R.id.tvState);
        tvResponseContent = view.findViewById(R.id.tvResponseContent);
        rlvState = view.findViewById(R.id.rlvState);
        rlvDivider = view.findViewById(R.id.rlvDivider);
        ratingBar = view.findViewById(R.id.ratingBar);

        init(view);
//        setTypefaceToView(view);
        mrlClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*mListener.onCloseStoryDetail(((IssueModelSingleton.instance.issue_status == Value.SOLVED_STATUS)
                        && (IssueModelSingleton.instance.issue_rating <= (float)0)));*/
            }
        });
        mrlAttachement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*if (new File(IssueModelSingleton.instance.issue_image_link).exists()) {
                    showCompleteImage();
                } else {
                    Toast.makeText(getContext(), getString(R.string.text_file_not_find), Toast.LENGTH_LONG).show();
                }*/
            }
        }); {

        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnIssueStoryListener) {
            mListener = (OnIssueStoryListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnSearchFragListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void init(View view) {
        /*if (IssueModelSingleton.instance.issue_image_link.isNullOrEmpty()) {
            mrlAttachement.setVisibility(View.GONE);
        }

        if (!IssueModelSingleton.instance.issue_name_en.isNullOrEmpty()) {
            if (Locale.getDefault().getLanguage().equals("en")) {
                tvIssueName.setText(IssueModelSingleton.instance.issue_name_en);
            } else {
                tvIssueName.setText(IssueModelSingleton.instance.issue_name_fr);
            }
        } else {
            tvIssueName.setText(getString(R.string.text_no_issue_specified));
        }
        tvDuration.setText(Utils.dateDifference(getContext(), IssueModelSingleton.instance.issue_survey_date));

        if (IssueModelSingleton.instance.issue_comment.isNullOrEmpty()) {
            tvComment.setText(getContext().getResources().getString(R.string.text_aucun_commentaire));
        } else {
            tvComment.setText(IssueModelSingleton.instance.issue_comment);
        }

        if (IssueModelSingleton.instance.issue_status == Value.NEW_STATUS) {
            rlvState.setBackgroundResource(R.drawable.background_new_issue);
            tvState.setText(getContext().getString(R.string.text_new));
        } else if (IssueModelSingleton.instance.issue_status == Value.PENDING_STATUS) {
            rlvState.setBackgroundResource(R.drawable.background_pending_issue);
            tvState.setText(getContext().getString(R.string.text_en_cours));
        } else if (IssueModelSingleton.instance.issue_status == Value.SOLVED_STATUS) {
            rlvState.setBackgroundResource(R.drawable.background_solved_issue);
            tvState.setText(getContext().getString(R.string.text_resolu));
        }

        if (IssueModelSingleton.instance.issue_status != Value.SOLVED_STATUS) {
            rlvDivider.setVisibility(View.GONE);
            tvResponseContent.setVisibility(View.GONE);
            ratingBar.setVisibility(View.GONE);
        } else {
            tvResponseContent.setText(getContext().getString(R.string.text_template_response));
            ratingBar.setRating(IssueModelSingleton.instance.issue_rating);
            if (IssueModelSingleton.instance.issue_rating <= 0) {
                ratingBar.setVisibility(View.GONE);
            }
        }*/
    }

    private void showCompleteImage() {
        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_show_complete_image);

        ImageView img_picture = dialog.findViewById(R.id.img_picture);
        ImageView mrlUpdateImage = dialog.findViewById(R.id.mrlUpdateImage);

//        img_picture.setImageBitmap(BitmapFactory.decodeFile(IssueModelSingleton.instance.issue_image_link));
        mrlUpdateImage.setVisibility(View.GONE);

        dialog.show();
    }

    public interface OnIssueStoryListener {
        void onCloseStoryDetail(Boolean showRatingDialog);
    }
}
