package rightcom.com.filo.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rightcom.com.filo.R;
import rightcom.com.filo.adapters.ReceiveVehiclePageAdapter;
import rightcom.com.filo.tools.Value;

public class ReceiveVehicleWizardStartFragment extends Fragment  {

	public static final String TAG = ReceiveVehicleWizardStartFragment.class.getSimpleName();
	//UserModel currentUser;
	private ReceiveVehiclePageAdapter adapter;
	private static ViewPager pager;
	private TextView previousButton;
	private TextView nextButton;
	private TextView navigator;
	private int currentItem;

	private EditText txt_addmoneyrechargecode_code;
	private TextView btnaddmoneyrechargecode_scan;
	private TextView btnaddmoneyrechargecode_type;

	public ReceiveVehicleWizardStartFragment(){}

	public static ReceiveVehicleWizardStartFragment newInstance() {
		return new ReceiveVehicleWizardStartFragment();
	}

	private ReceiveVehicleWizardStartFragment instance_created;

    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View rootView = inflater.inflate(R.layout.fragment_receivevehicle_start, container, false);
		//currentUser=KoodPay.getUSer();
		instance_created=this;
		currentItem = 0;

		pager = (ViewPager) rootView.findViewById(R.id.activity_wizard_universal_pager);
		previousButton = (TextView) rootView.findViewById(R.id.activity_wizard_universal_previous);
		nextButton = (TextView) rootView.findViewById(R.id.activity_wizard_universal_next);
		navigator = (TextView) rootView.findViewById(R.id.activity_wizard_universal_possition);

		adapter = new ReceiveVehiclePageAdapter( getChildFragmentManager());
		pager.setAdapter(adapter);
		pager.setCurrentItem(currentItem);

		setNavigator();

		pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int position) {
				// TODO Auto-generated method stub
				setNavigator();
			}
		});

		previousButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*if (pager.getCurrentItem() != 0) {
					pager.setCurrentItem(pager.getCurrentItem() - 1);
				}
				setNavigator();*/
				Toast.makeText(getActivity(), "Skip",
						Toast.LENGTH_SHORT).show();
			}
		});

		nextButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (pager.getCurrentItem() != (pager.getAdapter().getCount() - 1)) {
					pager.setCurrentItem(pager.getCurrentItem() + 1);
				} else {
					Toast.makeText(getActivity(), "Finish",
							Toast.LENGTH_SHORT).show();
				}
				setNavigator();
			}
		});
		return rootView;
	}

    @Override
    public void onActivityCreated(final Bundle savedState) {
        super.onActivityCreated(savedState);
    }

	public void setNavigator() {
		String navigation = "";
		for (int i = 0; i < adapter.getCount(); i++) {
			if (i == pager.getCurrentItem()) {
				navigation += getString(R.string.material_icon_point_full)
						+ "  ";
			} else {
				navigation += getString(R.string.material_icon_point_empty)
						+ "  ";
			}
		}
		navigator.setText(navigation);
	}

	public void setCurrentSlidePosition(int position) {
		this.currentItem = position;
	}

	public int getCurrentSlidePosition() {
		return this.currentItem;
	}

	public static void swipeto(int position, String parentFragTag, String selectedAction, String qr) {
        switch (position) {
            case 0:
                pager.setCurrentItem(0);
                Objects.requireNonNull(pager.getAdapter()).notifyDataSetChanged();
                break;
            case 1:
                if (pager != null) {
                    OCRScannerFragment.newInstance(parentFragTag, null, selectedAction, position);
                    pager.setCurrentItem(position);
                    Objects.requireNonNull(pager.getAdapter()).notifyDataSetChanged();
                }
                break;
            case 2:
                if (pager != null) {
                    ScanCodeCheckingFragment.newInstance(position);
                    pager.setCurrentItem(position);
                    Objects.requireNonNull(pager.getAdapter()).notifyDataSetChanged();
                }
                break;

            case 3:
                if (pager != null) {
                    ReceiveVehicleWizardShowResultsFragment.newInstance(position);
                    pager.setCurrentItem(position);
                    Objects.requireNonNull(pager.getAdapter()).notifyDataSetChanged();
                }
                break;
        }
//		ReceiveVehicleWizardShowResultsFragment.newInstance(qr);
//		pager.setCurrentItem(position);
//		Objects.requireNonNull(pager.getAdapter()).notifyDataSetChanged();
	}

	public static void refreshOCRFrag() {
		Objects.requireNonNull(pager.getAdapter()).notifyDataSetChanged();
	}

	final View.OnClickListener buttonProcessTypeListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {


		}
	};

	final View.OnClickListener buttonProcessScanListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
		}
	};

	public interface OnReceiveFragListener {
		void onTvReceiveFragTextClick(String s);
	}

}