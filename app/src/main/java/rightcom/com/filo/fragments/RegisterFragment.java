package rightcom.com.filo.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.Objects;

import rightcom.com.filo.R;


public class RegisterFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnRegisterFragListener mListener;

    private View view;
    private MaterialRippleLayout mrlCreateSimpleAccount;
    private MaterialRippleLayout mrlArrowBack;

    private EditText edtLastName;
    private EditText edtFirstname;
    private EditText edtPseudo;
    private EditText edtMail;
    private EditText edtPassword;

    public RegisterFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RegisterFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RegisterFragment newInstance(String param1, String param2) {
        RegisterFragment fragment = new RegisterFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_register, container, false);

        init();
        setTypefaceToView();
        initListener();

        return view;
    }

    private void init() {
        mrlCreateSimpleAccount = view.findViewById(R.id.mrlCreateSimpleAccount);
        mrlArrowBack = view.findViewById(R.id.mrlArrowBack);

        edtLastName = view.findViewById(R.id.edtLastName);
        edtFirstname = view.findViewById(R.id.edtFirstname);
        edtPseudo = view.findViewById(R.id.edtPseudo);
        edtMail = view.findViewById(R.id.edtMail);
        edtPassword = view.findViewById(R.id.edtPassword);
    }

    private void setTypefaceToView() {
        /*view.tilLastName.setTypeface(TypefaceManager.getRegularTypeFace(context))
        view.tilFirstname.setTypeface(TypefaceManager.getRegularTypeFace(context))
        view.tilPseudo.setTypeface(TypefaceManager.getRegularTypeFace(context))
        view.tilMail.setTypeface(TypefaceManager.getRegularTypeFace(context))
        view.tilPassword.setTypeface(TypefaceManager.getRegularTypeFace(context))
        view.edtLastName.typeface = TypefaceManager.getRegularTypeFace(context)
        view.edtFirstname.typeface = TypefaceManager.getRegularTypeFace(context)
        view.edtPseudo.typeface = TypefaceManager.getRegularTypeFace(context)
        view.edtMail.typeface = TypefaceManager.getRegularTypeFace(context)
        view.edtPassword.typeface = TypefaceManager.getRegularTypeFace(context)
        view.tvCreateAccount.typeface = TypefaceManager.getRegularTypeFace(context)*/
//        view.tvInscriptionWithOther.typeface = TypefaceManager.getRegularTypeFace(context)
    }

    private void initListener() {
        mrlCreateSimpleAccount.setOnClickListener(mrlCreateSimpleAccountListener);
        mrlArrowBack.setOnClickListener(mrlArrowBackListener);
    }

    private Boolean controlDataAndRequestRegistration() {

        if (edtLastName.getText().toString().isEmpty()) {
            edtLastName.setError(getString(R.string.text_saisir_nom));
            return false;
        } else if (edtFirstname.getText().toString().isEmpty()) {
            edtLastName.setError(getString(R.string.text_saisir_nom));
            return false;
        } else if (edtPseudo.getText().toString().isEmpty()) {
            edtPseudo.setError(getString(R.string.text_saisr_pseudo));
            return false;
        } else if (edtMail.getText().toString().isEmpty()) {
            edtMail.setError(getString(R.string.text_saisir_mail));
            return false;
        } else if (edtPassword.getText().toString().isEmpty()) {
            edtPassword.setError(getString(R.string.text_password));
            return false;
        } else {
            /*val userRegistrationModel = UserRegistrationModel()
            userRegistrationModel.email = view.edtMail.text.toString()
            userRegistrationModel.lastname = view.edtLastName.text.toString()
            userRegistrationModel.firstname = view.edtFirstname.text.toString()
            userRegistrationModel.password = view.edtPassword.text.toString()
            userRegistrationModel.confirm_password = userRegistrationModel.password
            userRegistrationModel.registration_type = Value.TYPE_NORMAL*/
//                userRegistrationModel.phone_data = Utils.getPhoneData(activity, SharedPrefrencesManger.getDeviceId(context)!!)
            mListener.onRegistered("Everything doing well.");
            return true;
        }
    }

    private View.OnClickListener mrlCreateSimpleAccountListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            controlDataAndRequestRegistration();
        }
    };

    private View.OnClickListener mrlArrowBackListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Objects.requireNonNull(getActivity()).onBackPressed();
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnRegisterFragListener) {
            mListener = (OnRegisterFragListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnToLoginFragListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnRegisterFragListener {
        void onRegistered(String userRegistrationModel);
    }
}
