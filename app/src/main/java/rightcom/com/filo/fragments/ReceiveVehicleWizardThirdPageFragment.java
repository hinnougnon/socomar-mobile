package rightcom.com.filo.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.WriterException;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rightcom.com.filo.R;
import rightcom.com.filo.adapters.LocationAdapter;
import rightcom.com.filo.api.ApiServiceGenerator;
import rightcom.com.filo.api.interfaces.ApiEndpointInterface;
import rightcom.com.filo.api.responses.CheckVINResponse;
import rightcom.com.filo.api.responses.LocationResponse;
import rightcom.com.filo.app.Filo;
import rightcom.com.filo.models.UserModel;
import rightcom.com.filo.tools.Value;
import rightcom.com.filo.window.ReceiveVehicleActivity;

public class ReceiveVehicleWizardThirdPageFragment extends Fragment {
    UserModel currentUser;
	private static final String ARG_POSITION = "position";
    public static CheckVINResponse checkingVINResponseObject = new CheckVINResponse();

	private int position;
	private TextView txtVIN;
	private TextView txtBL;
    private TextView txtDescription;

	private TextView tvBack;
	private TextView tvValid;

	private LinearLayout llBack;
	private LinearLayout llValid;

	private AutoCompleteTextView spinner_position;
    private LocationAdapter locationAdapter;

    LocationResponse currentlocation=null;

	private View rootView;

	private Filo app;


    private OnReceiveVehicleWizardThirdPageFragListener mListener;

	public static ReceiveVehicleWizardThirdPageFragment newInstance(int position) {
		ReceiveVehicleWizardThirdPageFragment f = new ReceiveVehicleWizardThirdPageFragment();
		Bundle b = new Bundle();
		b.putInt(ARG_POSITION, position);
		f.setArguments(b);
		return f;
	}

    public static void newInstance(String qr) {
	    Bundle bundle = new Bundle();
	    //bundle.putString(Value.ARG_QR_CODE_KEY, qr);
    }

    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// position = Objects.requireNonNull(getArguments()).getInt(ARG_POSITION);
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_receivevehicle_third_page,
				container, false);

        app = (Filo) Objects.requireNonNull(getActivity()).getApplication();

		 currentUser=Filo.getUSer();
		ViewCompat.setElevation(rootView, 50);

        initView();
        initTypeFace();
        initListener();
		initSpinner();

		return rootView;
	}

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed()){   // only at fragment screen is resumed

            if(checkingVINResponseObject!=null && checkingVINResponseObject.getVinkey() != "" && checkingVINResponseObject.getVinkey() != null)
            {

                txtVIN.setText(checkingVINResponseObject.getVin());
                txtBL.setText(checkingVINResponseObject.getBL());
                txtDescription.setText(checkingVINResponseObject.getDescription());

                locationAdapter = new LocationAdapter(getContext(), R.layout.custom_autocomplete_row, checkingVINResponseObject.getLocations());
                spinner_position.setThreshold(1);
                spinner_position.setAdapter(locationAdapter);
                // handle click event and set desc on textview
                spinner_position.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                          ReceiveVehicleWizardFourthPageFragment.cargetting_location = (LocationResponse) adapterView.getItemAtPosition(i);
                    }
                });
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnReceiveVehicleWizardThirdPageFragListener) {
            mListener = (OnReceiveVehicleWizardThirdPageFragListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnReceiveVehicleWizardThirdPageFragListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.mListener = null;
    }

    private void initView() {
        txtVIN = rootView.findViewById(R.id.txtVIN);
        txtBL = rootView.findViewById(R.id.txtBL);
        txtDescription = rootView.findViewById(R.id.txtDESCRIPTION);
        spinner_position = rootView.findViewById(R.id.spinner_position);
        tvBack = rootView.findViewById(R.id.tvBottomBackText);
        tvValid = rootView.findViewById(R.id.tvValid);

        llBack = rootView.findViewById(R.id.layoutBack);
        llValid = rootView.findViewById(R.id.layoutValid);
    }

    private void initTypeFace() {
        txtVIN.setTypeface(app.getTypeFace());
        txtBL.setTypeface(app.getTypeFace());
        txtDescription.setTypeface(app.getTypeFace());
        tvBack.setTypeface(app.getTypeFace());
        tvValid.setTypeface(app.getTypeFace());
    }

    private void initListener() {
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.go_to_receivevehicle_firstpage_fragment();
            }
        });
        llValid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ReceiveVehicleWizardFourthPageFragment.cargetting_location ==null ) {
				Toast.makeText(getActivity(), R.string.position_required,
						Toast.LENGTH_SHORT).show();
				return;
                }
                if( ReceiveVehicleWizardFourthPageFragment.cargetting_location.getlockey().equals("")  ) {
                    Toast.makeText(getActivity(), R.string.position_required,
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                if(ReceiveVehicleWizardFourthPageFragment.cargetting_location.getlockey().toString().equals(" ")  ) {
                    Toast.makeText(getActivity(), R.string.position_required,
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                ReceiveVehicleWizardFourthPageFragment.checkingVINResponseObject=checkingVINResponseObject;
                ReceiveVehicleActivity.swipeto(4);
            }
        });
    }

	private void initSpinner() {
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.spinner_array, R.layout.qrcode_scan_result_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner_position.setAdapter(adapter);
    }

	public interface OnReceiveVehicleWizardThirdPageFragListener {
        void go_to_receivevehicle_firstpage_fragment();
    }

}