package rightcom.com.filo.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rightcom.com.filo.R;
import rightcom.com.filo.api.ApiServiceGenerator;
import rightcom.com.filo.api.interfaces.ApiEndpointInterface;
import rightcom.com.filo.api.responses.CheckVINResponse;
import rightcom.com.filo.app.Filo;
import rightcom.com.filo.models.UserModel;
import rightcom.com.filo.tools.Value;

public class ScanCodeCheckingFragment extends Fragment {

    UserModel currentUser;
    public static final String TAG = ScanCodeCheckingFragment.class.getSimpleName();
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_POSITION = "position";

    public static String SELECTED_ACTION = null;
    public static String FRAG_PARENT_TAG = null;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnScanCodeFragListener mListener;

    private TextView tvTitle;
    private TextView tvSubTitle;
    private TextView tvBack;

    private ImageView ivSearch;
//    private ImageView ivFound;
    private ImageView ivNotFound;

    private LinearLayout llBack;

    private View view;

    private Filo app;

    public ScanCodeCheckingFragment() {
        // Required empty public constructor
    }

    public static ScanCodeCheckingFragment newInstance(int position) {
        ScanCodeCheckingFragment fragment = new ScanCodeCheckingFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    public static ScanCodeCheckingFragment newInstance(String param1, String param2) {
        ScanCodeCheckingFragment fragment = new ScanCodeCheckingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_scan_code_checking, container, false);
        currentUser=Filo.getUSer();
        initView();
        call_VINSearch();
        initTypeFace();
        initListener();

        return view;
    }

    private void initView() {
        tvTitle = view.findViewById(R.id.tvTitle);
        tvSubTitle = view.findViewById(R.id.tvSubTitle);
        tvBack = view.findViewById(R.id.tvBack);

        ivSearch = view.findViewById(R.id.ivSearch);
//        ivFound = view.findViewById(R.id.ivDone);
        ivNotFound = view.findViewById(R.id.ivFailed);

        llBack = view.findViewById(R.id.layoutBack);
    }
    private void call_VINSearch(){
        //KoodPay.appDialog= CustomDialog.createCustomDialog(LoginPageActivity.this);
        //KoodPay.appDialog.show();
        ApiEndpointInterface apiService =
                ApiServiceGenerator.getClient().create(ApiEndpointInterface.class);

        //String VIN= ReceiveVehicleWizardShowResultsFragment.CurrentQRcodeGenerated;
        String step= "in";
        Call<CheckVINResponse> call = apiService.lookforVIN("d",step,currentUser.getkey());
        call.enqueue(new Callback<CheckVINResponse>() {
            @Override
            public void onResponse(Call<CheckVINResponse>call, Response<CheckVINResponse> response) {
                //KoodPay.appDialog.dismiss();
                if(response.isSuccessful())
                {
                    CheckVINResponse data=response.body();
                    if(data.getVinkey()!=null && data.getVinkey()!=""){
                        // convert result
                        try {
                            Toast.makeText(getContext(), "save",
                                    Toast.LENGTH_SHORT).show();
                            ReceiveVehicleWizardShowResultsFragment.checkingVINResponseObject=data;
                            //ReceiveVehicleWizardShowResultsFragment.s.sw.s.swipeto(2);
                            //startActivity(new Intent("rightcom.com.filo.LeftMenusShopActivity"));
                        } catch (final Exception e) {
                            e.printStackTrace();
                        }


                    }
                    else
                    {
                        Toast.makeText(getContext(), R.string.login_incorrect,
                                Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                else
                {
                    Toast.makeText(getContext(), R.string.login_incorrect,
                            Toast.LENGTH_SHORT).show();
                    return;
                }
            }
            @Override
            public void onFailure(Call<CheckVINResponse>call, Throwable t) {
                // Log error here since request failed
                //KoodPay.appDialog.dismiss();
                Toast.makeText(getActivity(), R.string.failed_connect,
                        Toast.LENGTH_SHORT).show();
            }
        });


    }
    private void initTypeFace() {
        app = (Filo) Objects.requireNonNull(getActivity()).getApplication();
        tvTitle.setTypeface(app.getTypeFace());
        tvSubTitle.setTypeface(app.getTypeFace());
        tvBack.setTypeface(app.getTypeFace());
    }

    private void initListener() {
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*if (ivSearch.getVisibility() == View.VISIBLE) {
                    ivSearch.setVisibility(View.GONE);
                    ivNotFound.setVisibility(View.VISIBLE);
                    tvTitle.setText("Résultat(s)");
                    tvSubTitle.setText("Aucun élément retrouvé");
                } else if (ivNotFound.getVisibility() == View.VISIBLE) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (ScanCodeCheckingFragment.SELECTED_ACTION != null) {
                                switch (SELECTED_ACTION) {
                                    case Value.LABEL_ACTION_KEY:
                                        LabelVehicleWizardStartFragment.swipeto(3, null, null, "D42QIL");
                                        break;
                                    case Value.RECEIVE_ACTION_KEY:
                                        ReceiveVehicleWizardStartFragment.swipeto(3, null, null, "D42QIL");
                                        break;
                                }
                            }
                        }
                    }, 1000);
                }*/
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnScanCodeFragListener) {
            mListener = (OnScanCodeFragListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnScanCodeFragListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnScanCodeFragListener {
        void onSearchBackClick();
    }
}
