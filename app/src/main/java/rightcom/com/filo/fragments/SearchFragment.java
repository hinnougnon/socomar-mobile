package rightcom.com.filo.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Objects;

import rightcom.com.filo.R;
import rightcom.com.filo.app.Filo;

public class SearchFragment extends Fragment implements View.OnClickListener {

    public static final String TAG = SearchFragment.class.getSimpleName();
    private OnSearchFragListener mListener;

    private View view;

    private TextView txtviewhowtosearch;
    private TextView btnScanEtiquette;
    private TextView btnScanChassis;
    private TextView btnvehicleposition;
    private TextView btnvehiclechassis;

    private Filo app;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_search, container, false);

        initView();
        initTypeFace();
        initListener();

        return view;
    }

    private void initView() {
        txtviewhowtosearch = view.findViewById(R.id.txtviewhowtosearch);
        btnScanEtiquette = view.findViewById(R.id.btnScanEtiquette);
        btnScanChassis = view.findViewById(R.id.btnScanChassis);
        btnvehicleposition = view.findViewById(R.id.btnvehicleposition);
        btnvehiclechassis = view.findViewById(R.id.btnvehiclechassis);
    }

    private void initTypeFace() {
        app = (Filo) Objects.requireNonNull(getActivity()).getApplication();
        txtviewhowtosearch.setTypeface(app.getTypeFace());
        btnScanEtiquette.setTypeface(app.getTypeFace());
        btnScanChassis.setTypeface(app.getTypeFace());
        btnvehicleposition.setTypeface(app.getTypeFace());
        btnvehiclechassis.setTypeface(app.getTypeFace());
    }

    private void initListener() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSearchFragListener) {
            mListener = (OnSearchFragListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnSearchFragListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {

    }

    public interface OnSearchFragListener {
        void onTvSearchFragTextClick(String s);
    }
}
