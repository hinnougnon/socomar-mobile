package rightcom.com.filo.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.Objects;

import rightcom.com.filo.R;
import rightcom.com.filo.app.Filo;
import rightcom.com.filo.tools.Value;

public class ReceiveVehicleOCRFragment extends Fragment {

    public static final String TAG = ReceiveVehicleOCRFragment.class.getSimpleName();
    //UserModel currentUser;
    private static final String ARG_POSITION = "position";
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 20;

    private View rootView;

    private TextView tvReceiveTitle;
    private TextView tvReceiveDescription;
    private TextView tvBottomText;
    private TextView tvNext;

    private LinearLayout llBack;
    private LinearLayout llNext;

    private int position;
    private String qrcodecontent;
    private Filo app;

    private OnReceiveVehicleOCRFragListener mListener;

    public static ReceiveVehicleOCRFragment newInstance(int position) {
        ReceiveVehicleOCRFragment f = new ReceiveVehicleOCRFragment();
        Bundle b = new Bundle();
        b.putInt(ARG_POSITION, position);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        displayToast();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_receivevehicle_ocr, container, false);
        // currentUser=KoodPay.getUSer();
        ViewCompat.setElevation(rootView, 50);
        initView();
        initTypeFace();
        initListener();
//        TextView btnscan = (TextView) rootView.findViewById(R.id.btnscan);
//        btnscan.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                checkForPermissions();
//            }
//        });
        return rootView;
    }

    private void initView() {
        tvReceiveTitle = rootView.findViewById(R.id.tvReceiveTitle);
        tvReceiveDescription = rootView.findViewById(R.id.tvReceiveDescription);
        tvBottomText = rootView.findViewById(R.id.tvBottomText);
        tvNext = rootView.findViewById(R.id.tvNext);
        llBack = rootView.findViewById(R.id.layoutBack);
        llNext = rootView.findViewById(R.id.layoutBottomNext);
    }

    private void initTypeFace() {
        app = (Filo) Objects.requireNonNull(getActivity()).getApplication();
        tvReceiveTitle.setTypeface(app.getTypeFace());
        tvReceiveDescription.setTypeface(app.getTypeFace());
        tvBottomText.setTypeface(app.getTypeFace());
        tvNext.setTypeface(app.getTypeFace());
    }

    private void initListener() {
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getActivity(), "Retour", Toast.LENGTH_LONG).show();
               mListener.onBackClick();
            }
        });
        llNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callGoToOCRFragment();
            }
        });
    }

    public void scanFromFragment() {
        IntentIntegrator.forSupportFragment(this)
                .setOrientationLocked(false).setPrompt(getResources().getString(R.string.lbl_scantheqrcode)).initiateScan();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() != null) {
                qrcodecontent = result.getContents();
                call_go_to_show_results(qrcodecontent);
                Toast.makeText(getActivity(), "QR Code retrouvé:"+qrcodecontent,
                        Toast.LENGTH_SHORT).show();
            }
            else{
                Toast.makeText(getActivity(), "QR Code non retrouvé",
                        Toast.LENGTH_SHORT).show();
            }
            qrcodecontent=null;
        }
    }

    private void displayToast() {
        if(getActivity() != null && qrcodecontent != null) {

            qrcodecontent = null;
        }
    }

    private void call_go_to_show_results(String qrcode){
        ReceiveVehicleWizardStartFragment.swipeto(1, TAG, Value.RECEIVE_ACTION_KEY, qrcode);
    }

    private void callGoToOCRFragment() {
        OCRScannerFragment.SELECTED_ACTION = Value.RECEIVE_ACTION_KEY;
        OCRScannerFragment.PARENT_FRAG_TAG = ReceiveVehicleOCRFragment.TAG;
        ReceiveVehicleWizardStartFragment.swipeto(1, TAG, Value.RECEIVE_ACTION_KEY, null);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt(ARG_POSITION);

    }

    private void checkForPermissions() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            requestForPermissions();
        } else {
            // Permission has already been granted
            scanFromFragment();
        }
    }

    private void requestForPermissions() {
        // No explanation needed; request the permission
        requestPermissions(new String[]{Manifest.permission.CAMERA},
                MY_PERMISSIONS_REQUEST_CAMERA);
        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
        // app-defined int constant. The callback method gets the
        // result of the request.
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnReceiveVehicleOCRFragListener) {
            mListener = (OnReceiveVehicleOCRFragListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnReceiveVehicleOCRFragListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.mListener = null;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // camera-related task you need to do.
                    scanFromFragment();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    //checkForPermissions();
                }
                return;
            }
        }
    }

    public interface OnReceiveVehicleOCRFragListener {
        void onBackClick();
    }
}