package rightcom.com.filo.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rightcom.com.filo.R;
import rightcom.com.filo.api.ApiServiceGenerator;
import rightcom.com.filo.api.interfaces.ApiEndpointInterface;
import rightcom.com.filo.api.responses.CheckVINResponse;
import rightcom.com.filo.app.Filo;
import rightcom.com.filo.dialogs.CustomDialog;
import rightcom.com.filo.models.UserModel;
import rightcom.com.filo.printers.zebra.ZebraLabelPrinter;
import rightcom.com.filo.tools.Value;
import rightcom.com.filo.window.LabelVehicleActivity;

import static rightcom.com.filo.tools.Value.WIDTH;

public class LabelVehicleWizardSecondPageFragment extends Fragment {

    UserModel currentUser;
    public static String VIN_PROVIDED = "";

    public static final String TAG = LabelVehicleWizardSecondPageFragment.class.getSimpleName();
    private static final String ARG_POSITION = "position";
    public static CheckVINResponse checkingVINResponseObject ;
    private boolean isFound = false;


    private OnLabelVehicleWizardSecondPageFragment mListener;

    private TextView tvTitle;
    private TextView tvSubTitle;
    private TextView tvBack;
    private LinearLayout llPrint;
    private ImageView ivQRCode;
    private MKLoader mkLoader;

    private ImageView ivSearch;
//    private ImageView ivFound;
    private ImageView ivNotFound;

    private LinearLayout llBack;

    private View view;

    private Filo app;

    public LabelVehicleWizardSecondPageFragment() {
        // Required empty public constructor
    }

    public static LabelVehicleWizardSecondPageFragment newInstance(int position) {
        LabelVehicleWizardSecondPageFragment fragment = new LabelVehicleWizardSecondPageFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    public static LabelVehicleWizardSecondPageFragment newInstance(String param1, String param2) {
        LabelVehicleWizardSecondPageFragment fragment = new LabelVehicleWizardSecondPageFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_labelvehicle_second_page, container, false);

        initView();
        initTypeFace();
        initListener();

        return view;
    }

    private void initView() {
        tvTitle = view.findViewById(R.id.tvTitleLabelsecondpage);
        tvSubTitle = view.findViewById(R.id.tvDescriptionLabelsecondpage);
        tvBack = view.findViewById(R.id.tvBackLabelsecondpage);
        ivSearch = view.findViewById(R.id.ivSearchLabelsecondpage);
        ivNotFound = view.findViewById(R.id.ivFailedLabelsecondpage);
        llBack = view.findViewById(R.id.layoutBackLabelsecondpage);
        llPrint = view.findViewById(R.id.layoutPrint);
        ivQRCode = view.findViewById(R.id.ivRVWSRQRCode);
        mkLoader = view.findViewById(R.id.mRVWSRkLoader);
    }

    private void showUINoresults(String error_message){

        ivQRCode.setVisibility(View.GONE);
        llPrint.setVisibility(View.GONE);
        ivNotFound.setVisibility(View.VISIBLE);
        ivSearch.setVisibility(View.GONE);
        mkLoader.setVisibility(View.GONE);
        tvTitle.setText("Résultat(s)");
        tvSubTitle.setText("Aucun élément retrouvé. \n"+error_message);
    }

    private void call_Search(){
        if(VIN_PROVIDED!=null && !VIN_PROVIDED.equals("")){

            String VIN= VIN_PROVIDED;
            if(VIN!=null && VIN!=""){
                ApiEndpointInterface apiService =
                        ApiServiceGenerator.getClient().create(ApiEndpointInterface.class);

                //VIN_SCANNED="s0l1ne42";//
                String step= "in";
                Call<CheckVINResponse> call = apiService.lookforVIN(VIN,step,currentUser.getkey());
                call.enqueue(new Callback<CheckVINResponse>() {
                    @Override
                    public void onResponse(Call<CheckVINResponse>call, Response<CheckVINResponse> response) {
                        //KoodPay.appDialog.dismiss();
                        if(response.isSuccessful())
                        {
                            CheckVINResponse data=response.body();
                            checkingVINResponseObject=data;
                            if(data.getVinkey()!=null && data.getVinkey()!="" && data.getStatut().equals( Value.API_STATUS_OK)){
                                // convert result
                                try {
                                    isFound = true;
                                    generateQRCode();
                                    //LabelVehicleWizardThirdPageFragment.checkingVINResponseObject=data;
                                    //LabelVehicleActivity.swipeto(2);
                                } catch (final Exception e) {
                                    e.printStackTrace();
                                }


                            }
                            else
                            {
                                showUINoresults(data.getMsg());
                                return;
                            }
                        }
                        else
                        {

                            showUINoresults("Le serveur a rencontré un problème lors de l'exécution de la requête.");
                            return;
                        }
                    }
                    @Override
                    public void onFailure(Call<CheckVINResponse>call, Throwable t) {
                        showUINoresults("Echec lors de la connexion au serveur.");
                    }
                });
            }
            else{
                showUINoresults("Le châssis n'a pas été retrouvé.");
            }

        }
        else{
            showUINoresults("QR Code invalide car le code récupéré est vide.");
        }
    }

    private void generateQRCode() {
        // create thread to avoid ANR Exception
        Thread t = new Thread(new Runnable() {
            public void run() {
                try {
                    synchronized (this) {
                        wait(3000);
// runOnUiThread method used to do UI task in main thread.
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Bitmap bitmap = null;
                                    //bitmap = encodeAsBitmap(checkingVINResponseObject.getVin() +"_"+checkingVINResponseObject.getBL());
                                    bitmap = encodeAsBitmap(checkingVINResponseObject.getVin() +"_"+checkingVINResponseObject.getBL());
                                    ivQRCode.setImageBitmap(bitmap);
                                    //tvTitle.setText("Sauvegarde terminée");
                                    //tvSubTitle.setText("");
                                    // tvPrint.setText("imprimer");
                                    ivQRCode.setVisibility(View.VISIBLE);
                                    llPrint.setVisibility(View.VISIBLE);
                                    ivNotFound.setVisibility(View.GONE);
                                    ivSearch.setVisibility(View.GONE);
                                    mkLoader.setVisibility(View.GONE);
                                } catch (WriterException e) {
                                    e.printStackTrace();
                                } // end of catch block
                            } // end of run method
                        });
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
    }
    public Bitmap encodeAsBitmap(String str) throws WriterException {
        BitMatrix result;
        try {
            result = new MultiFormatWriter().encode(str,
                    BarcodeFormat.QR_CODE, WIDTH, WIDTH, null);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int w = result.getWidth();
        int h = result.getHeight();
        int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++) {
            int offset = y * w;
            for (int x = 0; x < w; x++) {
                pixels[offset + x] = result.get(x, y) ? getResources().getColor(R.color.colorBlackFilterOpac166):getResources().getColor(R.color.colorWhite);
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, 500, 0, 0, w, h);
        return bitmap;
    } /// end of this method
    @Override
    public void setUserVisibleHint(boolean visible){
        super.setUserVisibleHint(visible);
        if (visible){   // only at fragment screen is resumed
            currentUser=Filo.getUSer();
            call_Search();
        }
    }

    private void initTypeFace() {
        app = (Filo) Objects.requireNonNull(getActivity()).getApplication();
        tvTitle.setTypeface(app.getTypeFace());
        tvSubTitle.setTypeface(app.getTypeFace());
        tvBack.setTypeface(app.getTypeFace());
    }

    private void initListener() {
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LabelVehicleActivity.swipeto(0);
            }
        });
        llPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFound) {
                    //Toast.makeText(getActivity(), tvPrint.getText().toString().toUpperCase(), Toast.LENGTH_LONG).show();
                    callPrint();
                    // mListener.go_to_receivevehicle_firstpage_fragment();
                } else {

                }
            }
        });
    }
    private boolean sendData = true;
    public void callPrint() {
        Filo.appDialog= CustomDialog.callLoadingDialog(getContext());
        Filo.appDialog.show();
        new Thread(new Runnable() {
            public void run() {
                Looper.prepare();
                //connectAndSendJob("UYDTUYTYUDT","kjsdhjkh");
                ZebraLabelPrinter objzebra=new ZebraLabelPrinter();
                objzebra.connectAndSendLabelJob(checkingVINResponseObject.getVin(),checkingVINResponseObject.getBL());
                Looper.loop();
                Looper.myLooper().quit();
                sendData = true;
            }
        }).start();

    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnLabelVehicleWizardSecondPageFragment) {
            mListener = (OnLabelVehicleWizardSecondPageFragment) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnLabelVehicleWizardSecondPageFragment");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnLabelVehicleWizardSecondPageFragment {
        void onBackClick();
    }

}
