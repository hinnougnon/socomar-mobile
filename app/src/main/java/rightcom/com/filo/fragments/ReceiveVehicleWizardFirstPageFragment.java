package rightcom.com.filo.fragments;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.Objects;

import rightcom.com.filo.R;
import rightcom.com.filo.app.Filo;
import rightcom.com.filo.dialogs.DialogChooseOCRorType;
import rightcom.com.filo.dialogs.DialogTypeVIN;
import rightcom.com.filo.tools.Value;
import rightcom.com.filo.window.ReceiveVehicleActivity;

public class ReceiveVehicleWizardFirstPageFragment extends Fragment {

    public static final String TAG = ReceiveVehicleWizardFirstPageFragment.class.getSimpleName();
    //UserModel currentUser;
    private static final String ARG_POSITION = "position";
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 20;

    private View rootView;

    private TextView tvReceiveTitle;
    private TextView tvReceiveDescription;
    private TextView tvBottomText;
    private TextView tvNext;

    private FloatingActionButton fabEditCode;
    private LinearLayout llBack;
    private LinearLayout llNext;

    private int position;
    private String qrcodecontent;
    private Filo app;

    private Dialog dialog;

    private OnReceiveVehicleWizardFirstPageFragListener mListener;

    public static ReceiveVehicleWizardFirstPageFragment newInstance(int position) {
        ReceiveVehicleWizardFirstPageFragment f = new ReceiveVehicleWizardFirstPageFragment();
        Bundle b = new Bundle();
        b.putInt(ARG_POSITION, position);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        displayToast();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_receivevehicle_first_page, container, false);

        app = (Filo) getActivity().getApplication();

        // currentUser=KoodPay.getUSer();
        ViewCompat.setElevation(rootView, 50);
        initView();
        initTypeFace();
        initListener();
        return rootView;
    }

    private void initView() {
        tvReceiveTitle = rootView.findViewById(R.id.tvReceiveTitle);
        tvReceiveDescription = rootView.findViewById(R.id.tvReceiveDescription);
        tvBottomText = rootView.findViewById(R.id.tvBottomText);
        tvNext = rootView.findViewById(R.id.tvNext);
        llBack = rootView.findViewById(R.id.layoutBack);
        llNext = rootView.findViewById(R.id.layoutBottomNext);
    }

    private void initTypeFace() {
        tvReceiveTitle.setTypeface(app.getTypeFace());
        tvReceiveDescription.setTypeface(app.getTypeFace());
        tvBottomText.setTypeface(app.getTypeFace());
        tvNext.setTypeface(app.getTypeFace());
    }

    private void initListener() {
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(getActivity(), "Retour", Toast.LENGTH_LONG).show();
               mListener.onBackClick();
            }
        });
        llNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowDoChoicePopup();
            }
        });
    }



    public void createAndShowPopUp() {
        DialogTypeVIN objdialog =new DialogTypeVIN(this);
        objdialog.showDialog();
        /*dialog = new Dialog(getActivity());
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_type_VIN);

        TextView tvPopUpTitle = dialog.findViewById(R.id.tvPopupTitle);
        TextView tvPopUpInfo = dialog.findViewById(R.id.tvInfo);

        final TextInputLayout tilUserPwd = dialog.findViewById(R.id.tilUserPassword);
        final TextInputLayout tilIpAddress = dialog.findViewById(R.id.tilVIN);

        RelativeLayout rlCancel = dialog.findViewById(R.id.rlCancel);
        TextView tvCancel = dialog.findViewById(R.id.tvCancel);

        RelativeLayout rlSave = dialog.findViewById(R.id.rlSave);
        TextView tvSave = dialog.findViewById(R.id.tvSave);

        *//*Set dialog TextView TypeFace*//*
        tvPopUpTitle.setTypeface(app.getTypeFace());
        tvPopUpInfo.setTypeface(app.getTypeFace());
        tilUserPwd.setTypeface(app.getTypeFace());
        tvCancel.setTypeface(app.getTypeFace());
        tvSave.setTypeface(app.getTypeFace());

        tvPopUpTitle.setText("Châssis");
        tvPopUpInfo.setText("Veuillez saisir le code du châssis.");
        tilIpAddress.setHint("Code châssis");

        tilIpAddress.setVisibility(View.VISIBLE);
        tilUserPwd.setVisibility(View.GONE);

        rlCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        rlSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String editCode = tilIpAddress.getEditText().getText().toString();
                if (Filo.isValidText(editCode)) {
                    dialog.dismiss();
                    ReceiveVehicleWizardSecondPageFragment.VIN_SCANNED = editCode;
                    ReceiveVehicleActivity.swipeto(2);
                } else {
                    tilIpAddress.setError("Veuillez saisir un code valide, s'il vous plaît.");
                    tilIpAddress.getEditText().setText("");
                }
            }
        });
        dialog.show();*/
    }

    private void displayToast() {
        if(getActivity() != null && qrcodecontent != null) {

            qrcodecontent = null;
        }
    }

    private void ShowDoChoicePopup() {
       DialogChooseOCRorType objdialog =new DialogChooseOCRorType(this);
       objdialog.showDialog();
    }
    public void callGoToOCRFragment() {
        OCRScannerFragment.SELECTED_ACTION = Value.RECEIVE_ACTION_KEY;
        OCRScannerFragment.PARENT_FRAG_TAG = ReceiveVehicleWizardFirstPageFragment.TAG;
//        ReceiveVehicleWizardPagerLayoutFragment.swipeto(1);
        //ReceiveVehicleWizardStartFragment.swipeto(1, TAG, Value.RECEIVE_ACTION_KEY, null);
        ReceiveVehicleActivity.swipeto(1);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt(ARG_POSITION);

    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnReceiveVehicleWizardFirstPageFragListener) {
            mListener = (OnReceiveVehicleWizardFirstPageFragListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnReceiveVehicleWizardFirstPageFragListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.mListener = null;
    }

    public interface OnReceiveVehicleWizardFirstPageFragListener {
        void onBackClick();
    }
}