package rightcom.com.filo.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rightcom.com.filo.R;
import rightcom.com.filo.api.ApiServiceGenerator;
import rightcom.com.filo.api.interfaces.ApiEndpointInterface;
import rightcom.com.filo.api.responses.CargettingResponse;
import rightcom.com.filo.api.responses.LocationResponse;
import rightcom.com.filo.app.Filo;
import rightcom.com.filo.models.ExitModel;
import rightcom.com.filo.models.UserModel;
import rightcom.com.filo.tools.Value;

import static rightcom.com.filo.tools.Value.WIDTH;

public class ExitVehicleWizardThirdPageFragment extends Fragment {

    public static final String TAG = ExitVehicleWizardThirdPageFragment.class.getSimpleName();
    private static final String ARG_POSITION = "position";

    //public static ExitModel checkingVINResponseObject ;
    public static LocationResponse cargetting_location ;

    public static String QR_CODE_GENERATED = "";
    public static String OCR_CODE_GENERATED = "";

    private int position;

    public static ExitModel currentExit;
    public UserModel currentUser;

    private boolean isSaveExit = false;

//    private TextView tvTitle;
//    private TextView tvLabel;
//    private TextView tvScannedLabelText;
//    private TextView tvFrame;
//    private TextView tvScannedFrameText;
//    private TextView tvBtnFinish;
//    private RelativeLayout rlFinish;
//
//    private Intent intent = null;
//    private String labelText = null;
//    private String frameText = null;

    private TextView tvTitle;
    private TextView tvSubTitle;

    private ImageView ivQRCode;
    private ImageView ivFailed;
    private MKLoader mkLoader;

    private LinearLayout llBack;
    private LinearLayout llPrint;
    private TextView tvBack;
    private TextView tvPrint;

    private View view;

    private Filo app;

    private OnExitVehicleWizardThirdPageFragListener mListener;

    public static ExitVehicleWizardThirdPageFragment newInstance(int position) {

        ExitVehicleWizardThirdPageFragment fragment = new ExitVehicleWizardThirdPageFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_POSITION, position);
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = Objects.requireNonNull(getArguments()).getInt(ARG_POSITION);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_finish_scan_process, container, false);

        //app = (Filo) Objects.requireNonNull(getActivity()).getApplication();


        ViewCompat.setElevation(view, 50);

        initView();
        initListener();
        initTypeFace();

        return view;
    }

    private void showSuccessPage() {
        // create thread to avoid ANR Exception
        tvTitle.setText("Sauvegarde terminée");
        tvSubTitle.setText("");
        tvPrint.setText("Fermer");
        llPrint.setVisibility(View.VISIBLE);
    }



    private void showFailedTosave(String msg_server){
        tvTitle.setText("Echec de sauvegarde");
        tvSubTitle.setText("L'opération de sortie a échouée. "+msg_server);
        ivFailed.setVisibility(View.VISIBLE);
        ivQRCode.setVisibility(View.GONE);
        mkLoader.setVisibility(View.GONE);
        llPrint.setVisibility(View.VISIBLE);
    }

    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible){   // only at fragment screen is resumed
            currentUser = Filo.getUSer();
            call_SaveExit();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnExitVehicleWizardThirdPageFragListener) {
            this.mListener = (OnExitVehicleWizardThirdPageFragListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnExitVehicleWizardThirdPageFragListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void initView() {
        tvTitle = view.findViewById(R.id.tvTitle);
        tvSubTitle = view.findViewById(R.id.tvSubTitle);

        ivQRCode = view.findViewById(R.id.ivEVWSRQRCode);
        ivFailed = view.findViewById(R.id.ivFailed);
        mkLoader = view.findViewById(R.id.mEVWSRkLoader);

        tvBack = view.findViewById(R.id.tvBottomBackText);
        tvPrint = view.findViewById(R.id.tvPrint);

        llBack = view.findViewById(R.id.layoutBack);
        llPrint = view.findViewById(R.id.layoutPrint);
        llPrint.setVisibility(View.GONE);
    }

    private void initListener() {
        llPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isSaveExit) {
                    mListener.onCloseClick();
                } else {
                    tvTitle.setText("Sauvegarde en cours...");
                    tvSubTitle.setText("Nous sommes entrain de sauvegarder l'opération. Veuillez patienter...");
                    ivFailed.setVisibility(View.GONE);
                    llPrint.setVisibility(View.GONE);
                    call_SaveExit();
                }
            }
        });
    }

    private void initTypeFace() {
        //tvTitle.setTypeface(app.getTypeFace());
        //tvSubTitle.setTypeface(app.getTypeFace());
        //tvBack.setTypeface(app.getTypeFace());
        //tvPrint.setTypeface(app.getTypeFace());
//        tvLabel.setTypeface(app.getTypeFace());
//        tvFrame.setTypeface(app.getTypeFace());
//        tvScannedFrameText.setTypeface(app.getTypeFace());
//        tvScannedLabelText.setTypeface(app.getTypeFace());
//        tvBtnFinish.setTypeface(app.getTypeFace());
    }

    private void call_SaveExit(){
        if(currentExit != null && currentExit.getQrcode() != null && !currentExit.getQrcode().isEmpty() && currentUser!=null){
            ApiEndpointInterface apiService = ApiServiceGenerator.getClient().create(ApiEndpointInterface.class);
            String mvt = "out";
            Call<CargettingResponse> call = apiService.cargetting(currentExit.getQrcode(), mvt, cargetting_location.getlockey(),currentExit.getOcrcode(), currentUser.getkey());
            call.enqueue(new Callback<CargettingResponse>() {
                @Override
                public void onResponse(Call<CargettingResponse>call, Response<CargettingResponse> response) {
                    if(response.isSuccessful()) {
                        CargettingResponse data=response.body();
                        if(data.getStatut()!=null && data.getStatut()!="" && data.getStatut().equals(Value.API_STATUS_OK)){
                            isSaveExit = true;
                            showSuccessPage();
                        } else {
                            showFailedTosave(data.getMsg());
                            return;
                        }
                    } else {
                        showFailedTosave("Le serveur a rencontré un problème lors de l'exécution de la requête.");
                        return;
                    }
                }
                @Override
                public void onFailure(Call<CargettingResponse>call, Throwable t) {
                    Log.e("Error", t.getMessage());
                    showFailedTosave("Ceci est dû à un échec lors de la connexion au serveur.");
                }
            });
        }
    }

    public interface OnExitVehicleWizardThirdPageFragListener {
        void onCloseClick();
    }
}
