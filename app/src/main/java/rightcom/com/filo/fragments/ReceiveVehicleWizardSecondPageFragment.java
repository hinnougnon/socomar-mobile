package rightcom.com.filo.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rightcom.com.filo.R;
import rightcom.com.filo.api.ApiServiceGenerator;
import rightcom.com.filo.api.interfaces.ApiEndpointInterface;
import rightcom.com.filo.api.responses.CheckVINResponse;
import rightcom.com.filo.app.Filo;
import rightcom.com.filo.models.UserModel;
import rightcom.com.filo.tools.Value;
import rightcom.com.filo.window.ReceiveVehicleActivity;

public class ReceiveVehicleWizardSecondPageFragment extends Fragment {

    UserModel currentUser;
    public static String VIN_SCANNED = "";

    public static final String TAG = ReceiveVehicleWizardSecondPageFragment.class.getSimpleName();
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_POSITION = "position";

    public static String SELECTED_ACTION = null;
    public static String FRAG_PARENT_TAG = null;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnReceiveVehicleWizardSecondPageFragListener mListener;

    private TextView tvTitle;
    private TextView tvSubTitle;
    private TextView tvBack;

    private ImageView ivSearch;
//    private ImageView ivFound;
    private ImageView ivNotFound;

    private LinearLayout llBack;

    private View view;

    private Filo app;

    public ReceiveVehicleWizardSecondPageFragment() {
        // Required empty public constructor
    }

    public static ReceiveVehicleWizardSecondPageFragment newInstance(int position) {
        ReceiveVehicleWizardSecondPageFragment fragment = new ReceiveVehicleWizardSecondPageFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    public static ReceiveVehicleWizardSecondPageFragment newInstance(String param1, String param2) {
        ReceiveVehicleWizardSecondPageFragment fragment = new ReceiveVehicleWizardSecondPageFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_receivevehicle_second_page, container, false);
//        currentUser=Filo.getUSer();

        initView();
        initTypeFace();
        initListener();

        return view;
    }

    private void initView() {
        tvTitle = view.findViewById(R.id.tvTitle);
        tvSubTitle = view.findViewById(R.id.tvSubTitle);
        tvBack = view.findViewById(R.id.tvBack);

        ivSearch = view.findViewById(R.id.ivSearch);
//        ivFound = view.findViewById(R.id.ivDone);
        ivNotFound = view.findViewById(R.id.ivFailed);

        llBack = view.findViewById(R.id.layoutBack);
    }

    private void showUINoresults(String error_message){
        ivSearch.setVisibility(View.GONE);
        ivNotFound.setVisibility(View.VISIBLE);
        tvTitle.setText("Résultat(s)");
        tvSubTitle.setText("Aucun élément retrouvé. "+error_message);
    }

    private void call_VINSearch(){
        //KoodPay.appDialog= CustomDialog.createCustomDialog(LoginPageActivity.this);
        //KoodPay.appDialog.show();
        if(VIN_SCANNED!=null && VIN_SCANNED!=""){
            ApiEndpointInterface apiService =
                    ApiServiceGenerator.getClient().create(ApiEndpointInterface.class);

            //VIN_SCANNED="s0l1ne42";//
            String step= "in";
            Call<CheckVINResponse> call = apiService.lookforVIN(VIN_SCANNED,step,currentUser.getkey());
            call.enqueue(new Callback<CheckVINResponse>() {
                @Override
                public void onResponse(Call<CheckVINResponse>call, Response<CheckVINResponse> response) {
                    //KoodPay.appDialog.dismiss();
                    if(response.isSuccessful())
                    {
                        CheckVINResponse data=response.body();
                        if(data.getVinkey()!=null && data.getVinkey()!="" && data.getStatut().equals( Value.API_STATUS_OK)){
                            // convert result
                            try {
//                                Toast.makeText(getContext(), "save", Toast.LENGTH_SHORT).show();
                                ReceiveVehicleWizardThirdPageFragment.checkingVINResponseObject=data;
//                                ReceiveVehicleWizardPagerLayoutFragment.swipeto(3);
                                ReceiveVehicleActivity.swipeto(3);
                                //startActivity(new Intent("rightcom.com.filo.LeftMenusShopActivity"));
                            } catch (final Exception e) {
                                e.printStackTrace();
                            }


                        }
                        else
                        {

                            /*CheckVINResponse datavin=new CheckVINResponse();
                            datavin.setVinkey("192611");
                            datavin.setVin("S0L1NE42");
                            ReceiveVehicleWizardThirdPageFragment.checkingVINResponseObject= new CheckVINResponse();
                            ReceiveVehicleWizardThirdPageFragment.checkingVINResponseObject=datavin;*/

                            showUINoresults(data.getMsg());
//                            ReceiveVehicleWizardPagerLayoutFragment.swipeto(3);
                            //ReceiveVehicleActivity.swipeto(3);
                            return;
                        }
                    }
                    else
                    {
                        /*CheckVINResponse datavin=new CheckVINResponse();
                        datavin.setVinkey("192611");
                        datavin.setVin("S0L1NE42");
                        ReceiveVehicleWizardThirdPageFragment.checkingVINResponseObject= new CheckVINResponse();
                        ReceiveVehicleWizardThirdPageFragment.checkingVINResponseObject=datavin;*/

                        showUINoresults("Le serveur a rencontré un problème lors de l'exécution de la requête.");
//                        ReceiveVehicleWizardPagerLayoutFragment.swipeto(3);
                        //ReceiveVehicleActivity.swipeto(3);
                        return;
                    }
                }
                @Override
                public void onFailure(Call<CheckVINResponse>call, Throwable t) {
                    // Log error here since request failed
                    //KoodPay.appDialog.dismiss();
                    showUINoresults("Echec lors de la connexion au serveur.");
                }
            });
        }
    }

    @Override
    public void setUserVisibleHint(boolean visible){
        super.setUserVisibleHint(visible);
        if (visible){   // only at fragment screen is resumed
            currentUser=Filo.getUSer();
            call_VINSearch();
        }
    }

    private void initTypeFace() {
        app = (Filo) Objects.requireNonNull(getActivity()).getApplication();
        tvTitle.setTypeface(app.getTypeFace());
        tvSubTitle.setTypeface(app.getTypeFace());
        tvBack.setTypeface(app.getTypeFace());
    }

    private void initListener() {
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ReceiveVehicleWizardPagerLayoutFragment.swipeto(0);
                ReceiveVehicleActivity.swipeto(0);
                /*if (ivSearch.getVisibility() == View.VISIBLE) {
                    ivSearch.setVisibility(View.GONE);
                    ivNotFound.setVisibility(View.VISIBLE);
                    tvTitle.setText("Résultat(s)");
                    tvSubTitle.setText("Aucun élément retrouvé");
                } else if (ivNotFound.getVisibility() == View.VISIBLE) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (ScanCodeCheckingFragment.SELECTED_ACTION != null) {
                                switch (SELECTED_ACTION) {
                                    case Value.LABEL_ACTION_KEY:
                                        LabelVehicleWizardStartFragment.swipeto(3, null, null, "D42QIL");
                                        break;
                                    case Value.RECEIVE_ACTION_KEY:
                                        ReceiveVehicleWizardStartFragment.swipeto(3, null, null, "D42QIL");
                                        break;
                                }
                            }
                        }
                    }, 1000);
                }*/
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnReceiveVehicleWizardSecondPageFragListener) {
            mListener = (OnReceiveVehicleWizardSecondPageFragListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnReceiveVehicleWizardSecondPageFragListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnReceiveVehicleWizardSecondPageFragListener {
        void go_to_receivevehicle_firstpage_fragment();
    }

}
