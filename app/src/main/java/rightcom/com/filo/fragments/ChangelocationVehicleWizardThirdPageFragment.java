package rightcom.com.filo.fragments;

import android.content.Context;
import android.opengl.Visibility;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rightcom.com.filo.R;
import rightcom.com.filo.adapters.LocationAdapter;
import rightcom.com.filo.api.ApiServiceGenerator;
import rightcom.com.filo.api.interfaces.ApiEndpointInterface;
import rightcom.com.filo.api.responses.CargettingResponse;
import rightcom.com.filo.api.responses.CheckVINResponse;
import rightcom.com.filo.api.responses.LocationResponse;
import rightcom.com.filo.app.Filo;
import rightcom.com.filo.dialogs.CustomDialog;
import rightcom.com.filo.models.UserModel;
import rightcom.com.filo.tools.Value;
import rightcom.com.filo.window.ChangeLocationVehicleActivity;

public class ChangelocationVehicleWizardThirdPageFragment extends Fragment {

    UserModel currentUser;
    private static final String ARG_POSITION = "position";
    public static CheckVINResponse checkingVINResponseObject = new CheckVINResponse();
    public  LocationResponse cargetting_location ;
    private int position;
    private TextView txtVIN;
    private TextView txtBL;
    private TextView txtDescription;
    private TextView tvBack;
    private TextView tvValid;
    private LinearLayout llBack;
    private LinearLayout llValid;
    private RelativeLayout rlformresults;
    private RelativeLayout rlFormSuccess;
    private LinearLayout llBackAftersuccesschangelocationthirdpage;
    private AutoCompleteTextView spinner_position;
    private LocationAdapter locationAdapter;
    LocationResponse currentlocation=null;
    public static final String TAG = ChangelocationVehicleWizardThirdPageFragment.class.getSimpleName();
    private OnChangelocationVehicleWizardthirdPageFragment mListener;
    private View view;

    private Filo app;

    public ChangelocationVehicleWizardThirdPageFragment() {
        // Required empty public constructor
    }

    public static ChangelocationVehicleWizardThirdPageFragment newInstance(int position) {
        ChangelocationVehicleWizardThirdPageFragment fragment = new ChangelocationVehicleWizardThirdPageFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    public static ChangelocationVehicleWizardThirdPageFragment newInstance(String param1, String param2) {
        ChangelocationVehicleWizardThirdPageFragment fragment = new ChangelocationVehicleWizardThirdPageFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_changelocationvehicle_third_page, container, false);

        initView();
        initTypeFace();
        initListener();
        return view;
    }

    private void initView() {
        txtVIN = view.findViewById(R.id.txtVINchangelocationthirdpage);
        txtBL = view.findViewById(R.id.txtBLchangelocationthirdpage);
        txtDescription = view.findViewById(R.id.txtDESCRIPTIONchangelocationthirdpage);
        spinner_position = view.findViewById(R.id.spinner_positionchangelocationthirdpage);
        llBack = view.findViewById(R.id.layoutBackchangelocationthirdpage);
        llValid = view.findViewById(R.id.layoutSavechangelocation);
        rlformresults = view.findViewById(R.id.rlformresults);
        rlFormSuccess = view.findViewById(R.id.rlFormSuccess);
        llBackAftersuccesschangelocationthirdpage= view.findViewById(R.id.layoutBackAftersuccesschangelocationthirdpage);
        rlformresults.setVisibility(View.VISIBLE);
        rlFormSuccess.setVisibility(View.GONE);
    }




    @Override
    public void setUserVisibleHint(boolean visible){
        super.setUserVisibleHint(visible);
        if (visible){   // only at fragment screen is resumed
            currentUser=Filo.getUSer();
            if(checkingVINResponseObject!=null && checkingVINResponseObject.getVinkey() != "" && checkingVINResponseObject.getVinkey() != null)
            {

                txtVIN.setText(checkingVINResponseObject.getVin());
                txtBL.setText(checkingVINResponseObject.getBL());
                txtDescription.setText(checkingVINResponseObject.getDescription());

                locationAdapter = new LocationAdapter(getContext(), R.layout.custom_autocomplete_row, checkingVINResponseObject.getLocations());
                spinner_position.setThreshold(1);
                spinner_position.setAdapter(locationAdapter);
                // handle click event and set desc on textview
                spinner_position.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        cargetting_location = (LocationResponse) adapterView.getItemAtPosition(i);
                    }
                });
            }
        }
    }

    private void initTypeFace() {
        app = (Filo) Objects.requireNonNull(getActivity()).getApplication();
        //tvTitle.setTypeface(app.getTypeFace());
        //tvSubTitle.setTypeface(app.getTypeFace());
        //tvBack.setTypeface(app.getTypeFace());
    }

    private void initListener() {
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangeLocationVehicleActivity.refreshFrag();
                ChangeLocationVehicleActivity.swipeto(0);
            }
        });
        llValid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cargetting_location==null){
                    Toast.makeText(getActivity(), "Veuillez sélectionner une position valide", Toast.LENGTH_LONG).show();
                    return;
                }
                call_Save();
            }
        });
        llBackAftersuccesschangelocationthirdpage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChangeLocationVehicleActivity.refreshFrag();
                ChangeLocationVehicleActivity.swipeto(0);
            }
        });
    }

    private void call_Save(){

        if(checkingVINResponseObject!=null && checkingVINResponseObject.getVinkey() != "" && checkingVINResponseObject.getVinkey() != null && cargetting_location!=null){

            Filo.appDialog= CustomDialog.callLoadingDialog(getActivity());
            Filo.appDialog.show();

            ApiEndpointInterface apiService =
                    ApiServiceGenerator.getClient().create(ApiEndpointInterface.class);

            String mvt="loc";
            Call<CargettingResponse> call = apiService.cargetting(checkingVINResponseObject.getVin(),mvt,cargetting_location.getlockey(),"",currentUser.getkey());
            call.enqueue(new Callback<CargettingResponse>() {
                @Override
                public void onResponse(Call<CargettingResponse>call, Response<CargettingResponse> response) {
                    Filo.appDialog.dismiss();
                    if(response.isSuccessful())
                    {
                        CargettingResponse data=response.body();
                        if(data.getStatut()!=null && data.getStatut()!="" && data.getStatut().equals( Value.API_STATUS_OK)){
                            // convert result
                            try {
                                rlformresults.setVisibility(View.GONE);
                                rlFormSuccess.setVisibility(View.VISIBLE);


                            } catch (final Exception e) {
                                e.printStackTrace();
                            }
                        }
                        else
                        {
                            showFailedTosave(data.getMsg());
                            return;
                        }
                    }
                    else
                    {
                        showFailedTosave("Le serveur a rencontré un problème lors de l'exécution de la requête.");
                        return;
                    }
                }
                @Override
                public void onFailure(Call<CargettingResponse>call, Throwable t) {
                    // Log error here since request failed
                    Filo.appDialog.dismiss();
                    showFailedTosave("Ceci est dû à un échec lors de la connexion au serveur.");
                }
            });
        }
    }

    private void showFailedTosave(String message){
        Filo.appDialog= CustomDialog.createSahasraraDialog(getActivity(),"Erreur",message,true,false);
        Filo.appDialog.show();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnChangelocationVehicleWizardthirdPageFragment) {
            mListener = (OnChangelocationVehicleWizardthirdPageFragment) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnChangelocationVehicleWizardthirdPageFragment");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnChangelocationVehicleWizardthirdPageFragment {
        void onBackClick();
    }

}
