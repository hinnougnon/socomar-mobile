package rightcom.com.filo.fragments;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.content.pm.PackageManager;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import rightcom.com.filo.R;
import rightcom.com.filo.app.Filo;
import rightcom.com.filo.window.ChangeLocationVehicleActivity;

import java.util.Objects;


public class ChangelocationVehicleWizardFirstPageFragment extends Fragment {

    public static final String TAG = ChangelocationVehicleWizardFirstPageFragment.class.getSimpleName();
    //UserModel currentUser;
    private static final String ARG_POSITION = "position";
    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 20;

    private View rootView;

    private TextView tvTitle;
    private TextView tvDescription;
    private TextView tvBack;
    private TextView tvNext;

    private FloatingActionButton fabEditCode;
    private LinearLayout llBack;
    private LinearLayout llNext;

    private int position;
    private String qrcodecontent;
    private Filo app;

    private Dialog dialog;

    private OnChangelocationVehicleWizardFirstPageFragListener mListener;

    public static ChangelocationVehicleWizardFirstPageFragment newInstance(int position) {
        ChangelocationVehicleWizardFirstPageFragment f = new ChangelocationVehicleWizardFirstPageFragment();
        Bundle b = new Bundle();
        b.putInt(ARG_POSITION, position);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        displayToast();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_changelocationvehicle_first_page, container, false);

        app = (Filo) getActivity().getApplication();

        ViewCompat.setElevation(rootView, 50);
        initView();
        initTypeFace();
        initListener();
        return rootView;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            if(result.getContents() != null) {
                qrcodecontent = result.getContents();
                ChangelocationVehicleWizardSecondPageFragment.QRCodeSCANNED = qrcodecontent;
                ChangeLocationVehicleActivity.swipeto(1);
            }
            else{
                Toast.makeText(getActivity(), "QR Code non retrouvé", Toast.LENGTH_SHORT).show();
            }
            qrcodecontent = null;
        }
    }
    private void initView() {
        tvTitle = rootView.findViewById(R.id.tvChangelocationTitle);
        tvDescription = rootView.findViewById(R.id.tvChangelocationDescription);
        tvBack = rootView.findViewById(R.id.tvBackChangelocation);
        tvNext = rootView.findViewById(R.id.tvNextChangelocation);
        llBack = rootView.findViewById(R.id.layoutBottomBackChangelocation);
        llNext = rootView.findViewById(R.id.layoutBottomNextChangelocation);
    }

    private void initTypeFace() {
        tvTitle.setTypeface(app.getTypeFace());
        tvDescription.setTypeface(app.getTypeFace());
        tvBack.setTypeface(app.getTypeFace());
        tvNext.setTypeface(app.getTypeFace());
    }

    private void initListener() {
        llBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onBackClick();
            }
        });
        llNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkForPermissions();
            }
        });
    }
    private void checkForPermissions() {
        if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            requestForPermissions();
        } else {
            // Permission has already been granted
            scanFromFragment();
        }
    }

    private void requestForPermissions() {
        // No explanation needed; request the permission
        requestPermissions(new String[]{Manifest.permission.CAMERA},
                MY_PERMISSIONS_REQUEST_CAMERA);
        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
        // app-defined int constant. The callback method gets the
        // result of the request.
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // camera-related task you need to do.
                    scanFromFragment();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    //checkForPermissions();
                }
                return;
            }
        }
    }
    public void scanFromFragment() {
        IntentIntegrator.forSupportFragment(this)
                .setOrientationLocked(false).setPrompt(getResources().getString(R.string.lbl_scantheqrcode)).initiateScan();
    }
    private void displayToast() {
        if(getActivity() != null && qrcodecontent != null) {

            qrcodecontent = null;
        }
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments().getInt(ARG_POSITION);

    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnChangelocationVehicleWizardFirstPageFragListener) {
            mListener = (OnChangelocationVehicleWizardFirstPageFragListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnChangelocationVehicleWizardFirstPageFragListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.mListener = null;
    }

    public interface OnChangelocationVehicleWizardFirstPageFragListener {
        void onBackClick();
    }
}