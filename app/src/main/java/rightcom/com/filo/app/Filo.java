package rightcom.com.filo.app;

import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import rightcom.com.filo.R;
import rightcom.com.filo.models.PrinterModel;
import rightcom.com.filo.models.SettingModel;
import rightcom.com.filo.models.UserModel;
import rightcom.com.filo.tools.Utils;
import rightcom.com.filo.tools.Value;

import static rightcom.com.filo.tools.Value.WIDTH;

public class Filo extends Application {

    public static final String TAG = Filo.class.getSimpleName();
    private Filo appInstance;
    private static Context mContext;
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor sharedPreferencesEditor;
    public static Dialog appDialog;
    public static final int DEFAULT_VIN_MIN_LENGTH = 5;
    public static final int DEFAULT_VIN_MAX_LENGTH = 20;
    private static Context iContext;
    @Override
    public void onCreate() {
        super.onCreate();
        appInstance = this;
        iContext = this;
         setContext(getApplicationContext());
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        sharedPreferencesEditor = sharedPreferences.edit();
        setContext(getApplicationContext());
        saveInPreference();
    }

/*    public static Context getContext() {
        return iContext;
    }*/
    public static void setUSer(UserModel myuser) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings =getContext().getSharedPreferences("UserPrefs",
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonObject = gson.toJson(myuser);
        editor.putString("User", jsonObject);

        editor.commit();
        //Crashlytics.setUserIdentifier(myuser.getid());
        //Crashlytics.setUserEmail(myuser.getemail());
        //Crashlytics.setUserName(myuser.getfullname());
        //Crashlytics.setString("User", jsonObject.toString());

    }

    public static void logOut() {
        UserModel currentuser= Filo.getUSer();
        currentuser.setkey(null);
        currentuser.setAuthenticated(false);
        currentuser.setrights(null);
        Filo.setUSer(currentuser);
    }

    public static boolean isValidText(String text, int maxlength) {
        if(text.length() >= maxlength ){
            Pattern pattern = Pattern.compile("\\s");
            Matcher matcher = pattern.matcher(text);
            return !matcher.find();
        }
        return false;
    }

    public static String ConvertArrayStringToString(String[] arraystring_to_convert){

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < arraystring_to_convert.length; i++) {
            sb.append(arraystring_to_convert[i]).append(",");
        }
        return sb.toString();
    }

    public static UserModel getUSer() {
        SharedPreferences settings;
        String json;
        UserModel myuser=new UserModel();
        settings =getContext().getSharedPreferences("UserPrefs",
                Context.MODE_PRIVATE);
        json = settings.getString("User", null);
        JSONObject jsonObj = null;
        try {
            if (json != null) {
                jsonObj = new JSONObject(json);
                myuser.setkey(jsonObj.getString("key"));
                myuser.setusername(jsonObj.getString("username"));
                myuser.setuser(jsonObj.getString("user"));
                myuser.setrights( jsonObj.getString("rights") );
                //myuser.setlanguage(jsonObj.getString("language"));


            }
        } catch (JSONException e) {
        }
        return myuser;
    }

    public static boolean CheckUserRight(String user_right) {
        UserModel myusermodel= getUSer();
        boolean canpass=false;
        if(myusermodel!=null && myusermodel.getrights()!=null && !myusermodel.getrights().isEmpty()){
            String user_rights=myusermodel.getrights();
            if(user_rights.indexOf(user_right)>-1){
                canpass=true;
            }
        }
        return canpass;
    }
    public static void NotifyUserOnWrongAccess(){
        Toast.makeText(getContext(), getContext().getResources().getText(R.string.msg_no_user_right), Toast.LENGTH_LONG).show();
    }

    public static void setSetting(SettingModel mysetting) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings =getContext().getSharedPreferences("SettingPrefs",
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonObject = gson.toJson(mysetting);
        editor.putString("Setting", jsonObject);

        editor.commit();
    }

    public static SettingModel getSetting() {
        SharedPreferences settings;
        String json;
        SettingModel mysetting=new SettingModel();
        settings =getContext().getSharedPreferences("SettingPrefs",
                Context.MODE_PRIVATE);
        json = settings.getString("Setting", null);
        JSONObject jsonObj = null;
        try {
            if (json != null) {
                jsonObj = new JSONObject(json);
                mysetting.setaddressip(jsonObj.getString("addressip"));
                mysetting.setocrmintextlength(jsonObj.getInt("ocrmintextlength"));
                mysetting.setocrmaxtextlength(jsonObj.getInt("ocrmaxtextlength"));
            }
        } catch (JSONException e) {
        }
        return mysetting;
    }

    public static void setPrinter(PrinterModel defaultprinter) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings =getContext().getSharedPreferences("PrinterPrefs",
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonObject = gson.toJson(defaultprinter);
        editor.putString("Printer", jsonObject);

        editor.commit();
    }
    public static PrinterModel getPrinter() {
        SharedPreferences settings;
        String json;
        PrinterModel mysetting=new PrinterModel();
        settings =getContext().getSharedPreferences("PrinterPrefs",
                Context.MODE_PRIVATE);
        json = settings.getString("Printer", null);
        JSONObject jsonObj = null;
        try {
            if (json != null) {
                jsonObj = new JSONObject(json);
                mysetting.setname(jsonObj.getString("name"));
                mysetting.setaddressmac(jsonObj.getString("addressmac"));
            }
        } catch (JSONException e) {
        }
        return mysetting;
    }

    public static Context getContext() {
        return mContext;
    }

    public static void setContext(Context context) {
        mContext = context;
    }

    public Filo getAppInstance() {
        if (appInstance == null)
            throw new IllegalStateException(getContext().getString(R.string.illegal_state_exception));
        return appInstance;
    }

    public static SharedPreferences.Editor getApplicationPreferenceEditor() {
        if (sharedPreferencesEditor == null)
            sharedPreferencesEditor = sharedPreferences.edit();

        return sharedPreferencesEditor;
    }

    public static SharedPreferences getApplicationPreference() {
        if (sharedPreferences == null)
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());

        return sharedPreferences;
    }

    private void saveInPreference() {
        SharedPreferences sharedPreferences = this.getSharedPreferences(getString(R.string.scan_code_pref_key), Context.MODE_PRIVATE);
//        if (!sharedPreferences.contains(getString(R.string.save_table_num))) {
//            SharedPreferences.Editor editor = sharedPreferences.edit();
//            editor.putInt(getString(R.string.save_table_num), 5);
//            editor.apply();
//        }
    }

    public Typeface getTypeFace() {
        try {
            return Typeface.createFromAsset(getContext().getAssets(), Value.FONT_PATH);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
            return null;
        }
    }

    public void setTextInputLayoutTypeFace(TextInputLayout textInputLayout) {
        CharSequence charSequence = textInputLayout.getEditText().getText().toString();
        Typeface typeface = getTypeFace();
        Utils.setSpanFont(charSequence, typeface, textInputLayout);
    }

    public Bitmap encodeAsBitmap(String str) throws WriterException {
        BitMatrix result;
        try {
            result = new MultiFormatWriter().encode(str,
                    BarcodeFormat.QR_CODE, WIDTH, WIDTH, null);
        } catch (IllegalArgumentException iae) {
            // Unsupported format
            return null;
        }
        int w = result.getWidth();
        int h = result.getHeight();
        int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++) {
            int offset = y * w;
            for (int x = 0; x < w; x++) {
                pixels[offset + x] = result.get(x, y) ? getResources().getColor(R.color.colorBlackFilterOpac166):getResources().getColor(R.color.colorWhite);
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, 500, 0, 0, w, h);
        return bitmap;
    } /// end of this method
}
