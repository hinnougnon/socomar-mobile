package rightcom.com.filo;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.Objects;

import rightcom.com.filo.app.Filo;
import rightcom.com.filo.fragments.DashboardFragment;
import rightcom.com.filo.fragments.ExitVehicleWizardFirstPageFragment;

import rightcom.com.filo.fragments.ReceiveVehicleWizardFirstPageFragment;
import rightcom.com.filo.fragments.ReceiveVehicleWizardFourthPageFragment;
import rightcom.com.filo.fragments.ReceiveVehicleWizardPagerLayoutFragment;
import rightcom.com.filo.fragments.ReceiveVehicleWizardSecondPageFragment;
import rightcom.com.filo.fragments.ReceiveVehicleWizardThirdPageFragment;
import rightcom.com.filo.fragments.SettingsWizardFirstPageFragment;
import rightcom.com.filo.fragments.SettingsWizardStartFragment;
import rightcom.com.filo.fragments.ChooseOperationFragment;
import rightcom.com.filo.tools.Utils;
import rightcom.com.filo.window.ChangeLocationVehicleActivity;
import rightcom.com.filo.window.ExitVehicleActivity;

import rightcom.com.filo.window.LabelVehicleActivity;
import rightcom.com.filo.window.ReceiveVehicleActivity;

public class PrincipalActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener,
        DashboardFragment.OnDashboardFragListener,
        ChooseOperationFragment.OnChooseoperationFragListener,
        ReceiveVehicleWizardPagerLayoutFragment.OnReceiveVehicleWizardPagerFragListener,
        ReceiveVehicleWizardFirstPageFragment.OnReceiveVehicleWizardFirstPageFragListener,
        ReceiveVehicleWizardSecondPageFragment.OnReceiveVehicleWizardSecondPageFragListener,
        ReceiveVehicleWizardThirdPageFragment.OnReceiveVehicleWizardThirdPageFragListener,
        ReceiveVehicleWizardFourthPageFragment.OnReceiveVehicleWizardFourthPageFragListener,

        ExitVehicleWizardFirstPageFragment.OnExitVehicleWizardFirstPageFragment,
        SettingsWizardFirstPageFragment.OnSettingsWizardFirstPageFragListener  {

    private Toolbar toolbar;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private ActionBarDrawerToggle toggle;
    private FrameLayout frameLayout;

    private MaterialRippleLayout mrlAccount;

//    private TextView tvHeaderTitle;
//    private TextView tvHeaderSubTitle;

    private Filo filo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        filo = (Filo) getApplication();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        frameLayout = findViewById(R.id.frame_container);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setCheckedItem(R.id.nv_dashboard);
        navigationView.setNavigationItemSelectedListener(this);
        onDashboardSelected();

        View headerLayout = navigationView.getHeaderView(0);
        mrlAccount = headerLayout.findViewById(R.id.mrlAccount);
        initListener();
    }

    @Override
    public void onBackPressed() {
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nv_dashboard) {
            onDashboardSelected();
        } else if (id == R.id.nav_do_an_operation) {
            go_to_DOAnOperation();
        }  else if (id == R.id.nav_settings) {
            onSettingsSelected();
        }

        //DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

   /* @Override
    public void onTvSearchFragTextClick(String s) {
        Toast.makeText(PrincipalActivity.this, "You click on " + s + " text.", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onTvDownloadFragTextClick(String s) {
        Toast.makeText(PrincipalActivity.this, "You click on " + s + " text.", Toast.LENGTH_LONG).show();
    }*/

    @Override
    public void onTvReceiveFragTextClick(String s) {
        Toast.makeText(PrincipalActivity.this, "You click on " + s + " text.", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onTotalIssueClickListener() {
        Toast.makeText(PrincipalActivity.this, "You click on TotalIssue", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onPendingIssueClickListener() {
        Toast.makeText(PrincipalActivity.this, "You click on PendingIssue", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSolvedIssueClickListener() {
        Toast.makeText(PrincipalActivity.this, "You click on SolvedIssue", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onReportIssueClickListener() {
        go_to_DOAnOperation();
    }

    @Override
    public void onNewIssueClickListener() {
        Toast.makeText(PrincipalActivity.this, "You click on NewIssue", Toast.LENGTH_LONG).show();
    }



    @Override
    public void onReceiveActionSelected() {
        onReceiveSelected();
    }

    @Override
    public void onLabelActionSelected() {
        onLabelSelected();
    }

    @Override
    public void onChangelocationActionSelected() {
        onChangelocationSelected();
    }

    @Override
    public void onExitActionSelected() {
        onExitSelected();
    }

    @Override
    public void onBackClick() {
        go_to_DOAnOperation();
    }

    @Override
    public void go_to_receivevehicle_firstpage_fragment() {
        Utils.loadFragment(getSupportFragmentManager(), R.id.frame_container, new ReceiveVehicleWizardFirstPageFragment(), false, ReceiveVehicleWizardFirstPageFragment.TAG);
    }

    private void initListener() {
        mrlAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Filo.logOut();
                Intent intent = new Intent(PrincipalActivity.this, LoginPageActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    /**
     * Function call when user click on Dashboard menu
     */
    private void onDashboardSelected() {
        toolbar.setTitle("");
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.colorGrey700));
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        Utils.loadFragment(getSupportFragmentManager(), R.id.frame_container, new DashboardFragment(), false, DashboardFragment.TAG);
    }

    /**
     * Function call when user click on Submit an issue menu
     */
    private void go_to_DOAnOperation() {
        toolbar.setTitle(getString(R.string.text_signaler_un_problème));
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.colorGrey700));
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        Utils.loadFragment(getSupportFragmentManager(), R.id.frame_container, new ChooseOperationFragment(), false, ChooseOperationFragment.TAG);
        navigationView.setCheckedItem(R.id.nav_do_an_operation);
    }

    private void onSettingsSelected() {
        toolbar.setTitle("Paramètres");
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorWhite));
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.colorGrey700));
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        Utils.loadFragment(getSupportFragmentManager(), R.id.frame_container, new SettingsWizardStartFragment(), false, SettingsWizardStartFragment.TAG);
    }

    private void onLabelSelected() {
        toolbar.setTitle("");
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        startActivity(new Intent(PrincipalActivity.this,  LabelVehicleActivity.class));
    }
    private void onChangelocationSelected(){
        toolbar.setTitle("");
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        startActivity(new Intent(PrincipalActivity.this, ChangeLocationVehicleActivity.class));
    }
    private void onReceiveSelected(){
        toolbar.setTitle("");
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        startActivity(new Intent(PrincipalActivity.this, ReceiveVehicleActivity.class));
    }




    private void onExitSelected() {
        toolbar.setTitle("");
        startActivity(new Intent(PrincipalActivity.this, ExitVehicleActivity.class));
    }

}
