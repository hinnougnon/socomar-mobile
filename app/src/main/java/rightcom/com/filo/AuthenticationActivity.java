package rightcom.com.filo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import rightcom.com.filo.fragments.ToLoginFragment;
import rightcom.com.filo.tools.Utils;

public class AuthenticationActivity extends AppCompatActivity implements ToLoginFragment.OnToLoginFragListener {

    public static final String TAG = AuthenticationActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentication);

        init();
        Utils.loadFragment(getSupportFragmentManager(), R.id.frameLayout, new ToLoginFragment(), false, ToLoginFragment.TAG);
    }

    private void init() {
        // We init the progress dialog
    }

    @Override
    public void onLoginProcessStart(String userLoginMoel) {
        Toast.makeText(AuthenticationActivity.this, userLoginMoel, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRegisterProcessStart() {
        Log.e(TAG, "RegisterProcessStart");
    }
}
