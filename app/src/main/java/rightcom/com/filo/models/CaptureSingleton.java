package rightcom.com.filo.models;

public class CaptureSingleton {

    public static final CaptureSingleton instance = new CaptureSingleton();

    private byte[] picture = null;


    public static CaptureSingleton getInstance() {
        return instance;
    }

    private CaptureSingleton() {
    }

    public void clearData() {
        this.picture = null;
    }



    public byte[] getPicture() {
        return this.picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }


}
