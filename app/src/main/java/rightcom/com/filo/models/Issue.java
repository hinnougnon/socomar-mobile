package rightcom.com.filo.models;

public class Issue {
    private String issue_date = null;
    private String issue_hour = null;
    private String issue_service_fr = null;
    private String issue_service_en = null;
    private String issue_name_fr = "";
    private String issue_name_en = "";
    private String issue_comment = null;
    private String issue_image_link =null;
    private String issue_survey_date = null;
    private int issue_status = 0;
    private String issue_response =  null;
    private float issue_rating = 0;

    public Issue() {

    }

    public Issue(String issue_date, String issue_hour, String issue_service_fr, String issue_service_en,
                 String issue_name_fr, String issue_name_en, String issue_comment, String issue_image_link,
                 String issue_survey_date, int issue_status, String issue_response, float issue_rating) {
        this.issue_date = issue_date;
        this.issue_hour = issue_hour;
        this.issue_service_fr = issue_service_fr;
        this.issue_service_en = issue_service_en;
        this.issue_name_fr = issue_name_fr;
        this.issue_name_en = issue_name_en;
        this.issue_comment = issue_comment;
        this.issue_image_link = issue_image_link;
        this.issue_survey_date = issue_survey_date;
        this.issue_status = issue_status;
        this.issue_response = issue_response;
        this.issue_rating = issue_rating;
    }

    public String getIssue_date() {
        return issue_date;
    }

    public void setIssue_date(String issue_date) {
        this.issue_date = issue_date;
    }

    public String getIssue_hour() {
        return issue_hour;
    }

    public void setIssue_hour(String issue_hour) {
        this.issue_hour = issue_hour;
    }

    public String getIssue_service_fr() {
        return issue_service_fr;
    }

    public void setIssue_service_fr(String issue_service_fr) {
        this.issue_service_fr = issue_service_fr;
    }

    public String getIssue_service_en() {
        return issue_service_en;
    }

    public void setIssue_service_en(String issue_service_en) {
        this.issue_service_en = issue_service_en;
    }

    public String getIssue_name_fr() {
        return issue_name_fr;
    }

    public void setIssue_name_fr(String issue_name_fr) {
        this.issue_name_fr = issue_name_fr;
    }

    public String getIssue_name_en() {
        return issue_name_en;
    }

    public void setIssue_name_en(String issue_name_en) {
        this.issue_name_en = issue_name_en;
    }

    public String getIssue_comment() {
        return issue_comment;
    }

    public void setIssue_comment(String issue_comment) {
        this.issue_comment = issue_comment;
    }

    public String getIssue_image_link() {
        return issue_image_link;
    }

    public void setIssue_image_link(String issue_image_link) {
        this.issue_image_link = issue_image_link;
    }

    public String getIssue_survey_date() {
        return issue_survey_date;
    }

    public void setIssue_survey_date(String issue_survey_date) {
        this.issue_survey_date = issue_survey_date;
    }

    public int getIssue_status() {
        return issue_status;
    }

    public void setIssue_status(int issue_status) {
        this.issue_status = issue_status;
    }

    public String getIssue_response() {
        return issue_response;
    }

    public void setIssue_response(String issue_response) {
        this.issue_response = issue_response;
    }

    public float getIssue_rating() {
        return issue_rating;
    }

    public void setIssue_rating(float issue_rating) {
        this.issue_rating = issue_rating;
    }

    @Override
    public String toString() {
        return "IssueModel(issue_date=$issue_date, issue_hour=$issue_hour, " +
                "issue_service_fr=$issue_service_fr, issue_service_en=$issue_service_en, " +
                "issue_name_fr='$issue_name_fr', issue_name_en='$issue_name_en', " +
                "issue_comment=$issue_comment, issue_image_link=$issue_image_link, " +
                "issue_survey_date=$issue_survey_date, issue_status=$issue_status, " +
                "issue_response=$issue_response, issue_rating=$issue_rating)";
    }
}
