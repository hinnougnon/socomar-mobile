package rightcom.com.filo.models;

import rightcom.com.filo.app.Filo;

public class SettingModel {

	public void setaddressip(String uiaddressip) {
		this.addressip = uiaddressip;
	}
	public String getdddressip() {
		return addressip;
	}

	public void setocrmintextlength(int uiocrmintextlength) {
		this.ocrmintextlength = uiocrmintextlength;
	}
	public int getocrmintextlength() {
		return ocrmintextlength;
	}
	public void setocrmaxtextlength(int uiocrmaxtextlength) {
		this.ocrmaxtextlength = uiocrmaxtextlength;
	}
	public int getocrmaxtextlength() {
		return ocrmaxtextlength;
	}
	public static synchronized SettingModel getInstance() {
		if (instance == null) {
			instance = new SettingModel();

		}
		return instance;
	}
	private String addressip="";
	private int ocrmintextlength=Filo.DEFAULT_VIN_MIN_LENGTH;
	private int ocrmaxtextlength=Filo.DEFAULT_VIN_MAX_LENGTH;
	private static SettingModel instance = null;
}
