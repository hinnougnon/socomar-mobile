package rightcom.com.filo.models;

public class ExitModel {

	public String getQrcode() {
		return qrcode;
	}

	public void setQrcode(String qrcode) {
		this.qrcode = qrcode;
	}

	public String getOcrcode() {
		return ocrcode;
	}

	public void setOcrcode(String ocrcode) {
		this.ocrcode = ocrcode;
	}

    public Boolean getIsvalidqrcode() {
        return isvalidqrcode;
    }

    public void setIsvalidqrcode(Boolean isvalidqrcode) {
        this.isvalidqrcode = isvalidqrcode;
    }

    public boolean compareQRCodeAndOCRCode() {
	    if (this.qrcode != null && this.ocrcode != null) {
	        return this.qrcode.toUpperCase().equals(this.ocrcode.toUpperCase());
        }
        return false;
    }

    public static void setInstance(ExitModel instance) {
		ExitModel.instance = instance;
	}

	public static synchronized ExitModel getInstance() {
		if (instance == null) {
			instance = new ExitModel();

		}
		return instance;
	}

	private String qrcode;
	private String ocrcode;
	private Boolean isvalidqrcode = false;
	private static ExitModel instance = null;
}
