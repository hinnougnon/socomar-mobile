package rightcom.com.filo.models;

public class IssueSingleton {

    public static final IssueSingleton instance = new IssueSingleton();

    private String issue_date = null;
    private String issue_hour = null;
    private String issue_service_fr = null;
    private String issue_service_en = null;
    private String issue_name_fr = "";
    private String issue_name_en = "";
    private String issue_comment = null;
    private String issue_image_link =null;
    private String issue_survey_date = null;
    private int issue_status;
    private float issue_rating = (float)0;

    public static IssueSingleton getInstance() {
        return instance;
    }

    private IssueSingleton() {
    }

    public void clearData() {
        this.issue_date = null;
        this.issue_hour = null;
        this.issue_service_fr = null;
        this.issue_service_en = null;
        this.issue_name_fr = "";
        this.issue_name_en = "";
        this.issue_comment = null;
        this.issue_image_link =null;
        this.issue_survey_date = null;
        this.issue_status = 0;
        this.issue_rating = (float) 0;
    }

    public void copyData(Issue issue) {
        this.issue_date = issue.getIssue_date();
        this.issue_hour = issue.getIssue_hour();
        this.issue_service_fr = issue.getIssue_service_fr();
        this.issue_service_en = issue.getIssue_service_en();
        this.issue_name_fr = issue.getIssue_name_fr();
        this.issue_name_en = issue.getIssue_name_en();
        this.issue_comment = issue.getIssue_comment();
        this.issue_image_link = issue.getIssue_image_link();
        this.issue_survey_date = issue.getIssue_survey_date();
        this.issue_status = issue.getIssue_status();
        this.issue_rating = issue.getIssue_rating();
    }

    public String getIssue_date() {
        return issue_date;
    }

    public void setIssue_date(String issue_date) {
        this.issue_date = issue_date;
    }

    public String getIssue_hour() {
        return issue_hour;
    }

    public void setIssue_hour(String issue_hour) {
        this.issue_hour = issue_hour;
    }

    public String getIssue_service_fr() {
        return issue_service_fr;
    }

    public void setIssue_service_fr(String issue_service_fr) {
        this.issue_service_fr = issue_service_fr;
    }

    public String getIssue_service_en() {
        return issue_service_en;
    }

    public void setIssue_service_en(String issue_service_en) {
        this.issue_service_en = issue_service_en;
    }

    public String getIssue_name_fr() {
        return issue_name_fr;
    }

    public void setIssue_name_fr(String issue_name_fr) {
        this.issue_name_fr = issue_name_fr;
    }

    public String getIssue_name_en() {
        return issue_name_en;
    }

    public void setIssue_name_en(String issue_name_en) {
        this.issue_name_en = issue_name_en;
    }

    public String getIssue_comment() {
        return issue_comment;
    }

    public void setIssue_comment(String issue_comment) {
        this.issue_comment = issue_comment;
    }

    public String getIssue_image_link() {
        return issue_image_link;
    }

    public void setIssue_image_link(String issue_image_link) {
        this.issue_image_link = issue_image_link;
    }

    public String getIssue_survey_date() {
        return issue_survey_date;
    }

    public void setIssue_survey_date(String issue_survey_date) {
        this.issue_survey_date = issue_survey_date;
    }

    public int getIssue_status() {
        return issue_status;
    }

    public void setIssue_status(int issue_status) {
        this.issue_status = issue_status;
    }

    public float getIssue_rating() {
        return issue_rating;
    }

    public void setIssue_rating(float issue_rating) {
        this.issue_rating = issue_rating;
    }
}
