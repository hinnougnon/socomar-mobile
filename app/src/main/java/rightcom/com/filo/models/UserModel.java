package rightcom.com.filo.models;

public class UserModel {

	public void setusername(String uiusername) {
		this.username = uiusername;
	}
	public String getusername() {
		return username;
	}

	public void setuser(String uiuser) {
		this.user = uiuser;
	}
	public String getuser() {
		return user;
	}


	public String getrights() {
		return rights;
	}
	public void setrights(String uirights) {
		this.rights = uirights;
	}

	public String getkey() {
		return key;
	}
	public void setkey(String uikey) {
		this.key = uikey;
	}

		public void setlanguage(String uilanguage) {
		this.language = uilanguage;
	}
	public String getlanguage() {
		return (language!=null)?language:"fr";
	}

	public boolean isAuthenticated() {
		return isAuthenticated;
	}

	public void setAuthenticated(boolean authenticated) {
		isAuthenticated = authenticated;
	}
	public static synchronized UserModel getInstance() {
		if (instance == null) {
			instance = new UserModel();

		}
		return instance;
	}
	private String key;
	private String username;
	private String user ;
	private String rights;
	private String language;
	private static UserModel instance = null;
	private boolean isAuthenticated = false;
}
