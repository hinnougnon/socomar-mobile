package rightcom.com.filo.models;

public class PrinterModel {

	public void setaddressmac(String uiaddressmac) {
		this.addressmac = uiaddressmac;
	}
	public String getdddressmac() {
		return addressmac;
	}
	public void setname(String uiname) {
		this.name = uiname;
	}
	public String getname() {
		return name;
	}

	public String getFriendlyName() {

		//return this.name+" # "+this.getdddressmac();
		return (this.addressmac!=null && !this.addressmac.isEmpty())?(this.name+" # "+this.getdddressmac()):this.name;
	}

	public static synchronized PrinterModel getInstance() {
		if (instance == null) {
			instance = new PrinterModel();

		}
		return instance;
	}
	private String addressmac="";
	private String name="";
	private static PrinterModel instance = null;

	@Override
	public String toString() {
		return (this.addressmac!=null && !this.addressmac.isEmpty())?(this.addressmac):this.name;
	}
}
