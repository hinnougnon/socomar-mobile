package rightcom.com.filo.window;

import android.annotation.SuppressLint;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;

import rightcom.com.filo.R;
import rightcom.com.filo.adapters.ExitVehiclePageAdapter;
import rightcom.com.filo.app.Filo;
import rightcom.com.filo.fragments.ExitVehicleWizardFirstPageFragment;
import rightcom.com.filo.fragments.ExitVehicleWizardSecondPageFragment;
import rightcom.com.filo.fragments.ExitVehicleWizardScanCodeResultFragment;
import rightcom.com.filo.fragments.ExitVehicleWizardThirdPageFragment;
import rightcom.com.filo.fragments.OCRScannerFragment;
import rightcom.com.filo.models.UserModel;

public class ExitVehicleActivity extends AppCompatActivity implements
        ExitVehicleWizardFirstPageFragment.OnExitVehicleWizardFirstPageFragment,
        ExitVehicleWizardSecondPageFragment.OnExitVehicleWizardSecondPageFragListener,
        ExitVehicleWizardThirdPageFragment.OnExitVehicleWizardThirdPageFragListener{

    public static final String TAG = ExitVehicleActivity.class.getSimpleName();
    UserModel currentUser;

    private ExitVehiclePageAdapter adapter;

    private Toolbar toolbar;
    private static ViewPager pager;
    private TextView previousButton;
    private TextView nextButton;
    private TextView navigator;
    private int currentItem;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exit_vehicle);

        currentUser=Filo.getUSer();
//        instance_created=this;
        currentItem = 0;

        toolbar = findViewById(R.id.tbExitVehicleToolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Exit");
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        pager = (ViewPager) findViewById(R.id.activity_wizard_universal_pager);
        previousButton = (TextView) findViewById(R.id.activity_wizard_universal_previous);
        nextButton = (TextView) findViewById(R.id.activity_wizard_universal_next);
        navigator = (TextView) findViewById(R.id.activity_wizard_universal_possition);

        adapter = new ExitVehiclePageAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        pager.setCurrentItem(currentItem);

        setNavigator();

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onPageScrollStateChanged(int position) {
                // TODO Auto-generated method stub
                setNavigator();
            }
        });

        previousButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
				/*if (pager.getCurrentItem() != 0) {
					pager.setCurrentItem(pager.getCurrentItem() - 1);
				}
				setNavigator();*/
                Toast.makeText(ExitVehicleActivity.this, "Skip", Toast.LENGTH_SHORT).show();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (pager.getCurrentItem() != (pager.getAdapter().getCount() - 1)) {
                    pager.setCurrentItem(pager.getCurrentItem() + 1);
                } else {
                    Toast.makeText(ExitVehicleActivity.this, "Finish", Toast.LENGTH_SHORT).show();
                }
                setNavigator();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void setNavigator() {
        String navigation = "";
        for (int i = 0; i < adapter.getCount(); i++) {
            if (i == pager.getCurrentItem()) {
                navigation += getString(R.string.material_icon_point_full)
                        + "  ";
            } else {
                navigation += getString(R.string.material_icon_point_empty)
                        + "  ";
            }
        }
        navigator.setText(navigation);
    }

    public void setCurrentSlidePosition(int position) {
        this.currentItem = position;
    }

    public int getCurrentSlidePosition() {
        return this.currentItem;
    }

    public static void swipeto(int position) {
        pager.setCurrentItem(position);
//        Objects.requireNonNull(pager.getAdapter()).notifyDataSetChanged();
    }

    public static void refreshOCRFrag() {
        Objects.requireNonNull(pager.getAdapter()).notifyDataSetChanged();
    }

    @Override
    public void onBackClick() {
        onBackPressed();
    }

    @Override
    public void go_to_exitvehicle_firstpage_fragment() {
        swipeto(0);
    }

    @Override
    public void onCloseClick() {
        onBackPressed();
    }
}
