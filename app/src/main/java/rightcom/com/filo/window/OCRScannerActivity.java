package rightcom.com.filo.window;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import rightcom.com.filo.R;
import rightcom.com.filo.adapters.OCRActivityScanTextsAdapter;
import rightcom.com.filo.app.Filo;
import rightcom.com.filo.fragments.ExitVehicleWizardSecondPageFragment;

import rightcom.com.filo.models.SettingModel;

public class OCRScannerActivity extends AppCompatActivity implements OCRActivityScanTextsAdapter.OnItemClickListener  {

    public static final String TAG = OCRScannerActivity.class.getSimpleName();
    private static final int requestPermissionID = 101;

    private SettingModel currentsetting;
    private boolean autoFocus = true;
    private boolean useFlash = true;

    private String fragParentTag = null;
    //private String labelText = null;
    private String selectedAction = null;
    private List<String> strings = new ArrayList<>();
    private OCRActivityScanTextsAdapter adapter;

    private Toolbar toolbar;
    private SurfaceView svCamera;
    private RelativeLayout rlBottom;
    private CameraSource mCameraSource;
    private TextView tvScannedText;
    private ImageView ivPlay;
    private RecyclerView rvScannedText;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ocr_scanner_activity);

        toolbar = findViewById(R.id.tbOCRActivity);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Scanner châssis");
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        svCamera = findViewById(R.id.svCamera);
        rlBottom = findViewById(R.id.rlBottom);
        tvScannedText = findViewById(R.id.tvScannedText);
        ivPlay = findViewById(R.id.ivPlay);
        rvScannedText = findViewById(R.id.rvScannedText);
        currentsetting=Filo.getSetting();

        initListener();
        initRecyclerView();
        startCameraSource();
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode != requestPermissionID) {
            Log.d(TAG, "Got unexpected permission result: " + requestCode);
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            try {
                if (ActivityCompat.checkSelfPermission(OCRScannerActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                mCameraSource.start(svCamera.getHolder());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void initListener() {
//        tvScannedText.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(OCRScannerActivity.this, ExitVehicleActivity.class);
//                intent.putExtra("OCRCODE", tvScannedText.getText().toString());
//                setResult(ExitVehicleWizardSecondPageFragment.OCR_ACTIVITY_RESULT_CODE, intent);
//                checkSelectedActionAndStartProcess(tvScannedText.getText().toString());
//                LabelVehicleWizardShowResultsFragment.CurrentQRcodeGenerated = tvScannedText.getText().toString();
//                LabelVehicleWizardStartFragment.swipeto(2, null, null, "D42QIL");
//                String s = tvScannedText.getText().toString();
//                if (OCRScannerActivity.PARENT_FRAG_TAG != null && OCRScannerActivity.SELECTED_ACTION != null) {
//                    switch (SELECTED_ACTION) {
//                        case Value.LABEL_ACTION_KEY:
//                            LabelVehicleWizardShowResultsFragment.CurrentQRcodeGenerated = s;
//                            ScanCodeCheckingFragment.SELECTED_ACTION = Value.LABEL_ACTION_KEY;
//                            ScanCodeCheckingFragment.FRAG_PARENT_TAG = fragParentTag;
//                            LabelVehicleWizardStartFragment.swipeto(2, null, null, "D42QIL");
//                            break;
//
//                        case Value.RECEIVE_ACTION_KEY:
//                            /*ReceiveVehicleWizardShowResultsFragment.CurrentQRcodeGenerated = s;
//                            ScanCodeCheckingFragment.SELECTED_ACTION = Value.RECEIVE_ACTION_KEY;
//                            ScanCodeCheckingFragment.FRAG_PARENT_TAG = fragParentTag;
//                            ReceiveVehicleWizardStartFragment.swipeto(2, null, null, "D42QIL");*/
//                            ReceiveVehicleWizardSecondPageFragment.VIN_SCANNED = s;
////                            ReceiveVehicleWizardPagerLayoutFragment.swipeto(2);
//                            ReceiveVehicleActivity.swipeto(2);
//                            break;
//
//                        case Value.EXIT_ACTION_KEY:
////                            ExitVehicleWizardThirdPageFragment.QR_CODE_GENERATED = qrCodeGenerated;
////                            ExitVehicleWizardThirdPageFragment.OCR_CODE_GENERATED = s;
//                            ExitVehicleWizardSecondPageFragment.currentOCRCode = s;
////                            ExitVehicleWizardStartFragment.swipeto(3, null, null, s);
//                            ExitVehicleActivity.swipeto(1, null, null, s);
////                            ExitVehicleActivity.swipeto(3, null, null, s);
//                            break;
//                    }
//                }
//            }
//        });
        ivPlay.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onClick(View view) {
                hidePlayButton();
                strings.clear();
                OCRScannerActivity.this.recreate();
//                startCameraSource();
//                LabelVehicleWizardStartFragment.refreshOCRFrag();
//                if (OCRScannerActivity.PARENT_FRAG_TAG != null && OCRScannerActivity.SELECTED_ACTION != null) {
//                    switch (SELECTED_ACTION) {
//                        case Value.LABEL_ACTION_KEY:
//                            LabelVehicleWizardStartFragment.refreshOCRFrag();
//                            break;
//                        case Value.RECEIVE_ACTION_KEY:
//                            ReceiveVehicleActivity.refreshOCRFrag();
//                            break;
//
//                        case Value.EXIT_ACTION_KEY:
//                            ExitVehicleActivity.refreshOCRFrag();
//                            break;
//                    }
//                }
            }
        });
    }

    private void initRecyclerView() {
        rvScannedText.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(OCRScannerActivity.this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvScannedText.setLayoutManager(layoutManager);
        adapter = new OCRActivityScanTextsAdapter(OCRScannerActivity.this, strings);
        rvScannedText.setAdapter(adapter);
    }

    private void startCameraSource() {

        //Create the TextRecognizer
        final TextRecognizer textRecognizer = new TextRecognizer.Builder(OCRScannerActivity.this).build();

        if (!textRecognizer.isOperational()) {
            Log.w(TAG, "Detector dependencies not loaded yet");
        } else {

            //Initialize camerasource to use high resolution and set Autofocus on.
            mCameraSource = new CameraSource.Builder(Objects.requireNonNull(OCRScannerActivity.this), textRecognizer)
                    .setFacing(CameraSource.CAMERA_FACING_BACK)
                    .setRequestedPreviewSize(1280, 1024)
                    .setAutoFocusEnabled(true)
                    .setRequestedFps(2.0f)
                    //.setFlashMode(useFlash ? Camera.Parameters.FLASH_MODE_TORCH : null)
                    //.setFocusMode(autoFocus ? Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO : null)
                    .build();

            /**
             * Add call back to SurfaceView and check if camera permission is granted.
             * If permission is granted we can start our cameraSource and pass it to surfaceView
             */
            svCamera.getHolder().addCallback(new SurfaceHolder.Callback() {
                @Override
                public void surfaceCreated(SurfaceHolder holder) {
                    try {

                        if (ActivityCompat.checkSelfPermission(OCRScannerActivity.this,
                                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(OCRScannerActivity.this, new String[]{Manifest.permission.CAMERA}, requestPermissionID);
                            return;
                        }
                        mCameraSource.start(svCamera.getHolder());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                }

                @Override
                public void surfaceDestroyed(SurfaceHolder holder) {
                    mCameraSource.stop();
                }
            });

            //Set the TextRecognizer's Processor.
            textRecognizer.setProcessor(new Detector.Processor<TextBlock>() {
                @Override
                public void release() {
                }

                /**
                 * Detect all the text from camera using TextBlock and the values into a stringBuilder
                 * which will then be set to the textView.
                 * */
                @Override
                public void receiveDetections(Detector.Detections<TextBlock> detections) {
                    final SparseArray<TextBlock> items = detections.getDetectedItems();
                    if (items.size() != 0 ){

                        tvScannedText.post(new Runnable() {
                            @Override
                            public void run() {
                                showRVScanText();
                                for(int i=0;i<items.size();i++){
                                    TextBlock item = items.valueAt(i);
                                    if (Filo.isValidText(item.getValue(),currentsetting.getocrmintextlength())) {
                                        String current_string_found = item.getValue().toUpperCase().replace("O","0");
                                        String string_with_1 = current_string_found.replace("I","1");
                                        String string_with_L = current_string_found.replace("I","L");
                                        strings.add(String.valueOf(string_with_1));
                                        strings.add(String.valueOf(string_with_L));
                                    }
                                }

                                if (strings.size()>0) {
                                    showPlayButton();
                                    synchronized (mCameraSource) {
                                        try {
                                            mCameraSource.release();
                                        } catch (Exception e) {
                                            Log.e(TAG, e.getMessage());
                                        }
                                    }
                                    adapter.notifyDataSetChanged();
                                }




                            }
                        });
                    }
                }


            });
        }
    }

   /* private Boolean isValidText(String text) {
        if (text.length() > DEFAULT_VIN_LENGTH) {
            Pattern pattern = Pattern.compile("\\s");
            Matcher matcher = pattern.matcher(text);
            if (!matcher.find()) {
                return true;
            }
        }
        return false;
    }*/

    private void hidePlayButton () {
        if (ivPlay.getVisibility() == View.VISIBLE) {
            ivPlay.setVisibility(View.GONE);
            rlBottom.setVisibility(View.GONE);
        }
    }

    private void showPlayButton() {
        if (ivPlay.getVisibility() == View.GONE) {
            ivPlay.setVisibility(View.VISIBLE);
            rlBottom.setVisibility(View.VISIBLE);
        }
    }

    private void hideRVScanText() {
        if (rvScannedText.getVisibility() == View.VISIBLE) {
            rvScannedText.setVisibility(View.GONE);
//            if (tvScannedText.getVisibility() == View.GONE) {
//                tvScannedText.setVisibility(View.VISIBLE);
//            }
        }
    }

    private void showRVScanText() {
        if (rvScannedText.getVisibility() == View.GONE) {
            rvScannedText.setVisibility(View.VISIBLE);
//            if (tvScannedText.getVisibility() == View.VISIBLE) {
//                tvScannedText.setVisibility(View.GONE);
//            }
        }
    }

    @Override
    public void onItemClick(String s) {
        Intent intent = new Intent(OCRScannerActivity.this, ExitVehicleActivity.class);
        intent.putExtra("OCRCODE", s);
        setResult(ExitVehicleWizardSecondPageFragment.OCR_ACTIVITY_RESULT_CODE, intent);
        OCRScannerActivity.this.finish();
//        checkSelectedActionAndStartProcess(s);
//        if (OCRScannerActivity.PARENT_FRAG_TAG != null && OCRScannerActivity.SELECTED_ACTION != null) {
//            switch (SELECTED_ACTION) {
//                case Value.LABEL_ACTION_KEY:
//                    LabelVehicleWizardShowResultsFragment.CurrentQRcodeGenerated = s;
//                    ScanCodeCheckingFragment.SELECTED_ACTION = Value.LABEL_ACTION_KEY;
//                    ScanCodeCheckingFragment.FRAG_PARENT_TAG = fragParentTag;
//                    LabelVehicleWizardStartFragment.swipeto(2, null, null, "D42QIL");
//                    break;
//                case Value.RECEIVE_ACTION_KEY:
//                    /*ReceiveVehicleWizardShowResultsFragment.CurrentQRcodeGenerated = s;
//                    ScanCodeCheckingFragment.SELECTED_ACTION = Value.RECEIVE_ACTION_KEY;
//                    ScanCodeCheckingFragment.FRAG_PARENT_TAG = fragParentTag;
//                    ReceiveVehicleWizardStartFragment.swipeto(2, null, null, "D42QIL");*/
//                    ReceiveVehicleWizardSecondPageFragment.VIN_SCANNED = s;
////                    ReceiveVehicleWizardPagerLayoutFragment.swipeto(2);
//                    ReceiveVehicleActivity.swipeto(2);
//                    break;
//
//                case Value.EXIT_ACTION_KEY:
////                    ExitVehicleWizardThirdPageFragment.QR_CODE_GENERATED = qrCodeGenerated;
////                    ExitVehicleWizardThirdPageFragment.OCR_CODE_GENERATED = s;
//                    ExitVehicleWizardSecondPageFragment.currentOCRCode = s;
////                    ExitVehicleWizardStartFragment.swipeto(3, null, null, s);
////                    ExitVehicleActivity.swipeto(3, null, null, s);
//                    ExitVehicleActivity.swipeto(1, null, null, s);
//                    break;
//            }
//        }
    }
}
