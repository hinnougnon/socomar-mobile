package rightcom.com.filo.window;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;

import rightcom.com.filo.R;
import rightcom.com.filo.adapters.ChangelocationVehiclePageAdapter;
import rightcom.com.filo.app.Filo;
import rightcom.com.filo.fragments.ChangelocationVehicleWizardFirstPageFragment;
import rightcom.com.filo.fragments.ChangelocationVehicleWizardSecondPageFragment;
import rightcom.com.filo.fragments.ChangelocationVehicleWizardThirdPageFragment;
import rightcom.com.filo.models.UserModel;

public class ChangeLocationVehicleActivity extends AppCompatActivity implements
        ChangelocationVehicleWizardFirstPageFragment.OnChangelocationVehicleWizardFirstPageFragListener,
        ChangelocationVehicleWizardSecondPageFragment.OnChangelocationVehicleWizardSecondPageFragment,
        ChangelocationVehicleWizardThirdPageFragment.OnChangelocationVehicleWizardthirdPageFragment{

    public static final String TAG = ChangeLocationVehicleActivity.class.getSimpleName();
    UserModel currentUser;
    private ChangelocationVehiclePageAdapter adapter;

    private Toolbar toolbar;
    private static ViewPager pager;
    private TextView previousButton;
    private TextView nextButton;
    private TextView navigator;
    private int currentItem;

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changelocation_vehicle);

        currentUser=Filo.getUSer();
//        instance_created = this;
        currentItem = 0;

        toolbar = findViewById(R.id.tbChangelocationVehicleToolbar);
        pager = (ViewPager) findViewById(R.id.activity_wizard_changelocation_universal_pager);
        previousButton = (TextView) findViewById(R.id.activity_wizard_changelocation_universal_previous);
        nextButton = (TextView) findViewById(R.id.activity_wizard_changelocation_universal_next);
        navigator = (TextView) findViewById(R.id.activity_wizard_changelocation_universal_position);

        setSupportActionBar(toolbar);
        toolbar.setTitle("Modifier Position");
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp));
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        adapter = new ChangelocationVehiclePageAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        pager.setCurrentItem(currentItem);

        setNavigator();

        pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onPageScrollStateChanged(int position) {
                // TODO Auto-generated method stub
                setNavigator();
            }
        });

        previousButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Toast.makeText(ChangeLocationVehicleActivity.this, "Skip",
                        Toast.LENGTH_SHORT).show();
            }
        });

        nextButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (pager.getCurrentItem() != (pager.getAdapter().getCount() - 1)) {
                    pager.setCurrentItem(pager.getCurrentItem() + 1);
                } else {
                    Toast.makeText(ChangeLocationVehicleActivity.this, "Finish",
                            Toast.LENGTH_SHORT).show();
                }
                setNavigator();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public void setNavigator() {
        String navigation = "";
        for (int i = 0; i < adapter.getCount(); i++) {
            if (i == pager.getCurrentItem()) {
                navigation += getString(R.string.material_icon_point_full)
                        + "  ";
            } else {
                navigation += getString(R.string.material_icon_point_empty)
                        + "  ";
            }
        }
        navigator.setText(navigation);
    }

    public void setCurrentSlidePosition(int position) {
        this.currentItem = position;
    }

    public int getCurrentSlidePosition() {
        return this.currentItem;
    }

    public static void swipeto(int position) {
        pager.setCurrentItem(position);
    }

    public static void refreshFrag() {
        Objects.requireNonNull(pager.getAdapter()).notifyDataSetChanged();
    }

    final View.OnClickListener buttonProcessTypeListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {


        }
    };

    final View.OnClickListener buttonProcessScanListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
        }
    };

    @Override
    public void onBackClick() {
        onBackPressed();
    }

    /*@Override
    public void go_to_change_location_firstpage_fragment() {
        swipeto(0);
    }*/
}
