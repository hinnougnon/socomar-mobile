package rightcom.com.filo.dialogs;

import android.app.Dialog;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.balysv.materialripple.MaterialRippleLayout;

import rightcom.com.filo.fragments.ReceiveVehicleWizardFirstPageFragment;
import rightcom.com.filo.R;

public class DialogChooseOCRorType {

	private ReceiveVehicleWizardFirstPageFragment mReceiveVehicleWizardFirstPageFragment;
	private Dialog mDialog;
	private MaterialRippleLayout mGoToOCR = null;
	private MaterialRippleLayout mGoToType = null;
	private CardView cardGoToOCR = null;
	private CardView cardGoToType = null;
	private RelativeLayout rlCancel = null;
	public DialogChooseOCRorType(ReceiveVehicleWizardFirstPageFragment mReceiveVehicleWizardFirstPageFragment) {
		this.mReceiveVehicleWizardFirstPageFragment = mReceiveVehicleWizardFirstPageFragment;
	}

	public void showDialog() {
		if (mDialog == null) {
			mDialog = new Dialog(mReceiveVehicleWizardFirstPageFragment.getActivity());
		}
		mDialog.setContentView(R.layout.dialog_choose_ocr_or_type);


		cardGoToOCR = (CardView) mDialog.findViewById(R.id.cardGotoOCR);
		cardGoToType = (CardView) mDialog.findViewById(R.id.cardGotoType);
		cardGoToOCR.setCardBackgroundColor(mReceiveVehicleWizardFirstPageFragment.getResources().getColor(R.color.colorReportIssue));
		cardGoToType.setCardBackgroundColor(mReceiveVehicleWizardFirstPageFragment.getResources().getColor(R.color.colorNewIssue));
		mGoToOCR = (MaterialRippleLayout) mDialog.findViewById(R.id.MRLGotoOCR);
		mGoToType = (MaterialRippleLayout) mDialog.findViewById(R.id.MRLGotoType);
		rlCancel = mDialog.findViewById(R.id.rlCancel);
		initDialogButtons();
		mDialog.show();
	}

	private void initDialogButtons() {

		mGoToOCR.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				mReceiveVehicleWizardFirstPageFragment.callGoToOCRFragment();
			mDialog.dismiss();
			}
		});

		mGoToType.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				mReceiveVehicleWizardFirstPageFragment.createAndShowPopUp();
				mDialog.dismiss();
			}
		});

		rlCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				mDialog.dismiss();
			}
		});


	}

	public void dismissDialog() {
		mDialog.dismiss();
	}
}
