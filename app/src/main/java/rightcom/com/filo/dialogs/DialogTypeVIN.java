package rightcom.com.filo.dialogs;

import android.app.Dialog;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import rightcom.com.filo.R;
import rightcom.com.filo.app.Filo;
import rightcom.com.filo.fragments.ReceiveVehicleWizardFirstPageFragment;
import rightcom.com.filo.fragments.ReceiveVehicleWizardSecondPageFragment;
import rightcom.com.filo.models.SettingModel;
import rightcom.com.filo.window.ReceiveVehicleActivity;

public class DialogTypeVIN {

	private ReceiveVehicleWizardFirstPageFragment mReceiveVehicleWizardFirstPageFragment;
	private Dialog mDialog;
	private SettingModel currentsetting;

	public DialogTypeVIN(ReceiveVehicleWizardFirstPageFragment mReceiveVehicleWizardFirstPageFragment) {
		this.mReceiveVehicleWizardFirstPageFragment = mReceiveVehicleWizardFirstPageFragment;
	}

	public void showDialog() {
		if (mDialog == null) {
			mDialog = new Dialog(mReceiveVehicleWizardFirstPageFragment.getActivity());
		}
		mDialog.setContentView(R.layout.dialog_typevin);

		currentsetting=Filo.getSetting();
		TextView tvPopUpTitle = mDialog.findViewById(R.id.tvPopupTitle);
		TextView tvPopUpInfo = mDialog.findViewById(R.id.tvInfo);

		final TextInputLayout tilVIN = mDialog.findViewById(R.id.tilVIN);

		RelativeLayout rlCancel = mDialog.findViewById(R.id.rlCancel);
		TextView tvCancel = mDialog.findViewById(R.id.tvCancel);

		RelativeLayout rlSave = mDialog.findViewById(R.id.rlSave);
		TextView tvSave = mDialog.findViewById(R.id.tvSave);



		tvPopUpTitle.setText("Châssis");
		tvPopUpInfo.setText("Veuillez saisir le code du châssis.");

		rlCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mDialog.dismiss();
			}
		});
		rlSave.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String editCode = tilVIN.getEditText().getText().toString();
				if (Filo.isValidText(editCode,currentsetting.getocrmintextlength())) {
					mDialog.dismiss();
					ReceiveVehicleWizardSecondPageFragment.VIN_SCANNED = editCode;
					ReceiveVehicleActivity.swipeto(2);
				} else {
					tilVIN.setError("Veuillez saisir un code valide, s'il vous plaît.");
					tilVIN.getEditText().setText("");
				}
			}
		});
		mDialog.show();
	}



	public void dismissDialog() {
		mDialog.dismiss();
	}
}
