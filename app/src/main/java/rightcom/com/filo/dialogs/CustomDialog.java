package rightcom.com.filo.dialogs;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import rightcom.com.filo.R;
import rightcom.com.filo.R.string;
import rightcom.com.filo.app.Filo;

import com.zl.reik.dilatingdotsprogressbar.DilatingDotsProgressBar;

public class CustomDialog {

    Dialog dialog;
    Button btn, btnCancel;
    public final static Dialog createDialog(final Context context) {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(context.getResources().getString(string.connecting));
        progressDialog.setMessage(context.getResources().getString(string.connecting));
        progressDialog.setCancelable(false);
        return progressDialog;
    }
    public final static Dialog callLoadingDialog(final Context context)
    {
        Dialog dialog = new Dialog(context,
                android.R.style.Theme_Translucent);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(true);
        dialog.setContentView(R.layout.layout_loadingdialog);
        DilatingDotsProgressBar mDilatingDotsProgressBar = (DilatingDotsProgressBar)dialog.findViewById(R.id.progress);
        // show progress bar and start animating
        mDilatingDotsProgressBar.setNumberOfDots(4);
        mDilatingDotsProgressBar.showNow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0x7f000000));
        return dialog;
    }
    /*public final static Dialog createCustomDialog(final Context context)
    {
        Dialog dialog = new Dialog(context,
                android.R.style.Theme_Translucent);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(true);
        dialog.setContentView(R.layout.layout_loadingdialog);
        DilatingDotsProgressBar mDilatingDotsProgressBar = (DilatingDotsProgressBar)dialog.findViewById(R.id.progress);
        // show progress bar and start animating
        mDilatingDotsProgressBar.setNumberOfDots(4);
        mDilatingDotsProgressBar.showNow();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0x7f000000));
        return dialog;
    }*/

    public final static Dialog createSahasraraDialog(final Context context,final String header,final String message, Boolean canshow_OKButton, Boolean canshow_cancelButton)
    {
        Dialog dialog = new Dialog(context,
                android.R.style.Theme_Translucent);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(true);
        dialog.setContentView(R.layout.layout_dialog_sahasrara);
        final TextView Txtheader;
        final  TextView Txtmessage;
        final Button btnCancel;
        final Button btnOK;
        Txtheader = (TextView) dialog.findViewById(R.id.dialog_title);
        Txtmessage = (TextView) dialog.findViewById(R.id.dialog_text);
        btnOK = (Button) dialog.findViewById(R.id.btnOK);
        Txtheader.setText(header);
        Txtmessage.setText(message);


        btnOK.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Filo.appDialog.dismiss();
            }

        });


        return dialog;
    }
    public final static Dialog createCustomDialogWithTextOnly(final Context context,final String header,final String message)
    {
        Dialog dialog = new Dialog(context,
                android.R.style.Theme_Translucent);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(true);
        dialog.setContentView(R.layout.layout_dialog_with_text);
        final TextView Txtheader;
       final  TextView Txtmessage;
        Txtheader = (TextView) dialog.findViewById(R.id.dialog_title);
        Txtmessage = (TextView) dialog.findViewById(R.id.dialog_text);
        Txtheader.setText(header);
        Txtmessage.setText(message);
        // btnCancel = (Button) dialog.findViewById(R.id.btncancel);

        /*btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                dialog.cancel();
            }

        });*/

        final ImageView myImage = (ImageView) dialog.findViewById(R.id.loader);
        myImage.startAnimation(AnimationUtils.loadAnimation(context, R.anim.rotate) );

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0x7f000000));
        return dialog;
    }
    public final static Dialog createCustomDialogViewTicket(final Context context,final String header,final String message)
    {
        Dialog dialog = new Dialog(context,
                android.R.style.Theme_Translucent);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(true);
        dialog.setContentView(R.layout.layout_dialog_with_text);
        final TextView Txtheader;
        final  TextView Txtmessage;
        Txtheader = (TextView) dialog.findViewById(R.id.dialog_title);
        Txtmessage = (TextView) dialog.findViewById(R.id.dialog_text);
        Txtheader.setText(header);
        Txtmessage.setText(message);
        // btnCancel = (Button) dialog.findViewById(R.id.btncancel);

        /*btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                dialog.cancel();
            }

        });*/

        final ImageView myImage = (ImageView) dialog.findViewById(R.id.loader);
        myImage.startAnimation(AnimationUtils.loadAnimation(context, R.anim.rotate) );

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0x7f000000));
        return dialog;
    }
    /*protected void showCustomDialog(View v) {

        Dialog dialog = new Dialog(getActivity(),
                android.R.style.Theme_Translucent);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(true);
        dialog.setContentView(R.layout.layout_dialog);

        btnCancel = (Button) dialog.findViewById(R.id.btncancel);

        btnCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                dialog.cancel();
            }

        });

        final ImageView myImage = (ImageView) dialog.findViewById(R.id.loader);
        myImage.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.rotate) );

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0x7f000000));

        dialog.show();
    }*/

}
