package rightcom.com.filo.dialogs;

import android.app.Dialog;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import rightcom.com.filo.R;
import rightcom.com.filo.app.Filo;
import rightcom.com.filo.fragments.ReceiveVehicleWizardFirstPageFragment;
import rightcom.com.filo.fragments.ReceiveVehicleWizardSecondPageFragment;
import rightcom.com.filo.fragments.SettingsWizardFirstPageFragment;
import rightcom.com.filo.window.ReceiveVehicleActivity;

public class DialogSettingsEditOCRTextLength {

	private SettingsWizardFirstPageFragment mSettingsWizardFirstPageFragment;
	private Dialog mDialog;

	public DialogSettingsEditOCRTextLength(SettingsWizardFirstPageFragment mSettingsWizardFirstPageFragment) {
		this.mSettingsWizardFirstPageFragment = mSettingsWizardFirstPageFragment;
	}

	public void showDialog() {
		if (mDialog == null) {
			mDialog = new Dialog(mSettingsWizardFirstPageFragment.getActivity());
		}
		mDialog.setContentView(R.layout.dialog_settingseditocrtextlength);


		TextView tvPopUpTitle = mDialog.findViewById(R.id.tvPopupTitle);
		TextView tvPopUpInfo = mDialog.findViewById(R.id.tvInfo);

		final TextInputLayout tilDEFAULTVINLenth = mDialog.findViewById(R.id.tilDEFAULTVINLenth);
		final String minvalue=mSettingsWizardFirstPageFragment.currentSetting.getocrmintextlength()+"";
		tilDEFAULTVINLenth.getEditText().setText(minvalue);


		RelativeLayout rlCancel = mDialog.findViewById(R.id.rlCancel);
		TextView tvCancel = mDialog.findViewById(R.id.tvCancel);

		RelativeLayout rlSave = mDialog.findViewById(R.id.rlSave);
		TextView tvSave = mDialog.findViewById(R.id.tvSave);



		tvPopUpTitle.setText("Longueur");
		tvPopUpInfo.setText("Veuillez sélectionner la longueur minimum.");

		rlCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mDialog.dismiss();
			}
		});
		rlSave.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String valueMin = tilDEFAULTVINLenth.getEditText().getText().toString();
				mSettingsWizardFirstPageFragment.currentSetting.setocrmintextlength(Integer.parseInt(valueMin));
				Filo.setSetting(mSettingsWizardFirstPageFragment.currentSetting);
				mSettingsWizardFirstPageFragment.setOCRMinLengthCharactersSetting();
				dismissDialog();

			}
		});
		mDialog.show();
	}



	public void dismissDialog() {
		mDialog.dismiss();
	}
}
