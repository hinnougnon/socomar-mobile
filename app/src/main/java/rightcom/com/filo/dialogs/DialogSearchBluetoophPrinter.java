package rightcom.com.filo.dialogs;

import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Set;

import rightcom.com.filo.LoginPageActivity;
import rightcom.com.filo.R;
import rightcom.com.filo.adapters.SpinnerPrinterAdapter;
import rightcom.com.filo.app.Filo;
import rightcom.com.filo.fragments.ReceiveVehicleWizardFirstPageFragment;
import rightcom.com.filo.fragments.ReceiveVehicleWizardSecondPageFragment;
import rightcom.com.filo.fragments.SettingsWizardFirstPageFragment;
import rightcom.com.filo.models.PrinterModel;
import rightcom.com.filo.window.ReceiveVehicleActivity;

public class DialogSearchBluetoophPrinter {

	private SettingsWizardFirstPageFragment mSettingsWizardFirstPageFragment;
	private Dialog mDialog;
	private TextView tvInfo;
	private Spinner spinner_printers;
	private SpinnerPrinterAdapter spinnerprinterAdapter;

	// android built in classes for bluetooth operations
	//BluetoothAdapter mBluetoothAdapter;
	//BluetoothSocket mmSocket;
	//BluetoothDevice mmDevice;


	// needed for communication to bluetooth device / network
	OutputStream mmOutputStream;
	InputStream mmInputStream;
	Thread workerThread;

	byte[] readBuffer;
	int readBufferPosition;
	volatile boolean stopWorker;
	private ArrayList<PrinterModel> printers;
	private PrinterModel currentprinter;
	private RelativeLayout rlCancel;

	private RelativeLayout rlSave ;

	public DialogSearchBluetoophPrinter(final SettingsWizardFirstPageFragment mSettingsWizardFirstPageFragment) {
		this.mSettingsWizardFirstPageFragment = mSettingsWizardFirstPageFragment;
		if (mDialog == null) {
			mDialog = new Dialog(mSettingsWizardFirstPageFragment.getContext());
		}
		mDialog.setContentView(R.layout.dialog_search_printer);
		tvInfo =mDialog.findViewById(R.id.tvInfo);
		spinner_printers =mDialog.findViewById(R.id.spinner_printers);
		printers=new ArrayList<PrinterModel>();
		currentprinter=new PrinterModel();

		 RelativeLayout rlCancel = mDialog.findViewById(R.id.rlCancel);
		 RelativeLayout rlSave = mDialog.findViewById(R.id.rlSave);
		rlCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mDialog.dismiss();
			}
		});
		rlSave.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				Filo.setPrinter(currentprinter);
				mSettingsWizardFirstPageFragment.setDefaultBluetoothSetting();
				mDialog.dismiss();
			}
		});
	}
	//Performing action onItemSelected and onNothing selected
	/*@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int position,long id) {
		//Toast.makeText(getApplicationContext(), countryNames[position], Toast.LENGTH_LONG).show();
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
	}*/

	public void showDialog() {
		findPairedBT();


		//Getting the instance of Spinner and applying OnItemSelectedListener on it
		spinnerprinterAdapter = new SpinnerPrinterAdapter(mSettingsWizardFirstPageFragment.getContext(), R.layout.custom_printer_spinner,printers );

		spinner_printers.setAdapter(spinnerprinterAdapter);


		spinner_printers.post(new Runnable() {
			@Override
			public void run() {
				selectSpinnerItemByValue(spinner_printers, mSettingsWizardFirstPageFragment.currentPrinter.getdddressmac().toString());
				//int position_to_select=spinnerprinterAdapter.getPosition(mSettingsWizardFirstPageFragment.currentPrinter);
				//spinner_printers.setSelection(position_to_select);
			}
		});

		// handle click event and set desc on textview


		spinner_printers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
				currentprinter= (PrinterModel) parent.getItemAtPosition(pos);

			}

			public void onNothingSelected(AdapterView<?> parent) {

			}
		});

		mDialog.show();
	}
	public static void selectSpinnerItemByValue(Spinner spnr, String mac_adress) {
		SpinnerPrinterAdapter adapter = (SpinnerPrinterAdapter) spnr.getAdapter();
		for (int position = 0; position < adapter.getCount(); position++) {
			PrinterModel curpr=new PrinterModel();
			curpr=adapter.getItem(position);
			if(curpr.getdddressmac()!=null && curpr.getdddressmac().equals(mac_adress)) {
				spnr.setSelection(position);
				return;
			}
			else {
				spnr.setSelection(0);

			}
		}
	}

	public void findPairedBT() {

		try {
			// Set one element at  first row. the none selection
			PrinterModel mp= new PrinterModel();
			mp.setname(mSettingsWizardFirstPageFragment.getResources().getText(R.string.label_no_selection).toString());
			mp.setaddressmac(null);
			printers.add(mp);

			Set<BluetoothDevice> pairedDevices = mSettingsWizardFirstPageFragment.ListPairedDevices();

			if(pairedDevices.size() > 0) {
				for (BluetoothDevice device : pairedDevices) {

					//Check if device is a printer
					if (device.getBluetoothClass().getDeviceClass() == 1664) {
						PrinterModel curprinter= new PrinterModel();
						curprinter.setname(device.getName());
						curprinter.setaddressmac(device.getAddress());
						printers.add(curprinter);
					}

				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void dismissDialog() {
		mDialog.dismiss();
	}
}
