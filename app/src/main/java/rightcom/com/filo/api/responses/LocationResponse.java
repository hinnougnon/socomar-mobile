package rightcom.com.filo.api.responses;

import com.google.gson.annotations.SerializedName;

public class LocationResponse {

	public void setlockey(String uilockey) {
		this.lockey = uilockey;
	}
	public String getlockey() {
		return lockey;
	}

	public void setlocation(String uilocation) {
		this.location = uilocation;
	}
	public String getlocation() {
		return location;
	}


	public static synchronized LocationResponse getInstance() {
		if (instance == null) {
			instance = new LocationResponse();

		}
		return instance;
	}
	@SerializedName("Lockey")
	private String lockey = "0";
	@SerializedName("Location")
	private String location;
	private static LocationResponse instance = null;
}
