package rightcom.com.filo.api.responses;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by HOD on 6/12/15.
 */
public class CheckVINResponse {


    @SerializedName("Vin")
    private String Vin;
    public String getVin() {
        return Vin;
    }
    public void setVin(String Vin) {
        this.Vin = Vin;
    }

    @SerializedName("Vinkey")
    private String Vinkey;
    public String getVinkey() {
        return Vinkey;
    }
    public void setVinkey(String Vinkey) {
        this.Vinkey = Vinkey;
    }
    @SerializedName("Msg")
    private String Msg;
    public String getMsg() {
        return Msg;
    }
    public void setMsg(String Msg) {
        this.Msg = Msg;
    }

    @SerializedName("Statut")
    private String Statut;
    public String getStatut() {
        return Statut;
    }
    public void setStatut(String Statut) {
        this.Statut = Statut;
    }

    @SerializedName("BL")
    private String BL;
    public String getBL() {
        return BL;
    }
    public void setBL(String BL) {
        this.BL = BL;
    }

    @SerializedName("Description")
    private String Description;
    public String getDescription() {
        return Description;
    }
    public void setDescription(String Description) {
        this.Description = Description;
    }

    @SerializedName("QrCode")
    private String QrCode;
    public String getQrCode() {
        return QrCode;
    }
    public void setQrCode(String QrCode) {
        this.QrCode = QrCode;
    }

    @SerializedName("PayDate")
    private String PayDate;
    public String getPayDate() {
        return PayDate;
    }
    public void setPayDate(String PayDate) {
        this.PayDate = PayDate;
    }

    @SerializedName("PayDay")
    private String PayDay;
    public String getPayDay() {
        return PayDay;
    }
    public void setPayDay(String PayDay) {
        this.PayDay = PayDay;
    }

    @SerializedName("FreeToGo")
    private String FreeToGo;
    public String getFreeToGo() {
        return FreeToGo;
    }
    public void setFreeToGo(String FreeToGo) {
        this.FreeToGo = FreeToGo;
    }

    @SerializedName("Delevery")
    private String Delevery;
    public String getDelevery() {
        return Delevery;
    }
    public void setDelevery(String Delevery) {
        this.Delevery = Delevery;
    }

    @SerializedName("DeleveryDate")
    private String DeleveryDate;
    public String getDeleveryDate() {
        return DeleveryDate;
    }
    public void setDeleveryDate(String DeleveryDate) {
        this.DeleveryDate = DeleveryDate;
    }

    @SerializedName("Locations")
    private ArrayList<LocationResponse> locations;
    public ArrayList<LocationResponse> getLocations() {
        return locations;
    }
    public void setLocations(ArrayList<LocationResponse> locations) {
        this.locations = locations;
    }


}
