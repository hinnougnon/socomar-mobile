package rightcom.com.filo.api.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by HOD on 6/12/15.
 */
public class SigninResponse {
    /*@SerializedName("id")
    private String id;
    public String getid() {
        return id;
    }
    public void setid(String id) {
        this.id = id;
    }
    @SerializedName("token")
    private String token;
    public String gettoken() {
        return token;
    }
    public void settoken(String token) {
        this.token = token;
    }
    @SerializedName("firstname")
    private String firstname;
    public String getfirstname() {
        return firstname;
    }
    public void setfirstname(String firstname) {
        this.firstname = firstname;
    }
    @SerializedName("lastname")
    private String lastname;
    public String getlastname() {
        return lastname;
    }
    public void setlastname(String lastname) {
        this.lastname = lastname;
    }
    @SerializedName("language")
    private String language;
    public String getlanguage() {
        return language;
    }
    public void setlanguage(String language) {
        this.language = language;
    }
    @SerializedName("countrycode")
    private String countrycode;
    public String getcountrycode() {
        return countrycode;
    }
    public void setcountrycode(String countrycode) {
        this.countrycode = countrycode;
    }
    @SerializedName("currency")
    private String currency;
    public String getcurrency() {
        return currency;
    }
    public void setcurrency(String currency) {
        this.currency = currency;
    }
    @SerializedName("country")
    private String country;
    public String getcountry() {
        return country;
    }
    public void setcountry(String country) {
        this.country = country;
    }
*/


    @SerializedName("User")
    private String User;
    public String getUser() {
        return User;
    }
    public void setUser(String User) {
        this.User = User;
    }

    @SerializedName("Msg")
    private String Msg;
    public String getMsg() {
        return Msg;
    }
    public void setMsg(String Msg) {
        this.Msg = Msg;
    }

    @SerializedName("Statut")
    private String Statut;
    public String getStatut() {
        return Statut;
    }
    public void setStatut(String Statut) {
        this.Statut = Statut;
    }

    @SerializedName("Right")
    private String[] Right;
    public String[] getRight() {
        return Right;
    }
    public void setRight(String[] Right) {
        this.Right = Right;
    }

    @SerializedName("Ukey")
    private String Key;
    public String getkey() {
        return Key;
    }
    public void setKey(String Key) {
        this.Key = Key;
    }

}
