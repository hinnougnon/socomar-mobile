package rightcom.com.filo.api;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import rightcom.com.filo.app.Filo;
import rightcom.com.filo.models.SettingModel;

/**
 * Created by HOD on 10/23/16.
 */

public class ApiServiceGenerator {

    //public static String API_BASE_URL = "https://41.216.232.52/";
    public static String API_BASE_URL = "";
    private static Retrofit retrofit = null;
    public static Retrofit getClient() {
        if (retrofit==null) {
            SettingModel currentsetting = Filo.getSetting();
            if(currentsetting.getdddressip()!=null && !currentsetting.getdddressip().isEmpty()){
                API_BASE_URL = "https://" + currentsetting.getdddressip()+"/";
            }
            OkHttpClient okHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient();
            retrofit = new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();


        }
        return retrofit;
    }
}
