package rightcom.com.filo.api.interfaces;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rightcom.com.filo.api.responses.CargettingResponse;
import rightcom.com.filo.api.responses.CheckVINResponse;
import rightcom.com.filo.api.responses.SigninResponse;


/**
 * Created by HOD on 10/24/18.
 */

public interface ApiEndpointInterface {
    @GET("api/mobile/auth")
    Call<SigninResponse> signIn(@Query("name") String username, @Query("pass") String password, @Query("equipment") String source);
    @GET("api/mobile/vin")
    Call<CheckVINResponse> lookforVIN(@Query("numvin") String vin, @Query("step") String step, @Query("iduser") String userid);
    @GET("api/mobile/caroperation")
    Call<CargettingResponse> cargetting(@Query("numvin") String vin, @Query("mvt") String mvt, @Query("location") String location, @Query("img") String img, @Query("userid") String userid);
}

