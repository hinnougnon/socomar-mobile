package rightcom.com.filo.api.responses;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by HOD on 6/12/15.
 */
public class CargettingResponse {




    @SerializedName("Msg")
    private String Msg;
    public String getMsg() {
        return Msg;
    }
    public void setMsg(String Msg) {
        this.Msg = Msg;
    }

    @SerializedName("Statut")
    private String Statut;
    public String getStatut() {
        return Statut;
    }
    public void setStatut(String Statut) {
        this.Statut = Statut;
    }


}
