package rightcom.com.filo.viewHolders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Objects;
import java.util.Random;

import rightcom.com.filo.R;
import rightcom.com.filo.adapters.OCRFragScanTextsAdapter;

public class OCRFragScanTextsHolder extends RecyclerView.ViewHolder {

    private RelativeLayout relativeLayout;
    private TextView tvScanText;

    private OnItemClick listener;
    private OCRFragScanTextsAdapter mContext;
    private int[] randomColors;

    public OCRFragScanTextsHolder(@NonNull final View itemView, OCRFragScanTextsAdapter context) {
        super(itemView);

        this.mContext = context;
        if (context instanceof OnItemClick) {
            listener = (OnItemClick) context;
        }

        relativeLayout = itemView.findViewById(R.id.relativeLayout);
        tvScanText = itemView.findViewById(R.id.tvScanText);

        if (this.mContext.getFragActivityContext() != null) {
            randomColors = this.mContext.getFragActivityContext().getResources().getIntArray(R.array.status);
            tvScanText.setTextColor(Objects.requireNonNull(this.mContext.getFragActivityContext().getActivity()).getResources().getColor(R.color.colorWhite));
        }
    }

    public void display(String text) {
        tvScanText.setText(text);
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClickListener(tvScanText.getText().toString());
            }
        });
        initViewColor();
    }

    private void initViewColor() {
        relativeLayout.setBackgroundColor(randomColors[new Random().nextInt(randomColors.length-1)]);
    }

    public interface OnItemClick {
        void onClickListener(String s);
    }

}
