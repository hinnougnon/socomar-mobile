package rightcom.com.filo.viewHolders;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;

import java.util.Locale;
import java.util.Random;

import rightcom.com.filo.R;
import rightcom.com.filo.models.Issue;
import rightcom.com.filo.tools.Utils;
import rightcom.com.filo.tools.Value;

public class IssueStoryHolder extends RecyclerView.ViewHolder implements MaterialRippleLayout.OnClickListener {
    private View view;
    private TextView tvDuration;
    private TextView tvTitle;
    private TextView tvComment;
    private TextView tvState;
    private MaterialRippleLayout mrlHistory;
    private RelativeLayout rlvState;

    private int[] statusColor;
    private IssueStroyHolderListener mListener;

    public IssueStoryHolder(@NonNull View itemView) {
        super(itemView);

        view = itemView.findViewById(R.id.view);
        tvDuration = itemView.findViewById(R.id.tvDuration);
        tvTitle = itemView.findViewById(R.id.tvTitle);
        tvComment = itemView.findViewById(R.id.tvComment);
        tvState = itemView.findViewById(R.id.tvState);
        mrlHistory = itemView.findViewById(R.id.mrlHistory);
        rlvState = itemView.findViewById(R.id.rlvState);

        statusColor = itemView.getContext().getResources().getIntArray(R.array.status);

        if (itemView.getContext() instanceof IssueStroyHolderListener) {
            mListener = (IssueStroyHolderListener) itemView.getContext();
        }

        view.setBackgroundColor(statusColor[getColorPosition()]);
    }

    public void display(Context context, Issue issue) {
        if (Locale.getDefault().getLanguage().equals("en")) {
            tvTitle.setText(issue.getIssue_service_en());
        } else {
            tvTitle.setText(issue.getIssue_service_fr());
        }
        if (issue.getIssue_comment().isEmpty()) {
            tvComment.setText(context.getString(R.string.text_aucun_commentaire));
        } else {
            tvComment.setText(issue.getIssue_comment());
        }
        tvDuration.setText(Utils.dateDifference(context, issue.getIssue_survey_date()));
        mrlHistory.setOnClickListener(this);
        if (issue.getIssue_status() == Value.NEW_STATUS) {
            rlvState.setBackgroundResource(R.drawable.background_new_issue);
            view.setBackgroundColor(statusColor[0]);
            tvState.setText(context.getString(R.string.text_new));
        } else if (issue.getIssue_status() == Value.PENDING_STATUS) {
            rlvState.setBackgroundResource(R.drawable.background_pending_issue);
            view.setBackgroundColor(statusColor[1]);
            tvState.setText(context.getString(R.string.text_en_cours));
        } else if (issue.getIssue_status() == Value.SOLVED_STATUS) {
            rlvState.setBackgroundResource(R.drawable.background_solved_issue);
            view.setBackgroundColor(statusColor[2]);
            tvState.setText(context.getString(R.string.text_resolu));
        }
    }

    private int getColorPosition() {
        return new Random().nextInt(statusColor.length - 0) + 0;
    }

    @Override
    public void onClick(View view) {
//        onHistoryClickListener.onClick(issue);
    }

    interface IssueStroyHolderListener {
        void onIssueStoryHolderListener();
    }
}
