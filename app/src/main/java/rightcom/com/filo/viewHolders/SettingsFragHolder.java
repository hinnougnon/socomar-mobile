package rightcom.com.filo.viewHolders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Objects;
import java.util.Random;

import rightcom.com.filo.R;
import rightcom.com.filo.adapters.OCRFragScanTextsAdapter;
import rightcom.com.filo.adapters.SettingsFragAdapter;
import rightcom.com.filo.app.Filo;

public class SettingsFragHolder extends RecyclerView.ViewHolder {

    private RelativeLayout rlSettingsItem;
    private TextView tvSettingsItem;
    private TextView tvSettingsSubItem;

    private OnItemClick listener;
    private SettingsFragAdapter mContext;

//    private int[] randomColors;
    private int position = 0;

    private Filo app;

    public SettingsFragHolder(@NonNull final View itemView, SettingsFragAdapter context) {
        super(itemView);

        this.mContext = context;
        if (context instanceof OnItemClick) {
            listener = (OnItemClick) context;
        }

        app = (Filo) Objects.requireNonNull(this.mContext.getFragActivityContext().getActivity()).getApplication();

        rlSettingsItem = itemView.findViewById(R.id.rlSettingsItem);
        tvSettingsItem = itemView.findViewById(R.id.tvSettingsItem);
        tvSettingsSubItem = itemView.findViewById(R.id.tvSettingsSubItem);

        rlSettingsItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClickListener(tvSettingsItem.getText().toString(), position);
            }
        });

        initTypeFace();

//        if (this.mContext.getFragActivityContext() != null) {
//            randomColors = this.mContext.getFragActivityContext().getResources().getIntArray(R.array.status);
//            tvScanText.setTextColor(Objects.requireNonNull(this.mContext.getFragActivityContext().getActivity()).getResources().getColor(R.color.colorWhite));
//        }
    }

    public void display(String key, String value, int position) {
        this.position = position;
        tvSettingsItem.setText(key);
        if (!value.isEmpty()) {
            tvSettingsSubItem.setText(value);
        } else {
            tvSettingsSubItem.setVisibility(View.GONE);
        }
//        initViewColor();
    }

    private void initTypeFace() {
        tvSettingsItem.setTypeface(app.getTypeFace());
        tvSettingsSubItem.setTypeface(app.getTypeFace());
    }

//    private void initViewColor() {
//        relativeLayout.setBackgroundColor(randomColors[new Random().nextInt(randomColors.length-1)]);
//    }

    public interface OnItemClick {
        void onClickListener(String s, int i);
    }
}
