/***********************************************
 * CONFIDENTIAL AND PROPRIETARY 
 * 
 * The source code and other information contained herein is the confidential and the exclusive property of
 * ZIH Corp. and is subject to the terms and conditions in your end user license agreement.
 * This source code, and any other information contained herein, shall not be copied, reproduced, published, 
 * displayed or distributed, in whole or in part, in any medium, by any means, for any purpose except as
 * expressly permitted under such license agreement.
 * 
 * Copyright ZIH Corp. 2012
 * 
 * ALL RIGHTS RESERVED
 ***********************************************/

package rightcom.com.filo.printers.zebra;

import android.widget.Toast;

import com.zebra.sdk.comm.BluetoothConnection;
import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.printer.ZebraPrinterFactory;
import com.zebra.sdk.printer.ZebraPrinterLanguageUnknownException;

import rightcom.com.filo.app.Filo;
import rightcom.com.filo.models.PrinterModel;

public class ZebraLabelPrinter {

    private boolean sendData = true;
    Connection printerConnection = null;
    public ZebraLabelPrinter() { }

    public void connectAndSendLabelJob(String VIN_NUMBER,String BL) {
        PrinterModel objprinter = Filo.getPrinter();
        if(objprinter.getdddressmac()!=null && !objprinter.getdddressmac().isEmpty()){
            printerConnection = new BluetoothConnection(objprinter.getdddressmac());
            try {
                //helper.showLoadingDialog("Connecting...");
                printerConnection.open();

                com.zebra.sdk.printer.ZebraPrinter printer = null;

                if (printerConnection.isConnected()) {
                    printer = ZebraPrinterFactory.getInstance(printerConnection);

                    if (printer != null) {
                        sendJob(printerConnection,VIN_NUMBER,BL);
                        printerConnection.close();
                    }
                }
            } catch (ConnectionException e) {
                //helper.showErrorDialogOnGuiThread(e.getMessage());
                Toast.makeText(Filo.getContext(), "Printer error: "+e.getMessage(),
                        Toast.LENGTH_SHORT).show();
            } catch (ZebraPrinterLanguageUnknownException e) {
                //helper.showErrorDialogOnGuiThread("Could not detect printer language");
                Toast.makeText(Filo.getContext(), "Printer error: Could not detect printer language",
                        Toast.LENGTH_SHORT).show();
            } finally {
                //helper.dismissLoadingDialog();
                Filo.appDialog.dismiss();
            }
        }
        else{
            Toast.makeText(Filo.getContext(), "Printer error: Could not detect default printer",
                    Toast.LENGTH_SHORT).show();
            Filo.appDialog.dismiss();
        }

    }
    private void sendJob(Connection printerConnection,String VIN_NUMBER,String BL) {
        try {
            formatTextAndPrint(printerConnection,VIN_NUMBER,BL);
        } catch (ConnectionException e) {
            //helper.showErrorDialogOnGuiThread(e.getMessage());
            Toast.makeText(Filo.getContext(), "Printer error: "+e.getMessage(),
                    Toast.LENGTH_SHORT).show();
        }

    }
    private void formatTextAndPrint(Connection printerConnection, String VIN_NUMBER, String BL) throws ConnectionException {
        /*
         This routine is provided to you as an example of how to create a variable length label with user specified data.
         The basic flow of the example is as follows

            Header of the label with some variable data
            Body of the label
                Loops thru user content and creates small line items of printed material
            Footer of the label

         As you can see, there are some variables that the user provides in the header, body and footer, and this routine uses that to build up a proper ZPL string for printing.
         Using this same concept, you can create one label for your receipt header, one for the body and one for the footer. The body receipt will be duplicated as many items as there are in your variable data

         */
        //String VIN_NUMBER="56TGSFGSJSJSGFS5";
        //String BL="BL12";
        String textToPrint =
        /*
         Some basics of ZPL. Find more information here : http://www.zebra.com

         ^XA indicates the beginning of a label
         ^PW sets the width of the label (in dots)
         ^MNN sets the printer in continuous mode (variable length receipts only make sense with variably sized labels)
         ^LL sets the length of the label (we calculate this value at the end of the routine)
         ^LH sets the reference axis for printing.
            You will notice we change this positioning of the 'Y' axis (length) as we build up the label. Once the positioning is changed, all new fields drawn on the label are rendered as if '0' is the new home position
         ^FO sets the origin of the field relative to Label Home ^LH
         ^A sets font information
         ^FD is a field description
         ^GB is graphic boxes (or lines)
         ^B sets barcode information
         ^XZ indicates the end of a label
         */

                "^XA" +

                        "^POI^PW800^MNN^LL380^LH0,0" + "\r\n" +

                        "^FO50,5" + "\r\n" + "^A0,N,50,50" + "\r\n" + "^FD SOCOMAR^FS" + "\r\n" +

                        "^FO40,50" + "\r\n" + "^A0,N,35,35" + "\r\n" + "^FDEtiquette d'identification^FS" + "\r\n" +

                        "^FO10,100" + "\r\n" + "^A0,N,25,25" + "\r\n" + "^FDChassis:^FS" + "\r\n" +

                        "^FO100,100" + "\r\n" + "^A0,N,25,25" + "\r\n" + "^FD"+VIN_NUMBER+"^FS" + "\r\n" +

                        "^FO10,130" + "\r\n" + "^GB350,5,5,B,0^FS" +
                        "^FO10,145" + "\r\n" + "^A0,N,30,30" + "\r\n" + "^FDwww.socomar-cameroun.com^FS" + "\r\n" +

                        "^FO500,40,20^BQ,2,10^FDQA,"+VIN_NUMBER+"_"+BL+"^FS" + "\r\n" + "^XZ";

        printerConnection.write(textToPrint.getBytes());

    }
}
