package rightcom.com.filo.tools;

import android.content.Context;
import android.util.Log;
import android.util.SparseArray;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.text.TextBlock;

import rightcom.com.filo.ui.camera.GraphicOverlay;

/**
 * A very simple Processor which gets detected TextBlocks and adds them to the overlay
 * as OcrGraphics.
 */
public class OcrDetectorProcessor implements Detector.Processor<TextBlock>, OcrGraphic.OnOCRGraphicListener {

    private GraphicOverlay<OcrGraphic> graphicOverlay;
    private OnOCRDetectorProcessorListener mListener;
    private Context mContext;

    public OcrDetectorProcessor(GraphicOverlay<OcrGraphic> ocrGraphicOverlay, Context context) {
        graphicOverlay = ocrGraphicOverlay;
        if (context instanceof OnOCRDetectorProcessorListener) {
            mContext = context;
            mListener = (OnOCRDetectorProcessorListener) context;
        }
    }

    /**
     * Called by the detector to deliver detection results.
     * If your application called for it, this could be a place to check for
     * equivalent detections by tracking TextBlocks that are similar in location and content from
     * previous frames, or reduce noise by eliminating TextBlocks that have not persisted through
     * multiple detections.
     */
    @Override
    public void receiveDetections(Detector.Detections<TextBlock> detections) {
        graphicOverlay.clear();
        SparseArray<TextBlock> items = detections.getDetectedItems();
        for (int i = 0; i < items.size(); ++i) {
            TextBlock item = items.valueAt(i);
            if (item != null && item.getValue() != null) {
                Log.d("OcrDetectorProcessor", "Text detected! " + item.getValue());
                OcrGraphic graphic = new OcrGraphic(graphicOverlay, item, this);
                graphicOverlay.add(graphic);
            }
        }
        //graphicOverlay.clear();
    }

    /**
     * Frees the resources associated with this detection processor.
     */
    @Override
    public void release() {
        graphicOverlay.clear();
    }

    @Override
    public void onDrawFinish(Boolean finish) {
        mListener.onDrawFinishNotify(finish);
    }

    public interface OnOCRDetectorProcessorListener {
        void onDrawFinishNotify(Boolean b);
    }
}

