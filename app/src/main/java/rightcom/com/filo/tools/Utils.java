package rightcom.com.filo.tools;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.SpannableString;
import android.util.Log;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import rightcom.com.filo.R;

public class Utils {

    public static String dateDifference(Context context, String startDate) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date d1 = null;
        Date d2 = null;
        String result = null;

        try {
            d1 = format.parse(startDate);
            d2 = format.parse(format.format(Calendar.getInstance().getTime()));

            long diff = d2.getTime() - d1.getTime();
            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            switch ((int) diffDays) {
                case 0:
                    if (diffHours > (long)0) {
                        if (Locale.getDefault().getLanguage() == "en") {
                            result = ""+diffHours+"h"+" ago";
                        } else {
                            result = "Il y a "+diffHours+"h";
                        }
                    } else if (diffMinutes > (long)0) {
                        if (Locale.getDefault().getLanguage() == "en") {
                            result = ""+diffMinutes+"m"+" ago";
                        } else {
                            result = "Il y a "+diffMinutes+"m";
                        }
                    } else {
                        if (Locale.getDefault().getLanguage() == "en") {
                            result = ""+diffSeconds+"s"+" ago";
                        } else {
                            result = "Il y a "+diffSeconds+"s";
                        }
                    }
                    break;

                case 1:
                    context.getString(R.string.text_hier);
                    break;

                case 2:
                    if (Locale.getDefault().getLanguage() == "en") {
                        result = "2 days ago";
                    } else {
                        result = "Il y a 2 jours";
                    }
                    break;

                case 3:
                    if (Locale.getDefault().getLanguage() == "en") {
                        result = "3 days ago";
                    } else {
                        result = "Il y a 3 jours";
                    }
                    break;

                case 4:
                    if (Locale.getDefault().getLanguage() == "en") {
                        result = "4 days ago";
                    } else {
                        result = "Il ya 4 jours";
                    }
                    break;

                case 5:
                    if (Locale.getDefault().getLanguage() == "en") {
                        result = "5 days ago";
                    } else {
                        result = "Il y a 5 jours";
                    }
                    break;

                default:
                    Calendar mCalendar = Calendar.getInstance();
                    mCalendar.setTime(d1);
                    int day = mCalendar.get(Calendar.DAY_OF_MONTH);
                    int month = mCalendar.get(Calendar.MONTH) + 1;
                    int year = mCalendar.get(Calendar.YEAR);
                    result = day + " " + convertMonthToString(context, month) + " " + year;
                    break;
            }
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        }
        return result;
    }

    private static String convertMonthToString(Context context, int month) {
        String res = null;
        switch (month) {
            case 1 : context.getString(R.string.text_jan);break;
            case 2 : context.getString(R.string.text_fev);break;
            case 3 : context.getString(R.string.text_mars);break;
            case 4 : context.getString(R.string.text_avril);break;
            case 5 : context.getString(R.string.text_mai);break;
            case 6 : context.getString(R.string.text_juin);break;
            case 7 : context.getString(R.string.text_juil);break;
            case 8 : context.getString(R.string.text_aout);break;
            case 9 : context.getString(R.string.text_sept);break;
            case 10 : context.getString(R.string.text_oct);break;
            case 11 : context.getString(R.string.text_nov);break;
            case 12 : context.getString(R.string.text_dec);break;
            default:
                res = ""; break;
        }
        return res;
    }

    public static void loadFragmentWithAnim(FragmentManager fragmentManager, Fragment fragment,
                             Boolean addToBackStack,
                             int enterAnimation,
                             int exitAnimation,
                             int popEnterAnimation,
                             int popExitAnimation) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(enterAnimation, exitAnimation, popEnterAnimation, popExitAnimation);
        if (addToBackStack){
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.replace(R.id.frameLayout, fragment).commit();
    }

    public static void loadFragment(FragmentManager fragmentManager, int containerId, Fragment fragment, Boolean addToBackStack, String tag) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        if (addToBackStack){
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.replace(containerId, fragment, tag).commit();
    }

    public static void setSpanFont(CharSequence c, Typeface typeface, TextInputLayout textInputLayout) {
        SpannableString spannableString = SpannableString.valueOf(c);
        //spannableString.setSpan(new Cus);
    }


    public static final byte[] UNICODE_TEXT = new byte[] {0x23, 0x23, 0x23,
            0x23, 0x23, 0x23,0x23, 0x23, 0x23,0x23, 0x23, 0x23,0x23, 0x23, 0x23,
            0x23, 0x23, 0x23,0x23, 0x23, 0x23,0x23, 0x23, 0x23,0x23, 0x23, 0x23,
            0x23, 0x23, 0x23};

    private static String hexStr = "0123456789ABCDEF";
    private static String[] binaryArray = { "0000", "0001", "0010", "0011",
            "0100", "0101", "0110", "0111", "1000", "1001", "1010", "1011",
            "1100", "1101", "1110", "1111" };
    public static byte[] decodeBitmap(Bitmap bmp){
        int bmpWidth = bmp.getWidth();
        int bmpHeight = bmp.getHeight();

        List<String> list = new ArrayList<String>(); //binaryString list
        StringBuffer sb;


        int bitLen = bmpWidth / 8;
        int zeroCount = bmpWidth % 8;

        String zeroStr = "";
        if (zeroCount > 0) {
            bitLen = bmpWidth / 8 + 1;
            for (int i = 0; i < (8 - zeroCount); i++) {
                zeroStr = zeroStr + "0";
            }
        }

        for (int i = 0; i < bmpHeight; i++) {
            sb = new StringBuffer();
            for (int j = 0; j < bmpWidth; j++) {
                int color = bmp.getPixel(j, i);

                int r = (color >> 16) & 0xff;
                int g = (color >> 8) & 0xff;
                int b = color & 0xff;

                // if color close to white，bit='0', else bit='1'
                if (r > 160 && g > 160 && b > 160)
                    sb.append("0");
                else
                    sb.append("1");
            }
            if (zeroCount > 0) {
                sb.append(zeroStr);
            }
            list.add(sb.toString());
        }

        List<String> bmpHexList = binaryListToHexStringList(list);
        String commandHexString = "1D763000";
        String widthHexString = Integer
                .toHexString(bmpWidth % 8 == 0 ? bmpWidth / 8
                        : (bmpWidth / 8 + 1));
        if (widthHexString.length() > 2) {
            Log.e("decodeBitmap error", " width is too large");
            return null;
        } else if (widthHexString.length() == 1) {
            widthHexString = "0" + widthHexString;
        }
        widthHexString = widthHexString + "00";

        String heightHexString = Integer.toHexString(bmpHeight);
        if (heightHexString.length() > 2) {
            Log.e("decodeBitmap error", " height is too large");
            return null;
        } else if (heightHexString.length() == 1) {
            heightHexString = "0" + heightHexString;
        }
        heightHexString = heightHexString + "00";

        List<String> commandList = new ArrayList<String>();
        commandList.add(commandHexString+widthHexString+heightHexString);
        commandList.addAll(bmpHexList);

        return hexList2Byte(commandList);
    }
    public static List<String> binaryListToHexStringList(List<String> list) {
        List<String> hexList = new ArrayList<String>();
        for (String binaryStr : list) {
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < binaryStr.length(); i += 8) {
                String str = binaryStr.substring(i, i + 8);

                String hexString = myBinaryStrToHexString(str);
                sb.append(hexString);
            }
            hexList.add(sb.toString());
        }
        return hexList;

    }
    public static String myBinaryStrToHexString(String binaryStr) {
        String hex = "";
        String f4 = binaryStr.substring(0, 4);
        String b4 = binaryStr.substring(4, 8);
        for (int i = 0; i < binaryArray.length; i++) {
            if (f4.equals(binaryArray[i]))
                hex += hexStr.substring(i, i + 1);
        }
        for (int i = 0; i < binaryArray.length; i++) {
            if (b4.equals(binaryArray[i]))
                hex += hexStr.substring(i, i + 1);
        }

        return hex;
    }
    public static byte[] hexList2Byte(List<String> list) {
        List<byte[]> commandList = new ArrayList<byte[]>();

        for (String hexStr : list) {
            commandList.add(hexStringToBytes(hexStr));
        }
        byte[] bytes = sysCopy(commandList);
        return bytes;
    }

    public static byte[] hexStringToBytes(String hexString) {
        if (hexString == null || hexString.equals("")) {
            return null;
        }
        hexString = hexString.toUpperCase();
        int length = hexString.length() / 2;
        char[] hexChars = hexString.toCharArray();
        byte[] d = new byte[length];
        for (int i = 0; i < length; i++) {
            int pos = i * 2;
            d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));
        }
        return d;
    }

    public static byte[] sysCopy(List<byte[]> srcArrays) {
        int len = 0;
        for (byte[] srcArray : srcArrays) {
            len += srcArray.length;
        }
        byte[] destArray = new byte[len];
        int destLen = 0;
        for (byte[] srcArray : srcArrays) {
            System.arraycopy(srcArray, 0, destArray, destLen, srcArray.length);
            destLen += srcArray.length;
        }
        return destArray;
    }

    private static byte charToByte(char c) {
        return (byte) "0123456789ABCDEF".indexOf(c);
    }
}
