package rightcom.com.filo.tools;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import rightcom.com.filo.models.Issue;

public class SharedPrefrencesManger {

    PreferenceListener mListener = null;

    public SharedPrefrencesManger (PreferenceListener mListener) {
        this.mListener = mListener;
    }

    public void storeIssue(Context context, Issue issue) {
        SharedPreferences SP = context.getSharedPreferences("ISSUES", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = SP.edit();
        boolean result = editor.putString(issue.getIssue_survey_date(), new GsonBuilder().create().toJson(issue)).commit();
    }

    public void getIssues(Context context) {
        List<Issue> issues = new ArrayList<>();
        SharedPreferences SP = context.getSharedPreferences("ISSUES", Context.MODE_PRIVATE);
        Map<String, ?> all = SP.getAll();
        for (Object i: all.values()) {
            Issue issue = new GsonBuilder().create().fromJson(String.valueOf((i)), Issue.class);
            issues.add(issue);
        }
        mListener.onFinishToGetData(issues);
    }

    public void updateIssueByRating(Context context, String issue_survey_date, Float ratingValue) {
        // The variable issue_survey_date is the key to store one issue
        SharedPreferences SP = context.getSharedPreferences("ISSUES", Context.MODE_PRIVATE);
//        val editor = SP.edit()
        Issue issue = new GsonBuilder().create().fromJson(SP.getString(issue_survey_date, ""), Issue.class);
        issue.setIssue_rating(ratingValue);
        storeIssue(context, issue);
    }

    public void deleteAllIssue(Context context) {
        SharedPreferences SP = context.getSharedPreferences("ISSUES", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = SP.edit();
        editor.clear().commit();
    }

    public interface PreferenceListener {
        void onFinishToGetData(List<Issue> issues);
    }
}
