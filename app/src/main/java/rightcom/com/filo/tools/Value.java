package rightcom.com.filo.tools;

public class Value {
    public static final String ISSUE_TYPE = "issueType";
    public static final String TYPE_ALL = "totalIssue";
    public static final String TYPE_PENDING = "pendingIssue";
    public static final String TYPE_SOLVED = "solvedIssue";
    public static final String TYPE_NEW = "newIssue";

    public static final String TYPE_FACEBOOK = "FACEBOOK";
    public static final String TYPE_NORMAL = "NORMAL";

    public static final int NEW_STATUS = 0;
    public static final int PENDING_STATUS = -1;
    public static final int SOLVED_STATUS = 1;

    public static final String LABEL_TEXT_KEY = "LABEL";
    public static final String FRAME_TEXT_KEY = "FRAME";

    public static final String ACTIVITY_PARENT_KEY = "ACTIVITY";
    public static final String FRAG_PARENT_KEY = "FRAGMENT";
    public static final String SELECTED_ACTION_KEY = "ACTION_KEY";

    public static final String EXIT_ACTION_KEY = "SORTIR";
    public static final String LABEL_ACTION_KEY = "ETIQUETER";

    public static final String ARG_QR_CODE_KEY = "ARG_QR_CODE_KEY";

    public static final int WIDTH = 500;

    // Font path
    public static final String FONT_PATH = "fonts/Roboto/Roboto-Light.ttf";
    public static final String RECEIVE_ACTION_KEY = "RECEPTIONNER";
    public static final String API_STATUS_OK = "OK";
    public static final String API_STATUS_NOK = "NOK";
}
