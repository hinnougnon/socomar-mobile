package rightcom.com.filo;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.TextInputLayout;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rightcom.com.filo.R.string;
import rightcom.com.filo.api.ApiServiceGenerator;
import rightcom.com.filo.api.interfaces.ApiEndpointInterface;
import rightcom.com.filo.api.responses.SigninResponse;
import rightcom.com.filo.app.Filo;
import rightcom.com.filo.dialogs.CustomDialog;
import rightcom.com.filo.dialogs.DialogSearchBluetoophPrinter;
import rightcom.com.filo.models.CaptureSingleton;
import rightcom.com.filo.models.SettingModel;
import rightcom.com.filo.models.UserModel;
import rightcom.com.filo.printers.zebra.ZebraLabelPrinter;
import rightcom.com.filo.tools.Value;
import rightcom.com.filo.window.ExitVehicleWizardCameraActivity;
import rightcom.com.filo.window.MainPrinterDemoActivity;



//import com.zebra.android.devdemo.ConnectionScreen;
//import com.zebra.android.devdemo.R;
//import com.zebra.android.devdemo.util.DemoSleeper;
//import com.zebra.android.devdemo.util.SettingsHelper;
//import com.zebra.android.devdemo.util.UIHelper;
import com.zebra.sdk.comm.BluetoothConnection;
import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
//import com.zebra.sdk.comm.TcpConnection;
import com.zebra.sdk.printer.PrinterLanguage;
import com.zebra.sdk.printer.ZebraPrinter;
import com.zebra.sdk.printer.ZebraPrinterFactory;
import com.zebra.sdk.printer.ZebraPrinterLanguageUnknownException;



//import rightcom.com.filo.api.responses.GeneralResponse;
/*import rightcom.com.filo.application.KoodPay;
import rightcom.com.filo.dialogs.CustomDialog;*/
//import rightcom.com.filo.server.IDataManager.IResultLitener;

//import com.crashlytics.android.answers.Answers;
//import com.crashlytics.android.answers.LoginEvent;

public class LoginPageActivity extends Activity {
	//private  DBAdapter db;
	SettingModel currentsetting;
	private LoginPageActivity iLoginPageActivity;

	private EditText txtemail;
	private EditText txtpassword;
	private TextView btnlogon;
	private RelativeLayout rlSimpleConnexion;
	private static final int SCAN_ACTIVITY_ID = 1;
	public static final String LOGIN_PAGE_AND_LOADERS_CATEGORY = "rightcom.com.filo.LoginPageActivity";
	public static final String DARK = "Dark";
	public static final String LIGHT = "Light";
	private final static Gson GSON = new Gson();

    private Dialog dialog;

    Pattern IP_ADDRESS_PATERN= Pattern.compile(
            "((25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\\.(25[0-5]|2[0-4]"
                    + "[0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1]"
                    + "[0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}"
                    + "|[1-9][0-9]|[0-9]))");DialogSearchBluetoophPrinter dialog_b;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_login_page);
		txtemail= (EditText)findViewById(R.id.txtlogin_page_username);
		//txtemail.setText(KoodPay.getUSer().getemail());
		txtpassword= (EditText) findViewById(R.id.txtlogin_page_password);
		txtemail.setText("test");
		txtpassword.setText("t3st");
		rlSimpleConnexion = findViewById(R.id.rlSimpleConnexion);
		rlSimpleConnexion.setOnClickListener(rlSimplConnexionListener);
		btnlogon= (TextView) findViewById(R.id.buttonlogin);
		//btnlogon.setOnClickListener(buttonNextListener);

		//dialog_b = new DialogSearchBluetoophPrinter(LoginPageActivity.this);
        currentsetting = Filo.getSetting();
        iLoginPageActivity=this;
        Filo.logOut();
	}

	private void call_SignIn(String Code, String Pwd){
		Filo.appDialog= CustomDialog.callLoadingDialog(LoginPageActivity.this);
		Filo.appDialog.show();
		ApiEndpointInterface apiService =
				ApiServiceGenerator.getClient().create(ApiEndpointInterface.class);

		Call<SigninResponse> call = apiService.signIn(Code, Pwd, "pc");
		call.enqueue(new Callback<SigninResponse>() {
			@Override
			public void onResponse(Call<SigninResponse>call, Response<SigninResponse> response) {
				Filo.appDialog.dismiss();
				if(response.isSuccessful())
				{
					SigninResponse signindata=response.body();
					if(signindata.getkey()!=null && signindata.getkey()!="" && signindata.getStatut().equals(Value.API_STATUS_OK)){
						// convert result
						try {

							UserModel myuser=new UserModel();
							myuser.setkey(signindata.getkey());
							myuser.setusername(txtemail.getText().toString());
							myuser.setuser(signindata.getUser());
							myuser.setrights(Filo.ConvertArrayStringToString(signindata.getRight()));

							Filo.setUSer(myuser);
							Toast.makeText(getBaseContext(), "Bienvenue", Toast.LENGTH_SHORT).show();
							startActivity(new Intent(LoginPageActivity.this, PrincipalActivity.class));
							finish();
						} catch (final Exception e) {
                            Toast.makeText(getBaseContext(), string.login_incorrect+". Connexion réussie mais une erreur est intervenue lors du décodage des infos reçues du serveur."+e.getMessage(), Toast.LENGTH_LONG).show();
							e.printStackTrace();
                            return;
						}


					}
					else
					{
						Toast.makeText(getBaseContext(), string.login_incorrect+ " "+ signindata.getMsg(), Toast.LENGTH_SHORT).show();
						return;
					}
				}
				else
				{
					Toast.makeText(getBaseContext(), string.failed_connect, Toast.LENGTH_SHORT).show();
					return;
				}
			}

			@Override
			public void onFailure(Call<SigninResponse>call, Throwable t) {
				// Log error here since request failed
				Filo.appDialog.dismiss();
                Log.e("Failed", t.getMessage());
				Toast.makeText(getBaseContext(), string.failed_connect, Toast.LENGTH_SHORT).show();
			}
		});


	}

	@Override
	public void onBackPressed() {
	    super.onBackPressed();
	}



    @Override
    protected void onResume() {
        super.onResume();

        CheckIPSetting();

    }

    private boolean CheckIPSetting(){
        if(currentsetting.getdddressip()==null ){
            createAndShowPopUp();
            return false;
        }
        if(currentsetting.getdddressip().isEmpty() ){
            createAndShowPopUp();
            return false;
        }
        return true;
    }

    private void createAndShowPopUp() {
        dialog = new Dialog(LoginPageActivity.this);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.fragment_settings_wizard_first_page_popup);
        TextView tvPopUpTitle = dialog.findViewById(R.id.tvPopupTitle);
        TextView tvPopUpInfo = dialog.findViewById(R.id.tvInfo);
        final TextInputLayout tilUserPwd = dialog.findViewById(R.id.tilUserPassword);
        final TextInputLayout tilIpAddress = dialog.findViewById(R.id.tilIpAddress);
        RelativeLayout rlCancel = dialog.findViewById(R.id.rlCancel);
        TextView tvCancel = dialog.findViewById(R.id.tvCancel);
        RelativeLayout rlSave = dialog.findViewById(R.id.rlSave);
        TextView tvSave = dialog.findViewById(R.id.tvSave);
        /*Set dialog TextView TypeFace*/
//        tvPopUpTitle.setTypeface(app.getTypeFace());
//        tvPopUpInfo.setTypeface(app.getTypeFace());
//        tilUserPwd.setTypeface(app.getTypeFace());
//        tvCancel.setTypeface(app.getTypeFace());
//        tvSave.setTypeface(app.getTypeFace());

        tvPopUpTitle.setText("Adresse IP");
        tvPopUpInfo.setText("Modification de l'adresse ip actuelle.");
        tilIpAddress.setHint("Adresse IP");
        tilIpAddress.getEditText().setText(currentsetting.getdddressip());

        tilIpAddress.setVisibility(View.VISIBLE);
        tilUserPwd.setVisibility(View.GONE);

        rlCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        rlSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tilIpAddress.getEditText().getText().toString().isEmpty()) {
                    tilIpAddress.setError("Veuillez saisir une adresse ip.");
                    return;
                }

                Matcher matcher = IP_ADDRESS_PATERN.matcher(tilIpAddress.getEditText().getText().toString());
                if (!matcher.matches()) {
                    tilIpAddress.setError("Veuillez saisir une adresse ip valide.");
                    return;
                }

                currentsetting.setaddressip(tilIpAddress.getEditText().getText().toString());
                Filo.setSetting(currentsetting);
                dialog.dismiss();
            }
        });
        dialog.show();
    }



	private OnClickListener rlSimplConnexionListener = new OnClickListener() {
		@Override
		public void onClick(View view) {
			//startImageCapture();
			//startActivity(new Intent(LoginPageActivity.this, ExitVehicleWizardCameraActivity.class));
            if(!CheckIPSetting()){
                return;
            }
			if(txtemail.getText().toString().equals("") || txtemail.getText().toString().equals(" ")  ) {
				Toast.makeText(getBaseContext(), string.empty_username,
						Toast.LENGTH_SHORT).show();
				return;
			}
			if(txtpassword.getText().toString().equals("") || txtpassword.getText().toString().equals(" ")  ) {
				Toast.makeText(getBaseContext(), string.empty_password,
						Toast.LENGTH_SHORT).show();
				return;
			}
			call_SignIn(txtemail.getText().toString(),txtpassword.getText().toString());
		}
	};

// start print test area

	/*private boolean sendData = true;
	//Connection printerConnection = null;

	public void executeTest() {
		Filo.appDialog= CustomDialog.createCustomDialog(LoginPageActivity.this);
		Filo.appDialog.show();
		new Thread(new Runnable() {
			public void run() {
				Looper.prepare();
				//connectAndSendJob("UYDTUYTYUDT","kjsdhjkh");
				ZebraLabelPrinter objzebra=new ZebraLabelPrinter();
				objzebra.connectAndSendLabelJob("sljdlskjkl","sdmlkjfldj");
				Looper.loop();
				Looper.myLooper().quit();
				sendData = true;
			}
		}).start();

	}

	private void connectAndSendJob(String VIN_NUMBER,String BL) {
		printerConnection = new BluetoothConnection(txtemail.getText().toString());
		try {
			//helper.showLoadingDialog("Connecting...");
			Filo.appDialog= CustomDialog.createCustomDialog(LoginPageActivity.this);
			Filo.appDialog.show();
			printerConnection.open();

			ZebraPrinter printer = null;

			if (printerConnection.isConnected()) {
				printer = ZebraPrinterFactory.getInstance(printerConnection);

				if (printer != null) {
					sendJob(printerConnection,VIN_NUMBER,BL);
					printerConnection.close();
				}
			}
		} catch (ConnectionException e) {
			//helper.showErrorDialogOnGuiThread(e.getMessage());
			Toast.makeText(getBaseContext(), "Printer error: "+e.getMessage(),
					Toast.LENGTH_SHORT).show();
		} catch (ZebraPrinterLanguageUnknownException e) {
			//helper.showErrorDialogOnGuiThread("Could not detect printer language");
			Toast.makeText(getBaseContext(), "Printer error: Could not detect printer language",
					Toast.LENGTH_SHORT).show();
		} finally {
			//helper.dismissLoadingDialog();
			Filo.appDialog.dismiss();
		}
	}

	private void sendJob(Connection printerConnection,String VIN_NUMBER,String BL) {
		try {
			formatTextAndPrint(printerConnection,VIN_NUMBER,BL);
		} catch (ConnectionException e) {
			//helper.showErrorDialogOnGuiThread(e.getMessage());
			Toast.makeText(getBaseContext(), "Printer error: "+e.getMessage(),
					Toast.LENGTH_SHORT).show();
		}

	}
	private void formatTextAndPrint(Connection printerConnection,String VIN_NUMBER,String BL) throws ConnectionException {
        *//*
         This routine is provided to you as an example of how to create a variable length label with user specified data.
         The basic flow of the example is as follows

            Header of the label with some variable data
            Body of the label
                Loops thru user content and creates small line items of printed material
            Footer of the label

         As you can see, there are some variables that the user provides in the header, body and footer, and this routine uses that to build up a proper ZPL string for printing.
         Using this same concept, you can create one label for your receipt header, one for the body and one for the footer. The body receipt will be duplicated as many items as there are in your variable data

         *//*
		//String VIN_NUMBER="56TGSFGSJSJSGFS5";
		//String BL="BL12";
		String textToPrint =
        *//*
         Some basics of ZPL. Find more information here : http://www.zebra.com

         ^XA indicates the beginning of a label
         ^PW sets the width of the label (in dots)
         ^MNN sets the printer in continuous mode (variable length receipts only make sense with variably sized labels)
         ^LL sets the length of the label (we calculate this value at the end of the routine)
         ^LH sets the reference axis for printing.
            You will notice we change this positioning of the 'Y' axis (length) as we build up the label. Once the positioning is changed, all new fields drawn on the label are rendered as if '0' is the new home position
         ^FO sets the origin of the field relative to Label Home ^LH
         ^A sets font information
         ^FD is a field description
         ^GB is graphic boxes (or lines)
         ^B sets barcode information
         ^XZ indicates the end of a label
         *//*

				"^XA" +

						"^POI^PW800^MNN^LL380^LH0,0" + "\r\n" +

						"^FO50,5" + "\r\n" + "^A0,N,50,50" + "\r\n" + "^FD SOCOMAR^FS" + "\r\n" +

						"^FO40,50" + "\r\n" + "^A0,N,35,35" + "\r\n" + "^FDEtiquette d'identification^FS" + "\r\n" +

						"^FO10,100" + "\r\n" + "^A0,N,25,25" + "\r\n" + "^FDChassis:^FS" + "\r\n" +

						"^FO100,100" + "\r\n" + "^A0,N,25,25" + "\r\n" + "^FD"+VIN_NUMBER+"^FS" + "\r\n" +

						"^FO10,130" + "\r\n" + "^GB350,5,5,B,0^FS" +
						"^FO10,145" + "\r\n" + "^A0,N,30,30" + "\r\n" + "^FDExperience parc mobile!^FS" + "\r\n" +

						"^FO500,40,20^BQ,2,10^FDQA,"+VIN_NUMBER+"_"+BL+"^FS" + "\r\n" + "^XZ";

		printerConnection.write(textToPrint.getBytes());

	}*/
}
