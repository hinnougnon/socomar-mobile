package rightcom.com.filo.adapters;


import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import rightcom.com.filo.R;
import rightcom.com.filo.api.responses.LocationResponse;
import rightcom.com.filo.models.PrinterModel;

public class SpinnerPrinterAdapter extends ArrayAdapter<PrinterModel> {
    private Context context;
    private int resourceId;
    private List<PrinterModel> items, tempItems, suggestions;
    public SpinnerPrinterAdapter(@NonNull Context context, int resourceId, ArrayList<PrinterModel> items) {
        super(context, resourceId, items);
        this.items = items;
        this.context = context;
        this.resourceId = resourceId;
        tempItems = new ArrayList<>(items);
        suggestions = new ArrayList<>();
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        try {
            if (convertView == null) {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                view = inflater.inflate(resourceId, parent, false);
            }
            PrinterModel location = getItem(position);
            TextView name = (TextView) view.findViewById(R.id.textView_printer);
            if(!location.getdddressmac().toString().equals(null) && !location.getdddressmac().toString().isEmpty()){
                name.setText(location.getname()+" ("+location.getdddressmac()+")");
            }
            else{
                // no selection case
                name.setText(location.getname());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }
    @Nullable
    @Override
    public PrinterModel getItem(int position) {
        return items.get(position);
    }
    @Override
    public int getCount() {
        return items.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
}
