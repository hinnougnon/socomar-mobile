package rightcom.com.filo.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.HashMap;
import java.util.List;

import rightcom.com.filo.R;
import rightcom.com.filo.fragments.SettingsWizardFirstPageFragment;
import rightcom.com.filo.viewHolders.SettingsFragHolder;

public class SettingsFragAdapter extends RecyclerView.Adapter<SettingsFragHolder> implements SettingsFragHolder.OnItemClick {

    private SettingsWizardFirstPageFragment context;
    private HashMap<String, String> settings;
    private List<String> keys;
    private OnItemClickListener listener;

    public SettingsFragAdapter(SettingsWizardFirstPageFragment context, HashMap<String, String> settings, List<String> keys) {
        this.context = context;
        if (context instanceof OnItemClickListener) {
            this.listener = (OnItemClickListener) context;
        }
        this.settings = settings;
        this.keys = keys;
    }

    @NonNull
    @Override
    public SettingsFragHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new SettingsFragHolder(LayoutInflater.from(context.getActivity()).inflate(R.layout.fragment_settings_wizard_item, viewGroup, false), this);
    }

    @Override
    public void onBindViewHolder(@NonNull SettingsFragHolder holder, int i) {
        holder.display(keys.get(i), settings.get(keys.get(i)), i);
    }

    @Override
    public int getItemCount() {
        return keys.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(hasStableIds);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public void onClickListener(String s, int i) {
        listener.onItemClick(s, i);
    }

    public SettingsWizardFirstPageFragment getFragActivityContext() {
        if (context instanceof SettingsWizardFirstPageFragment)
            return (SettingsWizardFirstPageFragment) context;
        return null;
    }

    public interface OnItemClickListener {
        void onItemClick(String s, int i);
    }
}
