package rightcom.com.filo.adapters;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import rightcom.com.filo.fragments.ChangelocationVehicleWizardFirstPageFragment;
import rightcom.com.filo.fragments.ChangelocationVehicleWizardSecondPageFragment;
import rightcom.com.filo.fragments.ChangelocationVehicleWizardThirdPageFragment;
import rightcom.com.filo.fragments.OCRScannerFragment;
import rightcom.com.filo.fragments.ReceiveVehicleWizardFirstPageFragment;
import rightcom.com.filo.fragments.ReceiveVehicleWizardFourthPageFragment;
import rightcom.com.filo.fragments.ReceiveVehicleWizardSecondPageFragment;
import rightcom.com.filo.fragments.ReceiveVehicleWizardThirdPageFragment;

public class ChangelocationVehiclePageAdapter extends FragmentPagerAdapter {

    public ChangelocationVehiclePageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return ChangelocationVehicleWizardFirstPageFragment.newInstance(position);
        }
        if (position == 1) {
            return ChangelocationVehicleWizardSecondPageFragment.newInstance(position);
        }
        if (position == 2) {
            return ChangelocationVehicleWizardThirdPageFragment.newInstance(position);
        }
        // default
        return ChangelocationVehicleWizardFirstPageFragment.newInstance(position);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

}
