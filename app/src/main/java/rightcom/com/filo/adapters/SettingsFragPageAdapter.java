package rightcom.com.filo.adapters;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import rightcom.com.filo.fragments.SettingsWizardFirstPageFragment;

public class SettingsFragPageAdapter extends FragmentPagerAdapter {

    public SettingsFragPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }

    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return SettingsWizardFirstPageFragment.newInstance(position);
        }
        return null;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

}
