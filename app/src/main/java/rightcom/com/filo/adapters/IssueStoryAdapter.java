package rightcom.com.filo.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import rightcom.com.filo.R;
import rightcom.com.filo.models.Issue;
import rightcom.com.filo.viewHolders.IssueStoryHolder;

public class IssueStoryAdapter extends RecyclerView.Adapter<IssueStoryHolder> {

    private Context context;
    private List<Issue> issueStories;
    private IssueStroyListener mListener;

    public IssueStoryAdapter(Context context, List<Issue> issueStories) {
        this.context = context;
        this.issueStories = issueStories;
        if (context instanceof IssueStroyListener) {
            mListener = (IssueStroyListener) context;
        }
    }

    @NonNull
    @Override
    public IssueStoryHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new IssueStoryHolder(LayoutInflater.from(context).inflate(R.layout.item_story_new, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull IssueStoryHolder holder, int position) {
        Issue issue = issueStories.get(position);
        holder.display(context, issue);
    }

    @Override
    public int getItemCount() {
        if (issueStories.size() > 0) {
            return issueStories.size();
        }
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(hasStableIds);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public IssueStoryAdapter getContext() {
        return this;
    }

    public interface IssueStroyListener {
        public void onIssueStroyListener();
    }
}
