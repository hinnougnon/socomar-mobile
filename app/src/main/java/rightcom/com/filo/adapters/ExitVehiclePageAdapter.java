package rightcom.com.filo.adapters;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import rightcom.com.filo.fragments.ExitVehicleWizardFirstPageFragment;
import rightcom.com.filo.fragments.ExitVehicleWizardSecondPageFragment;
import rightcom.com.filo.fragments.ExitVehicleWizardScanCodeResultFragment;
import rightcom.com.filo.fragments.ExitVehicleWizardThirdPageFragment;
import rightcom.com.filo.fragments.OCRScannerFragment;

public class ExitVehiclePageAdapter extends FragmentPagerAdapter {

    public ExitVehiclePageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return ExitVehicleWizardFirstPageFragment.newInstance(position);
        }
        if (position == 1) {
            return ExitVehicleWizardSecondPageFragment.newInstance(position);
        }
        if (position == 2){
            return OCRScannerFragment.newInstance(position);
        }

        if (position == 3){
            return ExitVehicleWizardThirdPageFragment.newInstance(position);
        }
        return  ExitVehicleWizardFirstPageFragment.newInstance(position);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }
}
