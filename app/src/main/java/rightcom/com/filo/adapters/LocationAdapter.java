package rightcom.com.filo.adapters;


import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

import rightcom.com.filo.R;
import rightcom.com.filo.api.responses.LocationResponse;

public class LocationAdapter extends ArrayAdapter<LocationResponse> {
    private Context context;
    private int resourceId;
    private List<LocationResponse> items, tempItems, suggestions;
    public LocationAdapter(@NonNull Context context, int resourceId, ArrayList<LocationResponse> items) {
        super(context, resourceId, items);
        this.items = items;
        this.context = context;
        this.resourceId = resourceId;
        tempItems = new ArrayList<>(items);
        suggestions = new ArrayList<>();
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        try {
            if (convertView == null) {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                view = inflater.inflate(resourceId, parent, false);
            }
            LocationResponse location = getItem(position);
            TextView name = (TextView) view.findViewById(R.id.textView);
            name.setText(location.getlocation());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }
    @Nullable
    @Override
    public LocationResponse getItem(int position) {
        return items.get(position);
    }
    @Override
    public int getCount() {
        return items.size();
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @NonNull
    @Override
    public Filter getFilter() {
        return fruitFilter;
    }
    private Filter fruitFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            LocationResponse location = (LocationResponse) resultValue;
            return location.getlocation();
        }
        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            if (charSequence != null) {
                suggestions.clear();
                for (LocationResponse location: tempItems) {
                    if (location.getlocation().toLowerCase().startsWith(charSequence.toString().toLowerCase())) {
                        suggestions.add(location);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }
        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            ArrayList<LocationResponse> c = (ArrayList<LocationResponse>) filterResults.values;
            if (filterResults != null && filterResults.count > 0) {
                clear();
                for (LocationResponse cust : c) {
                    add(cust);
                    notifyDataSetChanged();
                }
            } else {
                clear();
                notifyDataSetChanged();
            }
        }
    };
}
