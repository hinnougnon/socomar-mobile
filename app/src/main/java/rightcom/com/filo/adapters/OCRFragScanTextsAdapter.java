package rightcom.com.filo.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import rightcom.com.filo.R;
import rightcom.com.filo.fragments.OCRScannerFragment;
import rightcom.com.filo.viewHolders.OCRFragScanTextsHolder;

public class OCRFragScanTextsAdapter extends RecyclerView.Adapter<OCRFragScanTextsHolder> implements OCRFragScanTextsHolder.OnItemClick {

    private OCRScannerFragment context;
    private List<String> mList;
    private OnItemClickListener listener;

    public OCRFragScanTextsAdapter(OCRScannerFragment context, List<String> list) {
        this.context = context;
        if (context instanceof OnItemClickListener) {
            this.listener = (OnItemClickListener) context;
        }
        this.mList = list;
    }

    @NonNull
    @Override
    public OCRFragScanTextsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new OCRFragScanTextsHolder(LayoutInflater.from(context.getActivity()).inflate(R.layout.fragment_ocr_scan_text_item, viewGroup, false), this);
    }

    @Override
    public void onBindViewHolder(@NonNull OCRFragScanTextsHolder holder, int i) {
        holder.display(mList.get(i));
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(hasStableIds);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public void onClickListener(String s) {
        listener.onItemClick(s);
    }

    public OCRScannerFragment getFragActivityContext() {
        if (context instanceof OCRScannerFragment)
            return (OCRScannerFragment) context;
        return null;
    }

    public interface OnItemClickListener {
        void onItemClick(String s);
    }
}
