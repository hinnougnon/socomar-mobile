package rightcom.com.filo.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import rightcom.com.filo.R;
import rightcom.com.filo.fragments.OCRScannerFragment;
import rightcom.com.filo.viewHolders.OCRActivityScanTextsHolder;
import rightcom.com.filo.viewHolders.OCRFragScanTextsHolder;
import rightcom.com.filo.window.OCRScannerActivity;

public class OCRActivityScanTextsAdapter extends RecyclerView.Adapter<OCRActivityScanTextsHolder> implements OCRActivityScanTextsHolder.OnItemClick {

    private OCRScannerActivity context;
    private List<String> mList;
    private OnItemClickListener listener;

    public OCRActivityScanTextsAdapter(OCRScannerActivity context, List<String> list) {
        this.context = context;
        if (context instanceof OnItemClickListener) {
            this.listener = (OnItemClickListener) context;
        }
        this.mList = list;
    }

    @NonNull
    @Override
    public OCRActivityScanTextsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new OCRActivityScanTextsHolder(LayoutInflater.from(context).inflate(R.layout.activity_ocr_scan_text_item, viewGroup, false), this);
    }

    @Override
    public void onBindViewHolder(@NonNull OCRActivityScanTextsHolder holder, int i) {
        holder.display(mList.get(i));
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(hasStableIds);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public void onClickListener(String s) {
        listener.onItemClick(s);
    }

    public OCRScannerActivity getFragActivityContext() {
        if (context instanceof OCRScannerActivity)
            return (OCRScannerActivity) context;
        return null;
    }

    public interface OnItemClickListener {
        void onItemClick(String s);
    }
}
