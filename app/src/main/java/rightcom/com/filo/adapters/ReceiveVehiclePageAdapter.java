package rightcom.com.filo.adapters;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import rightcom.com.filo.fragments.OCRScannerFragment;
import rightcom.com.filo.fragments.ReceiveVehicleOCRFragment;
import rightcom.com.filo.fragments.ReceiveVehicleScanCodeFragment;
import rightcom.com.filo.fragments.ReceiveVehicleWizardFirstPageFragment;
import rightcom.com.filo.fragments.ReceiveVehicleWizardFourthPageFragment;
import rightcom.com.filo.fragments.ReceiveVehicleWizardSecondPageFragment;
import rightcom.com.filo.fragments.ReceiveVehicleWizardShowResultsFragment;
import rightcom.com.filo.fragments.ReceiveVehicleWizardStartFragment;
import rightcom.com.filo.fragments.ReceiveVehicleWizardThirdPageFragment;
import rightcom.com.filo.fragments.ScanCodeCheckingFragment;

public class ReceiveVehiclePageAdapter  extends FragmentPagerAdapter {

    public ReceiveVehiclePageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public Fragment getItem(int position) {
//        if (position == 0) {
//            return ReceiveVehicleScanCodeFragment.newInstance(position);
//        } else {
//            return ReceiveVehicleWizardShowResultsFragment.newInstance(position);
//        }
        if (position == 0) {
            //return ReceiveVehicleOCRFragment.newInstance(position);
            return ReceiveVehicleWizardFirstPageFragment.newInstance(position);
        }
        if (position == 1){
            return OCRScannerFragment.newInstance(position);
        }
        if (position == 2) {
            return ReceiveVehicleWizardSecondPageFragment.newInstance(position);
        }
        if (position == 3) {
            return ReceiveVehicleWizardThirdPageFragment.newInstance(position);
        }
        if (position == 4) {
            return ReceiveVehicleWizardFourthPageFragment.newInstance(position);
        }
        // default
        return ReceiveVehicleWizardFirstPageFragment.newInstance(position);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

}
