package rightcom.com.filo.adapters;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import rightcom.com.filo.fragments.LabelVehicleWizardFirstPageFragment;
import rightcom.com.filo.fragments.LabelVehicleWizardSecondPageFragment;


public class LabelVehiclePageAdapter extends FragmentPagerAdapter {

    public LabelVehiclePageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return LabelVehicleWizardFirstPageFragment.newInstance(position);
        }
        if (position == 1) {
            return LabelVehicleWizardSecondPageFragment.newInstance(position);
        }

        // default
        return LabelVehicleWizardFirstPageFragment.newInstance(position);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

}
